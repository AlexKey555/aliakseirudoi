<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<form:form action="addNewsAction" commandName="newsVO">

	<div class="inputInfoDiv">
		<spring:message code="local.message.title" />
	</div>
	<div class="inputDiv">
		<form:input id="inputDivM" path="news.newsTitle" size="68" pattern="[\\w\\W]{3,30}$" title="3-30 symbols" placeholder="max lengh 30 symbols"/>
	</div>
	
	<div class="inputInfoDiv">
		<spring:message code="local.message.brief" />
	</div>
	<div class="inputDiv">
		<form:textarea path="news.newsShortText" cols="61" rows="3" placeholder="max lengh 100 symbols" maxlength="100" 
		 />
	</div>
	<div class="inputInfoDiv">
		<spring:message code="local.message.content" />
	</div>
	<div class="inputDiv">
		<form:textarea id="addNewsTextArea" path="news.newsFullText" cols="61" rows="17" placeholder="max lengh 2000 symbols" maxlength="2000"/>
	</div>

	<table id="tableSearchAddNewsPage">
		<tr>
			<td><form:select path="author.authorId">
					<form:option value="0" label="Please select the author" />
					<c:forEach var="author" items="${authorsList}">
						<form:option value="${author.authorId}"> 
							<c:out value="${author.authorName}"/>
						  </form:option>
					</c:forEach>
				</form:select></td>
			<td>
				<div class="multiselect">
					<div class="selectBox" onclick="showCheckboxes()">
						<select>
							<option>Select the tags</option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes">
						<c:forEach var="tags" items="${tagsList}">

							<label for="${tags.tagId}"> <form:checkbox
									path="tagsIdList" value="${tags.tagId}" /> 
								<c:out value="${tags.tagName}"/>
							</label>
						</c:forEach>
					</div>
				</div>
			</td>

		</tr>

	</table>

	<form:button path="" id="bottomAddNewsAddPage">
		<spring:message code="local.button.save" />
	</form:button>
</form:form>

	<div id="messageMissAuthorOrTags"> ${messageMissAuthorOrTags}</div>