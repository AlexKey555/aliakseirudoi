
$(function() {
    $('#light-theme').pagination({
     pages: '${pageId}',
        itemsOnPage: 3,
        cssStyle: 'light-theme',
        hrefTextPrefix: '',
        currentPage: '${paginNum}'
    });
});

