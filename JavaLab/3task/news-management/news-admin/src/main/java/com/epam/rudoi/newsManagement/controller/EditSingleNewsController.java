package com.epam.rudoi.newsManagement.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static com.epam.rudoi.newsManagement.constants.Expression.*;

import com.epam.rudoi.newsManagement.controller.util.ConfigurationManager;
import com.epam.rudoi.newsManagement.controller.util.ValidityChecker;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.INewsManagementService;
import com.epam.rudoi.newsportal.service.impl.jdbc.ServiceManagerImpl;

@Controller
public class EditSingleNewsController {

    @Autowired
    private INewsManagementService serviceManager;

    @RequestMapping({ "/addCommentAdmin" })
    public ModelAndView addCommentPage(@ModelAttribute("comment") Comment comment) throws ServiceExeption {

	if (ValidityChecker.checkValidity(comment.getCommentText(), EXPRESSION_FOR_SHORTTEXT_COMMENTS)) {
	    comment.setCreationDate(new Date());
	    serviceManager.addComment(comment);
	} else {
	    ModelAndView mv = new ModelAndView("redirect:/editSingleNews");
	    mv.addObject("messageCommentNotValid", ConfigurationManager.getProperty("message.fail.maxLengthComment"));
	    return mv;
	}

	ModelAndView mv = new ModelAndView("redirect:/editSingleNews?newsId=" + comment.getNewsId());

	return mv;
    }
    
    @RequestMapping({ "/updateNews" })
    public ModelAndView updateNewsAction(@ModelAttribute("newsVO1") NewsManagementVO newsVO) throws ServiceExeption {
	
	/*newsVO.getNews().setNewsCreationDate(new Date());
	newsVO.getNews().setNewsModificationDate(new Date());*/
	Author author = newsVO.getAuthor();
	List<Author> authorsList = new ArrayList<Author>();
	authorsList.add(author);
	List<Tag> tagsList = newsVO.getTagsList();
	newsVO.getNews().setAuthorsList(authorsList);
	newsVO.getNews().setTagsList(tagsList);
	
	boolean allowToUpdateCurrentNews = serviceManager.editNews(newsVO);
	
	
	ModelAndView mv = new ModelAndView("editSingleNews", "comment", new Comment());
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());
	if(!allowToUpdateCurrentNews) {
	    mv.addObject("failMessage", ConfigurationManager.getProperty("message.fail.updateNews"));    
	}
	mv.addObject("newsVO", serviceManager.showSinglNews(newsVO.getNews().getNewsId()));
	mv.addObject("commentsList", serviceManager.getCommentsList(newsVO.getNews().getNewsId()));
	mv.addObject("paginNum", serviceManager.countAllNews());
	mv.addObject("queryString", "&newsId=" + newsVO.getNews().getNewsId());
	mv.addObject("currentNewsId", newsVO.getNews().getNewsId());
	mvForSearch(mv);
	
	return mv;
	
    }
    
   

    @RequestMapping({ "/nextNews" })
    public ModelAndView showNextNews(HttpSession session, @RequestParam("newsId") Long newsId) throws ServiceExeption {

	List<NewsManagementVO> newsVOList = getNewsVOList(session);
	
	Long nextNewsId = singleNewsNext(newsId, newsVOList);

	ModelAndView mv = new ModelAndView("editSingleNews", "comment", new Comment());

	mv.addObject("resultNextNewsId", newsVOList.get(newsVOList.size() - 1).getNews().getNewsId());
	mv.addObject("newsVO", serviceManager.showSinglNews(nextNewsId));
	mv.addObject("commentsList", serviceManager.getCommentsList(nextNewsId));
	mv.addObject("queryString", "&newsId=" + newsId);

	return mv;

    }

    @RequestMapping({ "/previousNews" })
    public ModelAndView showPreviousNews(HttpSession session, @RequestParam("newsId") Long newsId)
	    throws ServiceExeption {

	List<NewsManagementVO> newsVOList = getNewsVOList(session);
	
	Long previousNewsId = singleNewsPrevious(newsId, newsVOList);

	ModelAndView mv = new ModelAndView("editSingleNews", "comment", new Comment());
	
	mv.addObject("resultPreviousNewsId", newsVOList.get(0).getNews().getNewsId());
	mv.addObject("newsVO", serviceManager.showSinglNews(previousNewsId));
	mv.addObject("commentsList", serviceManager.getCommentsList(previousNewsId));
	mv.addObject("queryString", "&newsId=" + newsId);

	return mv;
    }

    @RequestMapping({ "/deleteCommentAdmin" })
    public ModelAndView deleteComment(@ModelAttribute("comment") Comment comment) throws ServiceExeption {

	serviceManager.deleteComments(comment.getCommentId());

	ModelAndView mv = new ModelAndView("redirect:/editSingleNews?newsId=" + comment.getNewsId());

	return mv;
    }

    
    private List<NewsManagementVO> getNewsVOList(HttpSession session) throws ServiceExeption {
   	SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
   	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);
   	return newsVOList;
       }
    
    private Long singleNewsNext(Long newsId, List<NewsManagementVO> newsVOList) {
	ListIterator<NewsManagementVO> itr = newsVOList.listIterator();
	while (itr.hasNext()) {
	    if (itr.next().getNews().getNewsId().equals(newsId) & itr.hasNext()) {
		Long newsIdNext = itr.next().getNews().getNewsId();
		return newsIdNext;
	    }
	}
	return newsId;
    }

    private Long singleNewsPrevious(Long newsId, List<NewsManagementVO> newsVOList) {
	int i = 0;
	Long newsIdPrevious = null;
	for (NewsManagementVO newsManagementVO : newsVOList) {
	    if (newsManagementVO.getNews().getNewsId().equals(newsId)) {
		i = newsVOList.indexOf(newsManagementVO);
		break;
	    }
	}
	if (i != 0) {
	    newsIdPrevious = newsVOList.get(i - 1).getNews().getNewsId();
	    return newsIdPrevious;
	}
	return newsId;
    }
    
    private Long getCurrentNewsIndex(SearchCriteria searchCriteria, Long newsId, List<NewsManagementVO> currentNewsList) {
   	Long currentNewsIndex = null;
   		
   	for (int i = 0; i <= currentNewsList.size(); i++) {
   	    if(currentNewsList.get(i).getNews().getNewsId().equals(newsId)) {
   		 currentNewsIndex = (long) i;
   		 break;
   	     }
   	}
   		return currentNewsIndex;
       }
    
    private void mvForSearch(ModelAndView mv) throws ServiceExeption {
   	mv.addObject("authorsList", serviceManager.getAuthorsList());
   	mv.addObject("tagsList", serviceManager.getTagsList());
   	mv.addObject("tagsIdList", serviceManager.getTagIdList());
       }
}
