package com.epam.rudoi.newsManagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.rudoi.newsManagement.controller.util.ConfigurationManager;
import com.epam.rudoi.newsManagement.controller.util.ValidityChecker;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.INewsManagementService;
import com.epam.rudoi.newsportal.service.impl.jdbc.ServiceManagerImpl;

import static com.epam.rudoi.newsManagement.constants.Expression.*;

@Controller
public class NewsController {

    @Autowired
    private INewsManagementService serviceManager;

    @RequestMapping({ "addNewsAction" })
    public ModelAndView addNewsAction(HttpSession session, @ModelAttribute("newsVO") NewsManagementVO newsVO) throws ServiceExeption {

	if (newsVO.getAuthor() == null | newsVO.getTagsIdList() == null) {
	    ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
	    mv.addObject("messageMissAuthorOrTags", ConfigurationManager.getProperty("message.fail.newsAdd"));
	    mvForSearch(mv);
	    return mv;
	} else {
	   
	      if (ValidityChecker.checkValidity(newsVO.getNews().getNewsTitle(), EXPRESSION_FOR_TAGS_AUTHORS_TITLE)) {

		if (ValidityChecker.checkValidity(newsVO.getNews().getNewsShortText(),
			EXPRESSION_FOR_SHORTTEXT_COMMENTS)) {

		    if (ValidityChecker.checkValidity(newsVO.getNews().getNewsFullText(), EXPRESSION_FOR_FULLTEXT)) {

		    } else {
			ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
			mv.addObject("messageFullTextNotValid", "length size must be 3 - 2000 symbols");
			mvForSearch(mv);
			return mv;
		    }

		} else {
		    ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
		    mv.addObject("messageShortTextNotValid", "length size must be 3 - 100 symbols");
		    mvForSearch(mv);
		    return mv;
		}

	    } else {
		ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
		mv.addObject("messageTitleNotValid", "length size must be 3 - 30 symbols");
		mvForSearch(mv);
		return mv;
	    }
	      
	    List<Author> authorsList = new ArrayList<Author>();
	    Author author = new Author();
	    author.setAuthorId(newsVO.getAuthor().getAuthorId());
	    authorsList.add(author);

	    List<Tag> tagsList = new ArrayList<Tag>(newsVO.getTagsIdList().size());
	    List<Long> tagsIdList = newsVO.getTagsIdList();
	    for (Long long1 : tagsIdList) {
		Tag tag = new Tag();
		tag.setTagId(long1);
		tagsList.add(tag);
	    }
	    
	    newsVO.getNews().setAuthorsList(authorsList);
	    newsVO.getNews().setTagsList(tagsList);
	    newsVO.getNews().setNewsCreationDate(new Date());
	    newsVO.getNews().setNewsModificationDate(new Date());

	    serviceManager.addNews(newsVO);
	}

	SearchCriteria searchCriteria = fillSearchCriteria(session);
	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);

	ModelAndView mv = new ModelAndView("redirect:/adminHome", "searchCriteria", new SearchCriteria());
	
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());

	newsVOList = newsVOList.subList(0, 3);
	mv.addObject("newsVO", newsVOList);
	mvForSearch(mv);
 
	return mv;
    }
    
    @RequestMapping({ "/editSingleNews" })
    public ModelAndView showSingleNewsPage(@RequestParam("newsId") Long newsId) throws ServiceExeption {

	ModelAndView mv = new ModelAndView("editSingleNews", "comment", new Comment());
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());
	
	mv.addObject("newsVO", serviceManager.showSinglNews(newsId));
	mv.addObject("commentsList", serviceManager.getCommentsList(newsId));
	mv.addObject("paginNum", serviceManager.countAllNews());
	mv.addObject("queryString", "&newsId=" + newsId);
	mv.addObject("currentNewsId", newsId);
	mvForSearch(mv);

	return mv;
    }
    
    @RequestMapping({ "/deleteNews" })
    public ModelAndView deleteNews(HttpSession session, @ModelAttribute("newsVO1") NewsManagementVO newsVO) throws ServiceExeption {

	ModelAndView mv = new ModelAndView("adminHome", "searchCriteria", new SearchCriteria());
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());
	
	serviceManager.deleteNews(newsVO);

	SearchCriteria searchCriteria = fillSearchCriteria(session);
	
	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);
	newsVOList = newsVOList.subList(0, 3);

	newsVOList = newsVOList.subList(0, 3);
	mv.addObject("newsVO", newsVOList);
	mvForSearch(mv);

	return mv;
    }

    private SearchCriteria fillSearchCriteria(HttpSession session) {
	SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
	return searchCriteria;
    }

    private void mvForSearch(ModelAndView mv) throws ServiceExeption {
	mv.addObject("authorsList", serviceManager.getAuthorsList());
	mv.addObject("tagsList", serviceManager.getTagsList());
	mv.addObject("tagsIdList", serviceManager.getTagIdList());
    }

}
