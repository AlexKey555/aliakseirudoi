package com.epam.rudoi.newsManagement.constants;

public class Expression {
    	
    private Expression() {
	
    };
    
    	public final static String EXPRESSION_FOR_TAGS_AUTHORS_TITLE = "[\\w\\W]{3,30}$"; 
    	public final static String EXPRESSION_FOR_SHORTTEXT_COMMENTS = "[\\w\\W]{3,100}$";
    	public final static String EXPRESSION_FOR_FULLTEXT = "[\\w\\W]{3,2000}$";
}
