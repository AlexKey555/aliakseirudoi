package com.epam.rudoi.newsportal.dao;

import java.util.List;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface ICommentDAO.
 */
public interface ICommentDAO extends ICrudDAO<Comment>{
	
	/**
	 * Unlink.
	 * This method unlink news with comment
	 * @param newsId the news id
	 * @throws DAOExeption the DAO exception
	 */
	void unlink(Long newsId) throws DAOExeption;
	
	/**
	 * Unlink.
	 * This method unlink qentity news with comments
	 * @param newsIdList the news id list
	 * @throws DAOExeption the DAO exception
	 */
	void unlink(List<Long> newsIdList) throws DAOExeption;
	
	/**
	 * Gets the comment list.
	 * This method get comments list
	 * @param newsId the news id
	 * @return the comment list
	 * @throws DAOExeption the DAO exception
	 */
	List<Comment> getCommentList(Long newsId) throws DAOExeption;
	
	/**
	 * Delete news comments.
	 * This method delete news
	 * @param newsId the news id
	 * @throws DAOExeption the DAO exception
	 */
	void deleteNewsComments(Long newsId) throws DAOExeption;

}
