package com.epam.rudoi.newsportal.service;

import java.util.List;

import ognl.MethodFailedException;

import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface ITagService.
 */
public interface ITagService {

	/**
	 * Adds the tag.
	 * This method add tag
	 * @param managementVO the management vo
	 * @return the long
	 * @throws ServiceExeption the service exception
	 */
	Long addTag(NewsManagementVO managementVO) throws ServiceExeption;

	/**
	 * Edits the tag.
	 * This method edit tag
	 * @param tagItem the tag item
	 * @throws ServiceExeption the service exception
	 */
	void editTag(Tag tagItem) throws ServiceExeption;

	/**
	 * Delete tag.
	 * This method delete tag
	 * @param tagId the tag id
	 * @throws ServiceExeption the service exception
	 */
	void deleteTag(Long tagId) throws ServiceExeption;
	
	/**
	 * Link.
	 * This method lint qentity tags with news
	 * @param tagsIdList the tags id list
	 * @param newsId the news id
	 * @throws ServiceExeption the service exception
	 */
	void link(List<Long> tagsIdList, Long newsId) throws ServiceExeption;

	/**
	 * Unlink.
	 * This method unlink news with tag
	 * @param newsId the news id
	 * @throws ServiceExeption the service exception
	 */
	void unlink(Long newsId) throws ServiceExeption;

	/**
	 * Unlink.
	 * This method unlink qentity news with tags
	 * @param newsIdList the news id list
	 * @throws ServiceExeption the service exception
	 */
	void unlink(List<Long> newsIdList) throws ServiceExeption;
	
	/**
	 * Unlink by tag id.
	 * This method unlink by tag id tag with news
	 * @param tagId the tag id
	 * @throws ServiceExeption the service exception
	 */
	void unlinkByTagId(Long tagId) throws ServiceExeption;
	
	/**
	 * Gets the tags list.
	 * This method get tags list
	 * @return the tags list
	 * @throws ServiceExeption the service exception
	 */
	List<Tag> getTagsList () throws ServiceExeption;
	
	/**
	 * Gets the tag id list.
	 * This method get tag id list
	 * @return the tag id list
	 * @throws ServiceExeption the service exception
	 */
	List<Long> getTagIdList () throws ServiceExeption;

}
