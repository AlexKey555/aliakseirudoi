package com.epam.rudoi.newsportal.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

@Entity(name = "TAGS")
@Table(name = "TAGS")
@Indexed
@SequenceGenerator(name = "SQ_TAG_ID_SEQ", sequenceName = "SQ_TAG_ID_SEQ", allocationSize = 1)
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TAG_ID_SEQ")
    @Column(name = "TAG_ID", nullable = false, precision = 20)
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    private Long tagId;

    @Column(name = "TAG_NAME", precision = 30)
    private String tagName;

    
      @ManyToMany()
      
      @JoinTable(name = "NEWS_TAGS", joinColumns = { @JoinColumn(name =
      "TAG_ID", referencedColumnName = "TAG_ID") }, inverseJoinColumns = {
      @JoinColumn(name = "NEWS_ID", referencedColumnName = "NEWS_ID") })
      private List<News> newsList;
     

    public Tag() {
	super();
    }

    public Tag(Long tagId, String tagName) {
	super();
	this.tagId = tagId;
	this.tagName = tagName;
    }

    public Tag(Long tagId) {
	super();
	this.tagId = tagId;
    }

    public Tag(String tagName) {
	super();
	this.tagName = tagName;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
	result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Tag other = (Tag) obj;
	if (tagId == null) {
	    if (other.tagId != null)
		return false;
	} else if (!tagId.equals(other.tagId))
	    return false;
	if (tagName == null) {
	    if (other.tagName != null)
		return false;
	} else if (!tagName.equals(other.tagName))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
    }

 

}
