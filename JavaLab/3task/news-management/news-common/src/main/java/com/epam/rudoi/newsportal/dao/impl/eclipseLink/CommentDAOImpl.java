package com.epam.rudoi.newsportal.dao.impl.eclipseLink;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("eclipselink")
public class CommentDAOImpl implements ICommentDAO {

    @PersistenceUnit(unitName = "entityManagerFactory")
    private EntityManagerFactory emf;

    @Override
    public Long addItem(Comment comment) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    entityManager.persist(comment);
	} finally {
	    entityTransaction.commit();
	}

	return comment.getCommentId();
    }

    @Override
    public boolean editItem(Comment comment) throws DAOExeption {
	boolean success = true;
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    entityManager.merge(comment);
	} finally {
	    entityTransaction.commit();
	}

	return success;
    }

    @Override
    public void deleteItem(Long commentId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    Comment comment = entityManager.find(Comment.class, commentId);
	    entityManager.remove(comment);
	} finally {
	    entityTransaction.commit();
	}

    }

    @Override
    public Comment readItem(Long commentId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();

	Comment comment = entityManager.find(Comment.class, commentId);

	return comment;
    }

    @Override
    public List<Comment> getCommentList(Long newsId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	List<Comment> commentsList = null;

	commentsList = entityManager.createQuery("from COMMENTS a where a.newsId = :newsId", Comment.class)
		.setParameter("newsId", newsId).getResultList();

	return commentsList;
    }

    @Override
    public void deleteNewsComments(Long newsId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    Comment comment = entityManager.find(Comment.class, newsId);
	    entityManager.remove(comment);
	} finally {
	    entityTransaction.commit();
	}

    }

    @Override
    public void unlink(Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

}
