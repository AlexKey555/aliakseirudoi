package com.epam.rudoi.newsportal.util;

import static com.epam.rudoi.newsportal.constants.ParamConstants.*;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.epam.rudoi.newsportal.entity.SearchCriteria;

public class ParamForStatmentBuilder {

    public static void buidlSearchParam (SearchCriteria searchCriteria, PreparedStatement preparedStatement) throws SQLException {
	
	Long haveAuthorId = searchCriteria.getAuthor().getAuthorId();
	List<Long> haveTagsIdList = searchCriteria.getTagsIdList();

	if (haveAuthorIdAndTagsIdList(haveAuthorId, haveTagsIdList)) {
	    preparedStatement.setLong(1, searchCriteria.getAuthor().getAuthorId());
	    int i = PARAM_SIZE_FOR_ITER_2PARAM;
	    for (Long tagId : haveTagsIdList) {
		preparedStatement.setLong(i, tagId);
		i++;
	    }

	} else if (haveOnlyAuthorId(haveAuthorId)) {
	    preparedStatement.setLong(1, searchCriteria.getAuthor().getAuthorId());
	}

	else if (haveOnlyTagsIdList(haveTagsIdList)) {
	    int i = PARAM_SIZE_FOR_ITER_1PARAM;
	    for (Long tagId : haveTagsIdList) {
		preparedStatement.setLong(i, tagId);
		i++;

	    }
	}
    }
    
    private boolean haveOnlyTagsIdListForQuearyBuilder(Long haveAuthorId, List<Long> haveTagsIdList) {
	return (haveAuthorId == null || haveAuthorId == 0) & haveOnlyTagsIdList(haveTagsIdList);
    }

    private static boolean haveOnlyTagsIdList(List<Long> haveTagsIdList) {
	return haveTagsIdList != null;
    }

    private static boolean haveOnlyAuthorId(Long haveAuthorId) {
	return haveAuthorId != null && haveAuthorId != 0;
    }

    private static boolean haveAuthorIdAndTagsIdList(Long haveAuthorId, List<Long> haveTagsIdList) {
	return haveOnlyAuthorId(haveAuthorId) & haveOnlyTagsIdList(haveTagsIdList);
    }

    private boolean haveNoParam(Long haveAuthorId, List<Long> haveTagsIdList) {
	return (haveAuthorId == null || haveAuthorId == 0) & haveTagsIdList == null;
    }
    
}
