package com.epam.rudoi.newsportal.service.impl.eclipseLink;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.INewsService;

@Profile("eclipselink")
public class NewsServiceImpl implements INewsService{
    
  private static final Logger LOGGER = Logger.getLogger(NewsServiceImpl.class);
    
    private INewsDAO newsDAO;


    @Override
    public Long addNews(News news) throws ServiceExeption {
	Long newsId = null;
	try {
	    newsId = newsDAO.addItem(news);
	} catch (DAOExeption e) {
	    LOGGER.error("Adding news eclipseLink exeption", e);
	    throw new ServiceExeption();
	}
	return newsId;
    }

    @Override
    public boolean editNews(News news) throws ServiceExeption {
	boolean flag;
	try {
	    flag = newsDAO.editItem(news);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing news eclipseLink exeption", e);
	    throw new ServiceExeption();
	}
	return flag;
	
    }

    @Override
    public void deleteaQuantityNews(List<Long> newsIdList) throws ServiceExeption {
	try {
	    newsDAO.deleteaQuantityNews(newsIdList);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting quentity news eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	
    }

    @Override
    public NewsManagementVO showSingleNews(Long newsId) throws ServiceExeption {
	NewsManagementVO newsManagementVO= null;
	try {
	    newsManagementVO = newsDAO.showSinglNews(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Showing news eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	return newsManagementVO;
    }

    @Override
    public Long countAllNews() throws ServiceExeption {
	Long countNews = null;
	try {
	    countNews = newsDAO.countAllNews();
	} catch (DAOExeption e) {
	    LOGGER.error("Counting all news eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	return countNews;
    }

    @Override
    public List<NewsManagementVO> SearchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	List<NewsManagementVO> newsManagementVO = null;
	
	try {
	    newsManagementVO = newsDAO.searchNews(searchCriteria);
	} catch (DAOExeption e) {
	    LOGGER.error("searching news eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	
	return newsManagementVO;
    }

    @Override
    public List<Long> takeNewsIdList() throws ServiceExeption {
	List<Long> NewsIdList = null;
	try {
	    NewsIdList = newsDAO.takeNewsIdList();
	} catch (DAOExeption e) {
	    LOGGER.error("take news list eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	return NewsIdList;
    }

    @Override
    public Long countSearchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	Long countSearchNews = null;
	try {
	    countSearchNews = newsDAO.countSearchNews(searchCriteria);
	} catch (DAOExeption e) {
	    LOGGER.error("count search news eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	return countSearchNews;
    }

    public void setNewsDAO(INewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

}
