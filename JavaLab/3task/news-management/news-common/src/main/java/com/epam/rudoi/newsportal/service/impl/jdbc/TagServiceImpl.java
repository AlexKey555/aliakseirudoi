package com.epam.rudoi.newsportal.service.impl.jdbc;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.ITagService;

@Profile("jdbc")
@Transactional(rollbackFor = Exception.class)
public class TagServiceImpl implements ITagService {

    private static final Logger LOGGER = Logger.getLogger(TagServiceImpl.class);
    private ITagDAO tagDAO;

    public void setTagDAO(ITagDAO tagDAO) {
	this.tagDAO = tagDAO;
    }

    public Long addTag(NewsManagementVO newsManagementVO) throws ServiceExeption {
	Long tagId;
	try {
	    tagId = tagDAO.addItem(newsManagementVO.getTag());
	} catch (DAOExeption e) {
	    LOGGER.error("Linking tag with news exeption", e);
	    throw new ServiceExeption(e);
	}

	return tagId;
    }

    public void editTag(Tag tagItem) throws ServiceExeption {
	try {
	    tagDAO.editItem(tagItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing tag exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void deleteTag(Long tagId) throws ServiceExeption {
	try {
	    tagDAO.deleteItem(tagId);
	    tagDAO.unlinkByTagId(tagId);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting tag by tagId exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void link(List<Long> tagsIdList, Long newsId) throws ServiceExeption {
	try {
	    tagDAO.link(tagsIdList, newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Linking tag exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void unlink(Long newsId) throws ServiceExeption {
	try {
	    tagDAO.unlink(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking tag exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void unlink(List<Long> newsIdList) throws ServiceExeption {

	try {
	    tagDAO.unlink(newsIdList);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking tags with news exeption", e);
	    throw new ServiceExeption(e);
	}

    }

    public void unlinkByTagId(Long tagId) throws ServiceExeption {
	try {
	    tagDAO.unlinkByTagId(tagId);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking tag with news by tagId exeption", e);
	    throw new ServiceExeption(e);
	}

    }

    public List<Tag> getTagsList() throws ServiceExeption {
	List<Tag> tagsList = null;
	try {
	    tagsList = tagDAO.getTagsList();
	} catch (DAOExeption e) {
	    LOGGER.error("Getting tag list exception", e);
	    throw new ServiceExeption(e);
	}
	return tagsList;
    }

    public List<Long> getTagIdList() throws ServiceExeption {
	List<Long> tagsIdList = null;
	try {
	    tagsIdList = tagDAO.getTagsIdList();
	} catch (DAOExeption e) {
	    LOGGER.error("Getting tags id list exception", e);
	    throw new ServiceExeption(e);
	}
	return tagsIdList;
    }
}
