package com.epam.rudoi.newsportal.service.impl.jdbc;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.IAuthorService;

@Profile("jdbc")
@Transactional(rollbackFor = Exception.class)
public class AuthorServiceImpl implements IAuthorService {

    private static final Logger LOGGER = Logger.getLogger(AuthorServiceImpl.class);
    private IAuthorDAO authorDAO;

    public void setAuthorDAO(IAuthorDAO authorDAO) {
	this.authorDAO = authorDAO;
    }

    public Long addAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	Long authorId = null;
	try {
	    authorId = authorDAO.addItem(newsManagementVO.getAuthor());
	} catch (DAOExeption e) {
	    LOGGER.error("Adding author exeption", e);
	    throw new ServiceExeption(e);
	}

	return authorId;
    }

    public void editAuthor(Author authorItem) throws ServiceExeption {
	try {
	    authorDAO.editItem(authorItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing author exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void link(Long authorId, Long newsId) throws ServiceExeption {
	try {
	    authorDAO.link(authorId, newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Linking author with news exeption", e);
	    throw new ServiceExeption();
	}
    }

    public void unlink(Long newsId) throws ServiceExeption {
	try {
	    authorDAO.unlink(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking author with news exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void unlink(List<Long> newsIdList) throws ServiceExeption {
	try {
	    authorDAO.unlink(newsIdList);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking author with news exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void expiredAuthor(Long authorId) throws ServiceExeption {
	try {
	    authorDAO.expiredAuthor(authorId);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking author with news exeption", e);
	    throw new ServiceExeption(e);
	}

    }

    public List<Author> getAuthorsList() throws ServiceExeption {
	List<Author> authorsList = null;
	try {
	    authorsList = authorDAO.getAuthorsList();
	} catch (DAOExeption e) {
	    LOGGER.error("Geting authors list", e);
	    throw new ServiceExeption(e);
	}
	return authorsList;
    }

}
