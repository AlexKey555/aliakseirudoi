package com.epam.rudoi.newsportal.dao.impl.eclipseLink;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

import oracle.net.aso.s;

import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("eclipselink")
public class TagDAOImpl implements ITagDAO {

    @PersistenceUnit(unitName = "entityManagerFactory")
    private EntityManagerFactory emf;

    @Override
    public Long addItem(Tag tag) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();

	    entityManager.persist(tag);
	} finally {
	    entityTransaction.commit();
	}

	return tag.getTagId();
    }

    @Override
    public boolean editItem(Tag tag) throws DAOExeption {
	boolean success = true;
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();

	    entityManager.merge(tag);
	} finally {
	    entityTransaction.commit();
	}

	return success;
    }

    @Override
    public void deleteItem(Long tagId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    Tag tag = entityManager.find(Tag.class, tagId);
	    entityManager.remove(tag);
	} finally {
	    entityTransaction.commit();
	}

    }

    @Override
    public Tag readItem(Long tagId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	Tag tag = entityManager.find(Tag.class, tagId);

	return tag;
    }

    @Override
    public List<Tag> getTagsList() throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	List<Tag> tagsList = (List<Tag>) entityManager.createQuery("from TAGS t", Tag.class).getResultList();

	return tagsList;
    }

    @Override
    public List<Long> getTagsIdList() throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	List<Tag> tagsList = (List<Tag>) entityManager.createQuery("from TAGS t", Tag.class).getResultList();
	List<Long> tagsIdList = new ArrayList<Long>();
	for (Tag indexTag : tagsList) {
	    tagsIdList.add(indexTag.getTagId());
	}

	return tagsIdList;
    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void link(List<Long> tagIdList, Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlinkByTagId(Long tagId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

}
