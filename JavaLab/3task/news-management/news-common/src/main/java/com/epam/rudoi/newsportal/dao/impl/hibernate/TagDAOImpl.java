package com.epam.rudoi.newsportal.dao.impl.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("hibernate")
public class TagDAOImpl implements ITagDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Long addItem(Tag tag) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    session.save(tag);
	} finally {
	    tx.commit();
	    session.close();
	}

	return tag.getTagId();
    }

    @Override
    public boolean editItem(Tag tag) throws DAOExeption {
	boolean success = true;
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();

	    session.update(tag);

	} finally {
	    tx.commit();
	    session.close();
	}

	return success;
    }

    @Override
    public void deleteItem(Long tagId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();

	    Tag tag = (Tag) session.createCriteria(Tag.class).add(Restrictions.eq("tagId", tagId)).uniqueResult();
	    session.delete(tag);

	} finally {
	    tx.commit();
	    session.close();
	}

    }

    @Override
    public Tag readItem(Long tagId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Tag tag = null;
	try {
	    tag = (Tag) session.createCriteria(Tag.class).add(Restrictions.eq("tagId", tagId)).uniqueResult();
	} finally {
	    session.close();
	}

	return tag;
    }

    @Override
    public List<Tag> getTagsList() throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	List<Tag> tagsList = null;
	try {
	    tx = session.beginTransaction();
	    tagsList = session.createCriteria(Tag.class).list();

	} finally {
	    tx.commit();
	    session.close();
	}

	return tagsList;
    }

    @Override
    public List<Long> getTagsIdList() throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	List<Tag> tagsList = null;
	try {
	    tx = session.beginTransaction();
	     tagsList = session.createCriteria(Tag.class).list();
	} finally {
	    tx.commit();
	    session.close();
	}

	List<Long> tagsIdList = new ArrayList<Long>();
	for (Tag indexTag : tagsList) {
	    tagsIdList.add(indexTag.getTagId());
	}
	return tagsIdList;
    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void link(List<Long> tagIdList, Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlinkByTagId(Long tagId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

}
