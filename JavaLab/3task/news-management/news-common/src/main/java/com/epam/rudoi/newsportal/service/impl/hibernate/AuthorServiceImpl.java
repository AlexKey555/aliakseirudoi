package com.epam.rudoi.newsportal.service.impl.hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.IAuthorService;

@Profile("hibernate")
public class AuthorServiceImpl implements IAuthorService {

    private static final Logger LOGGER = Logger.getLogger(AuthorServiceImpl.class);
    
    private IAuthorDAO authorDAO;    

    @Override
    public Long addAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	Long authorId = null;
	try {
	    authorId = authorDAO.addItem(newsManagementVO.getAuthor());
	} catch (DAOExeption e) {
	    LOGGER.error("Adding author hibernate exeption", e);
	    throw new ServiceExeption(e);
	}

	return authorId;
    }

    @Override
    public void editAuthor(Author authorItem) throws ServiceExeption {
	try {
	    authorDAO.editItem(authorItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing author hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void link(Long authorId, Long newsId) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(Long newsId) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(List<Long> newsIdList) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void expiredAuthor(Long authorId) throws ServiceExeption {
	try {
	    authorDAO.expiredAuthor(authorId);
	} catch (DAOExeption e) {
	    LOGGER.error("Expired author hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public List<Author> getAuthorsList() throws ServiceExeption {
	 List<Author> authorsList = null;
	try {
	   authorsList = authorDAO.getAuthorsList();
	} catch (DAOExeption e) {
	    LOGGER.error("Geting authors list hibernate exception", e);
	    throw new ServiceExeption(e);
	}
	return authorsList;
    }
    public void setAuthorDAO(IAuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }
}	
