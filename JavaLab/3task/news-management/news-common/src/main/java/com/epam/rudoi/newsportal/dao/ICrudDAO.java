package com.epam.rudoi.newsportal.dao;

import com.epam.rudoi.newsportal.exeption.DAOExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface ICrudDAO.
 *
 * @param <E> the element type
 */
public interface ICrudDAO <E>{

	/**
	 * Adds the item.
	 * This method add entity
	 * @param entity the entity
	 * @return the long
	 * @throws DAOExeption the DAO exception
	 */
	Long addItem (E entity) throws DAOExeption;
	
	/**
	 * Edits the item.
	 * This method edit entity
	 * @param entity the entity
	 * @return TODO
	 * @throws DAOExeption the DAO exception
	 */
	boolean editItem (E entity) throws DAOExeption;
	
	/**
	 * Delete item.
	 * This method delete entity
	 * @param itemId the item id
	 * @throws DAOExeption the DAO exception
	 */
	void deleteItem (Long itemId) throws DAOExeption;
	
	/**
	 * Read item.
	 * This method read entity
	 * @param itemId the item id
	 * @return the e
	 * @throws DAOExeption the DAO exception
	 */
	E readItem(Long itemId) throws DAOExeption;
	
}
