package com.epam.rudoi.newsportal.dao.impl.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.search.Query;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("hibernate")
public class NewsDAOImpl implements INewsDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Long addItem(News news) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    session.save(news);
	} finally {
	    tx.commit();
	    session.close();
	}

	return news.getNewsId();
    }

    @Override
    public boolean editItem(News news) throws DAOExeption {
	boolean success = true;
	Session session = sessionFactory.openSession();
	try {
	    Transaction tx = session.beginTransaction();
	    session.update(news);
	    tx.commit();
	} catch (javax.persistence.OptimisticLockException e) {
	    success = false;
	} finally {
	    session.close();
	}

	return success;
    }

    @Override
    public void deleteItem(Long newsId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    News news = (News) session.createCriteria(News.class).add(Restrictions.eq("newsId", newsId)).uniqueResult();
	    session.delete(news);
	} finally {
	    tx.commit();
	    session.close();
	}

    }

    @Override
    public void deleteaQuantityNews(List<Long> newsIdList) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();

	    for (Long newsId : newsIdList) {
		News news = (News) session.createCriteria(News.class).add(Restrictions.eq("newsId", newsId))
			.uniqueResult();
		session.delete(news);
	    }
	} finally {
	    tx.commit();
	    session.close();
	}

    }

    @Override
    public News readItem(Long newsId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	News news = null;
	try {
	    news = (News) session.createCriteria(News.class).add(Restrictions.eq("newsId", newsId)).uniqueResult();
	} finally {
	    session.close();
	}

	return news;
    }

    @Override
    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	List<NewsManagementVO> newsManagementVOList = null;
	try {
	    tx = session.beginTransaction();

	    newsManagementVOList = new ArrayList<NewsManagementVO>();

	    int start = (int) (long) searchCriteria.getPageNumber();
	    int end = 10;

	    Criteria criteria = criteriaBuilder(searchCriteria, session, start, end);

	    List<News> newsList = criteria.list();

	    for (News news : newsList) {
		NewsManagementVO newManagementVO = new NewsManagementVO();
		newManagementVO.setNews(news);
		newManagementVO.setAuthor(news.getAuthorsList().get(0));
		newManagementVO.setCommentsList(news.getCommentsList());
		newManagementVO.setTagsList(news.getTagsList());

		newsManagementVOList.add(newManagementVO);
	    }
	} finally {
	    tx.commit();
	    session.close();
	}

	return newsManagementVOList;
    }

    private Criteria criteriaBuilder(SearchCriteria searchCriteria, Session session, int start, int end) {

	Long authorId = null;
	List<Long> tagsIdList = null;
	if (searchCriteria != null) {
	    authorId = searchCriteria.getAuthor().getAuthorId();
	    tagsIdList = searchCriteria.getTagsIdList();
	}
	int maxResults = end - start;
	Conjunction conjunction = Restrictions.conjunction();
	Criteria criteria = session.createCriteria(News.class);

	if (authorId != null && authorId != 0L) {
	    criteria.createAlias("authorsList", "author");
	    conjunction.add(Restrictions.eq("author.authorId", authorId));

	}
	if (tagsIdList != null && tagsIdList.size() != 0) {
	    criteria.createAlias("tagsList", "tag");
	    conjunction.add(Restrictions.in("tag.tagId", tagsIdList));
	}
	criteria.add(conjunction);
	criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	// criteria.addOrder(Property.forName("commentsCount").desc());
	criteria.addOrder(Property.forName("newsModificationDate").desc());
	criteria.setFirstResult(start);
	criteria.setMaxResults(maxResults);

	return criteria;
    }

    @Override
    public NewsManagementVO showSinglNews(Long newsId) throws DAOExeption {

	News news = readItem(newsId);
	NewsManagementVO newsManagementVO = new NewsManagementVO();

	Author author = new Author();
	author.setAuthorName(news.getAuthorsList().get(0).getAuthorName());
	newsManagementVO.setAuthor(author);
	newsManagementVO.setNews(news);
	return newsManagementVO;
    }

    @Override
    public Long countAllNews() throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	Long countNews = null;
	try {
	    tx = session.beginTransaction();
	    countNews = (Long) session.createCriteria(News.class).setProjection(Projections.rowCount()).uniqueResult();
	} finally {
	    tx.commit();
	    session.close();
	}

	return countNews;
    }

    @Override
    public List<Long> takeNewsIdList() throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	List<News> newsList = null;
	try {
	    tx = session.beginTransaction();
	    newsList = session.createCriteria(News.class).list();

	} finally {
	    tx.commit();
	    session.close();
	}

	List<Long> newsIdList = new ArrayList<Long>();
	for (News indexTag : newsList) {
	    newsIdList.add(indexTag.getNewsId());
	}

	return newsIdList;
    }

    @Override
    public Long countSearchNews(SearchCriteria searchCriteria) throws DAOExeption {
	Long totalCount = null;

	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    Criteria criteria = createSearchCriteria(searchCriteria, session);
	    totalCount = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
	} finally {
	    tx.commit();
	    session.close();
	}

	return totalCount;
    }

    private Criteria createSearchCriteria(SearchCriteria searchCriteria, Session session) {

	Long authorId = null;
	List<Long> tagsIdList = null;
	if (searchCriteria != null) {
	    authorId = searchCriteria.getAuthor().getAuthorId();
	    tagsIdList = searchCriteria.getTagsIdList();
	}
	Conjunction conjunction = Restrictions.conjunction();
	Criteria criteria = session.createCriteria(News.class);

	if (authorId != null && authorId != 0L) {
	    criteria.createAlias("authorsList", "author");
	    conjunction.add(Restrictions.eq("author.authorId", authorId));
	}
	if (tagsIdList != null && tagsIdList.size() != 0) {
	    criteria.createAlias("tagsList", "tag");
	    conjunction.add(Restrictions.in("tag.tagId", tagsIdList));
	}
	criteria.add(conjunction);
	criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	// criteria.addOrder(Property.forName("commentsCount").desc());
	criteria.addOrder(Property.forName("newsModificationDate").desc());

	return criteria;
    }

    public List<NewsManagementVO> getCurrentSearchNewsList(SearchCriteria searchCriteria) {
	List<News> currentNewsList = null;
	List<NewsManagementVO> newsManagementVOList = new ArrayList<NewsManagementVO>();

	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    Criteria criteria = createSearchCriteria(searchCriteria, session);
	    currentNewsList = criteria.list();
	} finally {
	    tx.commit();
	    session.close();
	}

	for (News news : currentNewsList) {
	    NewsManagementVO newManagementVO = new NewsManagementVO();
	    newManagementVO.setNews(news);
	    newManagementVO.setAuthor(news.getAuthorsList().get(0));
	    newManagementVO.setCommentsList(news.getCommentsList());
	    newManagementVO.setTagsList(news.getTagsList());

	    newsManagementVOList.add(newManagementVO);
	}

	return newsManagementVOList;
    }

}
