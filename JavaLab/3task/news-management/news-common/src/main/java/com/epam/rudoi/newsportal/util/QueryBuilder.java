package com.epam.rudoi.newsportal.util;

import java.text.MessageFormat;
import java.util.List;

import com.epam.rudoi.newsportal.entity.SearchCriteria;

public class QueryBuilder {

    public static String buildQueryByParam(SearchCriteria searchCriteria, String queryAuthorsAndTags, String queryOnlyTags) {

	String searchNewsTagsJoin = null;

	Long haveAuthorId = searchCriteria.getAuthor().getAuthorId();
	List<Long> haveTagsIdList = searchCriteria.getTagsIdList();

	if (haveAuthorIdAndTagsIdList(haveAuthorId, haveTagsIdList)) {

	    searchNewsTagsJoin = buildQueryByParamHelper(queryAuthorsAndTags, searchCriteria);

	} else if (haveOnlyTagsIdListForQuearyBuilder(haveAuthorId, haveTagsIdList)) {

	    searchNewsTagsJoin = buildQueryByParamHelper(queryOnlyTags, searchCriteria);
	}

	return searchNewsTagsJoin;
    }

    public static String buildQueryForStatment(SearchCriteria searchCriteria, String mainQuery, String mFormatAuthorJoin,
	    String mFormatGag, String searchNewsTagsJoin) {

	String querySearchNews = null;

	Long haveAuthorId = searchCriteria.getAuthor().getAuthorId();
	List<Long> haveTagsIdList = searchCriteria.getTagsIdList();

	if (haveNoParam(haveAuthorId, haveTagsIdList)) {

	    querySearchNews = MessageFormat.format(mainQuery, mFormatGag, mFormatGag);
	    
	} else if (haveAuthorIdAndTagsIdList(haveAuthorId, haveTagsIdList)) {
		querySearchNews = MessageFormat.format(mainQuery, searchNewsTagsJoin, mFormatGag);

	    } else if (haveOnlyAuthorId(haveAuthorId)) {
		    querySearchNews = MessageFormat.format(mainQuery, mFormatAuthorJoin, mFormatGag);

		} else  if (haveOnlyTagsIdList(haveTagsIdList)) {
			querySearchNews = MessageFormat.format(mainQuery, searchNewsTagsJoin, mFormatGag);

	    }

	return querySearchNews;
    }

    public String buildQueryForStatment(String query, List<Long> newsIdList) {

	String completeQueryParam = buildQueryParams(newsIdList);
	String completeQuery = buiderQuearyWithAnyParam(query, completeQueryParam);
	return completeQuery;
    }

    private String buiderQuearyWithAnyParam(String query, String completeQueryParam) {

	StringBuilder prepareQuery = new StringBuilder(query);
	prepareQuery.append(completeQueryParam);
	prepareQuery.append(")");
	String completeQuery = prepareQuery.toString();

	return completeQuery;
    }

    private static String buildQueryByParamHelper(String query, SearchCriteria searchCriteria) {

	String paramQueryTags = buildQueryParams(searchCriteria.getTagsIdList());
	StringBuilder queryTags = new StringBuilder(query);
	queryTags.append(paramQueryTags);
	queryTags.append(")");
	String searchNewsTagsJoin = queryTags.toString();

	return searchNewsTagsJoin;
    }

    private static String buildQueryParams(List<Long> entityIdList) {

	StringBuilder resultQueryTags = new StringBuilder();
	for (Long tagId : entityIdList) {

	    resultQueryTags.append("?, ");
	}

	resultQueryTags.delete(resultQueryTags.length() - 2, resultQueryTags.length());

	return resultQueryTags.toString();
    }

    private static boolean haveOnlyTagsIdListForQuearyBuilder(Long haveAuthorId, List<Long> haveTagsIdList) {
	return (haveAuthorId == null || haveAuthorId == 0) & haveOnlyTagsIdList(haveTagsIdList);
    }

    private static boolean haveOnlyTagsIdList(List<Long> haveTagsIdList) {
	return haveTagsIdList != null;
    }

    private static boolean haveOnlyAuthorId(Long haveAuthorId) {
	return haveAuthorId != null && haveAuthorId != 0;
    }

    private static boolean haveAuthorIdAndTagsIdList(Long haveAuthorId, List<Long> haveTagsIdList) {
	return haveOnlyAuthorId(haveAuthorId) & haveOnlyTagsIdList(haveTagsIdList);
    }

    private static boolean haveNoParam(Long haveAuthorId, List<Long> haveTagsIdList) {
	return (haveAuthorId == null || haveAuthorId == 0) & haveTagsIdList == null;
    }

}
