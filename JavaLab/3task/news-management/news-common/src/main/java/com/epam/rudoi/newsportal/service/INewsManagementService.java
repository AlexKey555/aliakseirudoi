package com.epam.rudoi.newsportal.service;

import java.util.List;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface INewsManagementService.
 */
public interface INewsManagementService {
	
	

	/**
	 * Adds the news.
	 * This method add news
	 * @param newsManagementVO the news management vo
	 * @throws ServiceExeption the service exception
	 */
	void addNews(NewsManagementVO newsManagementVO) throws ServiceExeption;

	/**
	 * Edits the news.
	 * This method edit news
	 * @param newsManagementVO the news management vo
	 * @return TODO
	 * @throws ServiceExeption the service exception
	 */
	boolean editNews(NewsManagementVO newsManagementVO) throws ServiceExeption;

	/**
	 * Delete news.
	 * This method delete news
	 * @param newsManagementVO the news management vo
	 * @throws ServiceExeption the service exception
	 */
	void deleteNews(NewsManagementVO newsManagementVO) throws ServiceExeption;

	/**
	 * Search news.
	 * This method search news by params
	 * @param searchCriteria the search criteria
	 * @return the list
	 * @throws ServiceExeption the service exception
	 */
	List<NewsManagementVO> searchNews(SearchCriteria searchCriteria)
			throws ServiceExeption;

	/**
	 * Show single news.
	 * This method show one news
	 * @param newsId the news id
	 * @return the news management vo
	 * @throws ServiceExeption the service exception
	 */
	NewsManagementVO showSinglNews(Long newsId) throws ServiceExeption;
	
	/**
	 * Get current search news list.
	 * This method Get current search news list
	 * @return list NewsManagementVO 
	 * @throws DAOExeption the DAO exception
	 *//*
	List<NewsManagementVO> getCurrentSearchNewsList(SearchCriteria searchCriteria) throws ServiceExeption;*/

	/**
	 * Count all news.
	 * This method count all news
	 * @return the long
	 * @throws ServiceExeption the service exception
	 */
	Long countAllNews() throws ServiceExeption;
	
	/**
	 * Take news id list.
	 * This method take news id list
	 * @return the list
	 * @throws ServiceExeption the service exception
	 */
	List<Long> takeNewsIdList () throws ServiceExeption;
	
	/**
	 * Count all find news.
	 * This method count all news that find
	 * @return the long 
	 * @throws DAOExeption the DAO exception
	 */
	Long countSearchNews (SearchCriteria searchCriteria) throws ServiceExeption;
	
	/**
	 * Adds the comment.
	 * This method add comment
	 * @param commentItem the comment item
	 * @return the long
	 * @throws ServiceExeption the service exception
	 */
	Long addComment(Comment commentItem) throws ServiceExeption;

	/**
	 * Delete comments.
	 * This method delete comment
	 * @param commentId the comment id
	 * @throws ServiceExeption the service exception
	 */
	void deleteComments(Long commentId) throws ServiceExeption;
	
	/**
	 * Gets the comments list.
	 * This method get comments list
	 * @param newsId the news id
	 * @return the comments list
	 * @throws ServiceExeption the service exception
	 */
	List<Comment> getCommentsList(Long newsId) throws ServiceExeption;
	
	/**
	 * Delete news comments.
	 * This method delete news comments
	 * @param newsId the news id
	 * @throws ServiceExeption the service exception
	 */
	void deleteNewsComments(Long newsId) throws ServiceExeption;
	
	

	/**
	 * Adds the tag.
	 * This method add tag
	 * @param newsManagementVO the news management vo
	 * @throws ServiceExeption the service exception
	 */
	void addTag(NewsManagementVO newsManagementVO) throws ServiceExeption;

	/**
	 * Delete tag.
	 * This method delete tag
	 * @param tagId the tag id
	 * @throws ServiceExeption the service exception
	 */
	void deleteTag(Long tagId) throws ServiceExeption;
	
	/**
	 * Gets the tags list.
	 * This method get tags list
	 * @return the tags list
	 * @throws ServiceExeption the service exception
	 */
	List<Tag> getTagsList ()  throws ServiceExeption;
		
	/**
	 * Gets the tag id list.
	 * This method get tag id list
	 * @return the tag id list
	 * @throws ServiceExeption the service exception
	 */
	List<Long> getTagIdList () throws ServiceExeption;
	
	/**
	 * Edits the tag.
	 * This method edit tag
	 * @param newsManagementVO the news management vo
	 * @throws ServiceExeption the service exception
	 */
	void editTag(NewsManagementVO newsManagementVO) throws ServiceExeption;
	
	
	
	/**
	 * Gets the authors list.
	 * This method  get authors list
	 * @return the authors list
	 * @throws ServiceExeption the service exception
	 */
	List<Author> getAuthorsList ()  throws ServiceExeption;

	/**
	 * Adds the author.
	 * This method add author
	 * @param newsManagementVO the news management vo
	 * @throws ServiceExeption the service exception
	 */
	void addAuthor(NewsManagementVO newsManagementVO)  throws ServiceExeption; 
	
	/**
	 * Edits the author.
	 * This method edit author
	 * @param newsManagementVO the news management vo
	 * @throws ServiceExeption the service exception
	 */
	void editAuthor (NewsManagementVO newsManagementVO)  throws ServiceExeption; 
	
	/**
	 * Expired author.
	 * This method expired author
	 * @param newsManagementVO the news management vo
	 * @throws ServiceExeption the service exception
	 */
	void expiredAuthor (NewsManagementVO newsManagementVO)  throws ServiceExeption; 

}
