package com.epam.rudoi.newsportal.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.Min;

import org.eclipse.persistence.annotations.PrivateOwned;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;


@Entity
@Table(name="NEWS")
@SequenceGenerator(name="SQ_NEWS_ID_SEQ", sequenceName = "SQ_NEWS_ID_SEQ", allocationSize=1)
public class News {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SQ_NEWS_ID_SEQ")
	@Column(name="NEWS_ID",nullable=false,precision=20)
	private Long newsId;
	
	@Column(name="TITLE",nullable=false,precision=30)
	private String newsTitle;
	
	@Column(name="SHORT_TEXT",nullable=false,precision=100)
	private String newsShortText;
	
	@Column(name="FULL_TEXT",nullable=false,precision=2000)
	private String newsFullText;
	
	@Column(name="CREATION_DATE",nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date newsCreationDate;
	
	@Column(name="MODIFICATION_DATE",nullable=false)
	@Temporal(TemporalType.DATE)
	private Date newsModificationDate;
	
	@Version
	@Column(name="VERSION", nullable=false, precision=20)
	private Long version;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@PrivateOwned
	@OneToMany(targetEntity=com.epam.rudoi.newsportal.entity.Comment.class, /*cascade=CascadeType.ALL,*/ orphanRemoval=true)
	@JoinColumn(name="NEWS_ID",referencedColumnName="NEWS_ID", updatable=false)
	private List<Comment> commentsList;
	
	@LazyCollection(value = LazyCollectionOption.FALSE)
	@ManyToMany()
	@JoinTable(name = "NEWS_TAGS", joinColumns = { @JoinColumn(name = "NEWS_ID",
	referencedColumnName = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "TAG_ID",
	referencedColumnName = "TAG_ID") })
	private List<Tag> tagsList;
	
	 @LazyCollection(LazyCollectionOption.FALSE)
	 @ManyToMany
	 @JoinTable(name = "NEWS_AUTHORS", joinColumns = { @JoinColumn(name = "NEWS_ID",
	 referencedColumnName = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "AUTHOR_ID",
	 referencedColumnName = "AUTHOR_ID") })
	 private List<Author> authorsList;
	 
	 @Transient
	 @Min(value = 1/*, message="{local.error.message.emptyAuthor}"*/ )
	 private Long authorId;
	 
	 @Transient
	 private List<Long>tagsIdList ; 
	 
	@Transient
	private Long newsCount;
	
	@Transient
	private Long newsCommentAmmount;
	
	public News() {
	    super();
	}
	
	public News(Long newsId) {
	    super();
	    this.newsId = newsId;
	}
	
	public News(Long newsId, String newsTitle, String newsShortText, String newsFullText) {
	    super();
	    this.newsId = newsId;
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	}
	
	public News(Long newsId, String newsTitle, String newsShortText, String newsFullText, Date newsCreationDate, Date newsModificationDate) {
	    super();
	    this.newsId = newsId;
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	    this.newsCreationDate = newsCreationDate;
	    this.newsModificationDate = newsModificationDate;
	}
	
	public News(Long newsId, String newsTitle, String newsShortText, String newsFullText, Long newsCount, Long newsCommentAmmount) {
	    super();
	    this.newsId = newsId;
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	    this.newsCount = newsCount;
	    this.newsCommentAmmount = newsCommentAmmount;
	}
	
	public News(String newsTitle, String newsShortText, String newsFullText) {
	    super();
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	}

	public Long getNewsId() {
	    return newsId;
	}

	public void setNewsId(Long newsId) {
	    this.newsId = newsId;
	}

	public String getNewsTitle() {
	    return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
	    this.newsTitle = newsTitle;
	}

	public String getNewsShortText() {
	    return newsShortText;
	}

	public void setNewsShortText(String newsShortText) {
	    this.newsShortText = newsShortText;
	}

	public String getNewsFullText() {
	    return newsFullText;
	}

	public void setNewsFullText(String newsFullText) {
	    this.newsFullText = newsFullText;
	}

	public Date getNewsCreationDate() {
	    return newsCreationDate;
	}

	public void setNewsCreationDate(Date newsCreationDate) {
	    this.newsCreationDate = newsCreationDate;
	}

	public Date getNewsModificationDate() {
	    return newsModificationDate;
	}

	public void setNewsModificationDate(Date newsModificationDate) {
	    this.newsModificationDate = newsModificationDate;
	}
	
	public Long getVersion() {
	    return version;
	}

	public void setVersion(Long version) {
	    this.version = version;
	}

	public List<Comment> getCommentsList() {
	    return commentsList;
	}

	public void setCommentsList(List<Comment> commentsList) {
	    this.commentsList = commentsList;
	}

	public List<Tag> getTagsList() {
	    return tagsList;
	}

	public void setTagsList(List<Tag> tagsList) {
	    this.tagsList = tagsList;
	}

	public List<Author> getAuthorsList() {
	    return authorsList;
	}

	public void setAuthorsList(List<Author> authorsList) {
	    this.authorsList = authorsList;
	}

	public Long getAuthorId() {
	    return authorId;
	}

	public void setAuthorId(Long authorId) {
	    this.authorId = authorId;
	}

	public List<Long> getTagsIdList() {
	    return tagsIdList;
	}

	public void setTagsIdList(List<Long> tagsIdList) {
	    this.tagsIdList = tagsIdList;
	}

	public Long getNewsCount() {
	    return newsCount;
	}

	public void setNewsCount(Long newsCount) {
	    this.newsCount = newsCount;
	}

	public Long getNewsCommentAmmount() {
	    return newsCommentAmmount;
	}

	public void setNewsCommentAmmount(Long newsCommentAmmount) {
	    this.newsCommentAmmount = newsCommentAmmount;
	}

	

	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
	    result = prime * result + ((authorsList == null) ? 0 : authorsList.hashCode());
	    result = prime * result + ((commentsList == null) ? 0 : commentsList.hashCode());
	    result = prime * result + ((newsCommentAmmount == null) ? 0 : newsCommentAmmount.hashCode());
	    result = prime * result + ((newsCount == null) ? 0 : newsCount.hashCode());
	    result = prime * result + ((newsCreationDate == null) ? 0 : newsCreationDate.hashCode());
	    result = prime * result + ((newsFullText == null) ? 0 : newsFullText.hashCode());
	    result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
	    result = prime * result + ((newsModificationDate == null) ? 0 : newsModificationDate.hashCode());
	    result = prime * result + ((newsShortText == null) ? 0 : newsShortText.hashCode());
	    result = prime * result + ((newsTitle == null) ? 0 : newsTitle.hashCode());
	    result = prime * result + ((tagsIdList == null) ? 0 : tagsIdList.hashCode());
	    result = prime * result + ((tagsList == null) ? 0 : tagsList.hashCode());
	    result = prime * result + ((version == null) ? 0 : version.hashCode());
	    return result;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    News other = (News) obj;
	    if (authorId == null) {
		if (other.authorId != null)
		    return false;
	    } else if (!authorId.equals(other.authorId))
		return false;
	    if (authorsList == null) {
		if (other.authorsList != null)
		    return false;
	    } else if (!authorsList.equals(other.authorsList))
		return false;
	    if (commentsList == null) {
		if (other.commentsList != null)
		    return false;
	    } else if (!commentsList.equals(other.commentsList))
		return false;
	    if (newsCommentAmmount == null) {
		if (other.newsCommentAmmount != null)
		    return false;
	    } else if (!newsCommentAmmount.equals(other.newsCommentAmmount))
		return false;
	    if (newsCount == null) {
		if (other.newsCount != null)
		    return false;
	    } else if (!newsCount.equals(other.newsCount))
		return false;
	    if (newsCreationDate == null) {
		if (other.newsCreationDate != null)
		    return false;
	    } else if (!newsCreationDate.equals(other.newsCreationDate))
		return false;
	    if (newsFullText == null) {
		if (other.newsFullText != null)
		    return false;
	    } else if (!newsFullText.equals(other.newsFullText))
		return false;
	    if (newsId == null) {
		if (other.newsId != null)
		    return false;
	    } else if (!newsId.equals(other.newsId))
		return false;
	    if (newsModificationDate == null) {
		if (other.newsModificationDate != null)
		    return false;
	    } else if (!newsModificationDate.equals(other.newsModificationDate))
		return false;
	    if (newsShortText == null) {
		if (other.newsShortText != null)
		    return false;
	    } else if (!newsShortText.equals(other.newsShortText))
		return false;
	    if (newsTitle == null) {
		if (other.newsTitle != null)
		    return false;
	    } else if (!newsTitle.equals(other.newsTitle))
		return false;
	    if (tagsIdList == null) {
		if (other.tagsIdList != null)
		    return false;
	    } else if (!tagsIdList.equals(other.tagsIdList))
		return false;
	    if (tagsList == null) {
		if (other.tagsList != null)
		    return false;
	    } else if (!tagsList.equals(other.tagsList))
		return false;
	    if (version == null) {
		if (other.version != null)
		    return false;
	    } else if (!version.equals(other.version))
		return false;
	    return true;
	}

	@Override
	public String toString() {
	    return "News [newsId=" + newsId + ", newsTitle=" + newsTitle + ", newsShortText=" + newsShortText
		    + ", newsFullText=" + newsFullText + ", newsCreationDate=" + newsCreationDate
		    + ", newsModificationDate=" + newsModificationDate + ", version=" + version + ", commentsList="
		    + commentsList + ", tagsList=" + tagsList + ", authorsList=" + authorsList + ", authorId="
		    + authorId + ", tagsIdList=" + tagsIdList + ", newsCount=" + newsCount + ", newsCommentAmmount="
		    + newsCommentAmmount + "]";
	}


}
