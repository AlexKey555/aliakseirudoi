package com.epam.rudoi.newsportal.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "COMMENTS")
@Table(name = "COMMENTS")
@SequenceGenerator(name = "SQ_COMMENT_ID_SEQ",sequenceName="SQ_COMMENT_ID_SEQ", allocationSize = 1)
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_COMMENT_ID_SEQ")
    @Column(name = "COMMENT_ID", nullable = false, precision = 20)
    private Long commentId;

    @PrimaryKeyJoinColumn
    @Column(name = "NEWS_ID", nullable = false,  precision = 20)
    private Long newsId;

    @Column(name = "COMMENT_TEXT", nullable = false, precision = 100)
    private String commentText;

    @Column(name = "CREATION_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    /*@ManyToOne
    @JoinColumn(name = "NEWS_ID", nullable = false, insertable = false, updatable = false)
    private News news;*/

    public Comment() {
	super();
    }

    public Comment(Long commentId, Long newsId, String commentText, Date creationDate) {
	super();
	this.commentId = commentId;
	this.newsId = newsId;
	this.commentText = commentText;
	this.creationDate = creationDate;
    }

    public Comment(Long commentId, Long newsId, String commentText) {
	super();
	this.commentId = commentId;
	this.newsId = newsId;
	this.commentText = commentText;
    }

    public Comment(Long newsId, String commentText) {
	super();
	this.newsId = newsId;
	this.commentText = commentText;
    }

    public Long getCommentId() {
	return commentId;
    }

    public void setCommentId(Long commentId) {
	this.commentId = commentId;
    }

    public Long getNewsId() {
	return newsId;
    }

    public void setNewsId(Long newsId) {
	this.newsId = newsId;
    }

    public String getCommentText() {
	return commentText;
    }

    public void setCommentText(String commentText) {
	this.commentText = commentText;
    }

    public Date getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
	result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
	result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
	result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Comment other = (Comment) obj;
	if (commentId == null) {
	    if (other.commentId != null)
		return false;
	} else if (!commentId.equals(other.commentId))
	    return false;
	if (commentText == null) {
	    if (other.commentText != null)
		return false;
	} else if (!commentText.equals(other.commentText))
	    return false;
	if (creationDate == null) {
	    if (other.creationDate != null)
		return false;
	} else if (!creationDate.equals(other.creationDate))
	    return false;
	if (newsId == null) {
	    if (other.newsId != null)
		return false;
	} else if (!newsId.equals(other.newsId))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Comment [commentId=" + commentId + ", newsId=" + newsId + ", commentText=" + commentText
		+ ", creationDate=" + creationDate + "]";
    }

}
