package com.epam.rudoi.newsportal.service.impl.eclipseLink;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.ITagService;

@Profile("eclipselink")
public class TagServiceImpl implements ITagService{
    
    private static final Logger LOGGER = Logger.getLogger(TagServiceImpl.class);   

    private ITagDAO tagDAO;

    @Override
    public Long addTag(NewsManagementVO managementVO) throws ServiceExeption {
	Long tagId = null;
	try {
	    tagId = tagDAO.addItem(managementVO.getTag());
	} catch (DAOExeption e) {
	    LOGGER.error("Linking tag with news eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	
	return tagId;
    }

    @Override
    public void editTag(Tag tagItem) throws ServiceExeption {
	try {
	    tagDAO.editItem(tagItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing tag eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceExeption {
	try {
	    tagDAO.deleteItem(tagId);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting tag by tagId eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void link(List<Long> tagsIdList, Long newsId) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(Long newsId) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(List<Long> newsIdList) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlinkByTagId(Long tagId) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public List<Tag> getTagsList() throws ServiceExeption {
	List<Tag> tagsList = null;
	try {
	    tagsList = tagDAO.getTagsList();
	} catch (DAOExeption e) {
	    LOGGER.error("Getting tag list eclipseLink exception", e);
	    throw new ServiceExeption(e);
	}
	    
	    return tagsList;
    }

    @Override
    public List<Long> getTagIdList() throws ServiceExeption {
	List<Long> tagsIdList = null;
	try {
	    tagsIdList = tagDAO.getTagsIdList();
	} catch (DAOExeption e) {
	    LOGGER.error("Getting tags id list eclipseLink exception", e);
	    throw new ServiceExeption(e);
	}
	
	return tagsIdList;
    }

    public void setTagDAO(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }
    
}
