package com.epam.rudoi.newsportal.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("hibernate")
public class CommentDAOImpl implements ICommentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Long addItem(Comment comment) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    session.save(comment);

	} finally {
	    tx.commit();
	    session.close();
	}

	return comment.getCommentId();
    }

    @Override
    public boolean editItem(Comment comment) throws DAOExeption {
	boolean success = true;
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    session.update(comment);
	} finally {
	    tx.commit();
	    session.close();
	}

	return success;
    }

    @Override
    public void deleteItem(Long commentId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    Comment comment = (Comment) session.createCriteria(Comment.class)
		    .add(Restrictions.eq("commentId", commentId)).uniqueResult();
	    session.delete(comment);
	} finally {
	    tx.commit();
	    session.close();
	}

    }

    @Override
    public Comment readItem(Long commentId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Comment comment = null;
	try {
	    comment = (Comment) session.createCriteria(Comment.class).add(Restrictions.eq("commentId", commentId))
		    .uniqueResult();

	} finally {
	    session.close();
	}

	return comment;
    }

    @Override
    public List<Comment> getCommentList(Long newsId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	List<Comment> commentsList = null;
	try {
	    tx = session.beginTransaction();
	    commentsList = session.createCriteria(Comment.class).add(Restrictions.eq("newsId", newsId)).list();
	} finally {
	    tx.commit();
	    session.close();
	}

	return commentsList;
    }

    @Override
    public void deleteNewsComments(Long newsId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    Comment comment = (Comment) session.createCriteria(Comment.class).add(Restrictions.eq("newsId", newsId))
		    .uniqueResult();
	    session.delete(comment);
	} finally {
	    tx.commit();
	    session.close();
	}

    }

    @Override
    public void unlink(Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

}
