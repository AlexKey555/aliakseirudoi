package com.epam.rudoi.newsportal.service.impl.hibernate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.Transactional;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.IAuthorService;
import com.epam.rudoi.newsportal.service.ICommentService;
import com.epam.rudoi.newsportal.service.INewsManagementService;
import com.epam.rudoi.newsportal.service.INewsService;
import com.epam.rudoi.newsportal.service.ITagService;

@Profile("hibernate")
@Transactional(rollbackFor = Exception.class)
public class ServiceManagerImpl implements INewsManagementService{
    
 
    private IAuthorService authorService;
   
    private ICommentService commentService;
    
    private INewsService newsService;   

    private ITagService tagService;
    
    @Override
    public void addNews(NewsManagementVO newsManagementVO) throws ServiceExeption {
	newsService.addNews(newsManagementVO.getNews());
	
    }

    @Override
    public boolean editNews(NewsManagementVO newsManagementVO) throws ServiceExeption {
	return newsService.editNews(newsManagementVO.getNews());
	
    }

    @Override
    public void deleteNews(NewsManagementVO newsManagementVO) throws ServiceExeption {
	newsService.deleteaQuantityNews(newsManagementVO.getNewsIdList());
    }

    @Override
    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	List<NewsManagementVO> newsVOList = newsService.SearchNews(searchCriteria);  
		
	return newsVOList;
    }

    @Override
    public NewsManagementVO showSinglNews(Long newsId) throws ServiceExeption {
	NewsManagementVO newsManagementVO = newsService.showSingleNews(newsId);
	return newsManagementVO;
    }
    
    @Override
    public Long countAllNews() throws ServiceExeption {
	Long newsCount = newsService.countAllNews();
	return newsCount;
    }

    @Override
    public List<Long> takeNewsIdList() throws ServiceExeption {
	List<Long> newsIdList = newsService.takeNewsIdList();
	return newsIdList;
    }
    
    @Override
    public Long countSearchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	Long countNews = newsService.countSearchNews(searchCriteria);
	return countNews;
    }

    @Override
    public Long addComment(Comment commentItem) throws ServiceExeption {
	Long commentId = commentService.addComment(commentItem);
	return commentId;
    }

    @Override
    public void deleteComments(Long commentId) throws ServiceExeption {
	commentService.deleteComment(commentId);
	
    }

    @Override
    public List<Comment> getCommentsList(Long newsId) throws ServiceExeption {
	List<Comment> comentsList = commentService.getCommentList(newsId);
	return comentsList;
    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceExeption {
	commentService.deleteNewsComments(newsId);
	
    }

    @Override
    public void addTag(NewsManagementVO newsManagementVO) throws ServiceExeption {
	tagService.addTag(newsManagementVO);
	
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceExeption {
	tagService.deleteTag(tagId);
	
    }

    @Override
    public List<Tag> getTagsList() throws ServiceExeption {
	List<Tag> tagsList = tagService.getTagsList();
	return tagsList;
    }

    @Override
    public List<Long> getTagIdList() throws ServiceExeption {
	List<Long> tagsIdList = tagService.getTagIdList();
	return tagsIdList;
    }

    @Override
    public void editTag(NewsManagementVO newsManagementVO) throws ServiceExeption {
	tagService.editTag(newsManagementVO.getTag());
	
    }

    @Override
    public List<Author> getAuthorsList() throws ServiceExeption {
	List<Author> authorsList = authorService.getAuthorsList();
	return authorsList;
    }

    @Override
    public void addAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	authorService.addAuthor(newsManagementVO);
	
    }

    @Override
    public void editAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	authorService.editAuthor(newsManagementVO.getAuthor());
	
    }

    @Override
    public void expiredAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	authorService.expiredAuthor(newsManagementVO.getAuthor().getAuthorId());
	
    }
    
    public void setAuthorService(IAuthorService authorService) {
        this.authorService = authorService;
    }

    public void setCommentService(ICommentService commentService) {
        this.commentService = commentService;
    }

    public void setNewsService(INewsService newsService) {
        this.newsService = newsService;
    }

    public void setTagService(ITagService tagService) {
        this.tagService = tagService;
    }


}
