package com.epam.rudoi.newsportal.dao;

import java.util.List;

import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface INewsDAO.
 */
public interface INewsDAO extends ICrudDAO<News> {

	/**
	 * Search news.
	 * This method search news by params
	 * @param searchCriteria the search criteria
	 * @return the list
	 * @throws DAOExeption the DAO exception
	 */
	List<NewsManagementVO> searchNews (SearchCriteria searchCriteria) throws DAOExeption;
	
	/**
	 * Show single news.
	 * This method show one news  
	 * @param newsId the news id
	 * @return the news management vo
	 * @throws DAOExeption the DAO exception
	 */
	NewsManagementVO showSinglNews (Long newsId) throws DAOExeption;
	
	/**
	 * Count all news.
	 * This method count all news
	 * @return the long
	 * @throws DAOExeption the DAO exception
	 */
	Long countAllNews() throws DAOExeption;
	
	/**
	 * Deletea quantity news.
	 *
	 * @param newsIdList the news id list
	 * @throws DAOExeption the DAO exception
	 */
	void deleteaQuantityNews (List<Long> newsIdList) throws DAOExeption;
	
	/**
	 * Take news id list.
	 * This method take news id list
	 * @return the list
	 * @throws DAOExeption the DAO exception
	 */
	List<Long> takeNewsIdList () throws DAOExeption;
	
	/**
	 * Count all find news.
	 * This method count all news that find
	 * @return the long 
	 * @throws DAOExeption the DAO exception
	 */
	Long countSearchNews (SearchCriteria searchCriteria) throws DAOExeption;
	
	/**
	 * Get current search news list.
	 * This method Get current search news list
	 * @return list NewsManagementVO 
	 * @throws DAOExeption the DAO exception
	 *//*
	List<NewsManagementVO> getCurrentSearchNewsList(SearchCriteria searchCriteria) throws DAOExeption;*/
	
	
}
