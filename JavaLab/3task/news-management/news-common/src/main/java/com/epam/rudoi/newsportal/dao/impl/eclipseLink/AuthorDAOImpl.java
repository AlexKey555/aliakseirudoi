package com.epam.rudoi.newsportal.dao.impl.eclipseLink;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("eclipselink")
public class AuthorDAOImpl implements IAuthorDAO {

    @PersistenceUnit(unitName = "entityManagerFactory")
    private EntityManagerFactory emf;

    @Override
    public Long addItem(Author author) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    entityManager.persist(author);
	} finally {
	    entityTransaction.commit();
	}

	return author.getAuthorId();
    }

    @Override
    public boolean editItem(Author author) throws DAOExeption {
	boolean success = true;
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    entityManager.merge(author);
	} finally {
	    entityTransaction.commit();
	}

	return success;

    }

    @Override
    public void deleteItem(Long authorId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    Author author = entityManager.find(Author.class, authorId);
	    entityManager.remove(author);
	} finally {
	    entityTransaction.commit();
	}

    }

    @Override
    public Author readItem(Long authorId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();

	Author author = entityManager.find(Author.class, authorId);

	return author;
    }

    @Override
    public List<Author> getAuthorsList() throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	List<Author> authorsList = (List<Author>) entityManager.createQuery("from AUTHORS a", Author.class)
		.getResultList();

	return authorsList;
    }

    @Override
    public void expiredAuthor(Long authorId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    Author author = entityManager.find(Author.class, authorId);
	    author.setExpider(new Date());
	    entityManager.merge(author);

	} finally {
	    entityTransaction.commit();
	}

    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void link(Long authorId, Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

}
