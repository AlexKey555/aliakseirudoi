package com.epam.rudoi.newsportal.service.impl.eclipseLink;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.ICommentService;

@Profile("eclipselink")
public class CommentServiceImpl implements ICommentService{
    
    private static final Logger LOGGER = Logger.getLogger(CommentServiceImpl.class);

    private ICommentDAO commentDAO;

    @Override
    public Long addComment(Comment commentItem) throws ServiceExeption {
	Long commentId = null;
	try {
	    commentId = commentDAO.addItem(commentItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Adding comment eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	return commentId;
    }

    @Override
    public void editComment(Comment commentItem) throws ServiceExeption {
	try {
	    commentDAO.editItem(commentItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing comment eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceExeption {
	try {
	    commentDAO.deleteItem(commentId);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting comment eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void unlink(Long newsId) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(List<Long> newsIdList) throws ServiceExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public List<Comment> getCommentList(Long newsId) throws ServiceExeption {
	List<Comment> commentsList = null;
	try {
	    commentsList = commentDAO.getCommentList(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("getting comment list eclipseLink exeption", e);
	    throw new ServiceExeption(e);
	}
	return commentsList;
    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceExeption {
	try {
	    commentDAO.deleteNewsComments(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting all news comments hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void setCommentDAO(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

}
