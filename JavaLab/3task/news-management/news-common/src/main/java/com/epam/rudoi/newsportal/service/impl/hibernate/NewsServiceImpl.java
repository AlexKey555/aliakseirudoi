package com.epam.rudoi.newsportal.service.impl.hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.INewsService;

@Profile("hibernate")
public class NewsServiceImpl implements INewsService {

    private static final Logger LOGGER = Logger.getLogger(NewsServiceImpl.class);
    
    private INewsDAO newsDAO;

    @Override
    public Long addNews(News newsItem) throws ServiceExeption {
	Long newsId = null;
	try {
	    newsDAO.addItem(newsItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Adding news hibernate exeption", e);
	    throw new ServiceExeption();
	}
	return newsId;
    }

    @Override
    public boolean editNews(News newsItem) throws ServiceExeption {
	boolean flag;
	try {
	    flag = newsDAO.editItem(newsItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing news hibernate exeption", e);
	    throw new ServiceExeption();
	}
	return flag;
    }

    @Override
    public void deleteaQuantityNews(List<Long> newsIdList) throws ServiceExeption {
	try {
	    newsDAO.deleteaQuantityNews(newsIdList);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting quentity news hibernate exeption", e);
	    throw new ServiceExeption(e);
	}

    }

    @Override
    public NewsManagementVO showSingleNews(Long newsId) throws ServiceExeption {
	NewsManagementVO newsManagementVO= null;
	try {
	    newsManagementVO =  newsDAO.showSinglNews(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Showing news hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
	
	return newsManagementVO;
    }

    @Override
    public Long countAllNews() throws ServiceExeption {
	Long newsCount = null;
	try {
	    newsCount = newsDAO.countAllNews();
	} catch (DAOExeption e) {
	    LOGGER.error("Counting all news hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
	
	return newsCount;
    }

    @Override
    public List<NewsManagementVO> SearchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	List<NewsManagementVO> newsVOList = null;

	try {
	    newsVOList = newsDAO.searchNews(searchCriteria);
	} catch (DAOExeption e) {
	    LOGGER.error("searching news hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
	
	return newsVOList;
    }

    @Override
    public List<Long> takeNewsIdList() throws ServiceExeption {
	List<Long> newsIdList= null;
	try {
	    newsIdList = newsDAO.takeNewsIdList();
	} catch (DAOExeption e) {
	    LOGGER.error("take news list hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
	
	return newsIdList;
    }
    

    public void setNewsDAO(INewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    @Override
    public Long countSearchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	Long countNews = null;
	try {
	    countNews = newsDAO.countSearchNews(searchCriteria);
	} catch (DAOExeption e) {
	    LOGGER.error("count search news hibernate exeption", e);
	    throw new ServiceExeption(e);
	}
	return countNews;
    }

}
