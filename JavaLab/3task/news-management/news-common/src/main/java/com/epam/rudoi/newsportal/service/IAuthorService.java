package com.epam.rudoi.newsportal.service;

import java.util.List;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface IAuthorService.
 */
public interface IAuthorService {

	/**
	 * Adds the author.
	 * This method add author 
	 * @param newsManagementVO the news management vo
	 * @return the long
	 * @throws ServiceExeption the service exception
	 */
	Long addAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption;
	
	/**
	 * Edits the author.
	 * This method edit author
	 * @param authorItem the author item
	 * @throws ServiceExeption the service exception
	 */
	void editAuthor(Author authorItem) throws ServiceExeption;
	
	/**
	 * Link.
	 * This method link author with news
	 * @param authorId the author id
	 * @param newsId the news id
	 * @throws ServiceExeption the service exception
	 */
	void link (Long authorId, Long newsId) throws ServiceExeption;
	
	/**
	 * Unlink.
	 * This method link author with news
	 * @param newsId the news id
	 * @throws ServiceExeption the service exception
	 */
	void unlink (Long newsId) throws ServiceExeption;
	
	/**
	 * Unlink.
	 * This method unlink qentity news with authors
	 * @param newsIdList the news id list
	 * @throws ServiceExeption the service exception
	 */
	void unlink (List<Long> newsIdList) throws ServiceExeption;
	
	/**
	 * Expired author.
	 * This method make author expired
	 * @param authorId the author id
	 * @throws ServiceExeption the service exception
	 */
	void expiredAuthor (Long authorId) throws ServiceExeption;
	
	/**
	 * Gets the authors list.
	 * This method get authors list
	 * @return the authors list
	 * @throws ServiceExeption the service exception
	 */
	List<Author> getAuthorsList () throws ServiceExeption;
}
