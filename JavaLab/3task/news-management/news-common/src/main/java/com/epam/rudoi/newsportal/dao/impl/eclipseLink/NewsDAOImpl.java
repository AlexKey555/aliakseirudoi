package com.epam.rudoi.newsportal.dao.impl.eclipseLink;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.context.annotation.Profile;

import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("eclipselink")
public class NewsDAOImpl implements INewsDAO {

    @PersistenceUnit(unitName = "entityManagerFactory")
    private EntityManagerFactory emf;

    @Override
    public Long addItem(News news) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    entityManager.persist(news);
	} finally {
	    entityTransaction.commit();
	}

	return news.getNewsId();
    }

    @Override
    public boolean editItem(News news) throws DAOExeption {
	boolean success = true;
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    news.setNewsModificationDate(new Date());
	    news.setNewsCreationDate(new Date());
	    entityManager.merge(news);

	    entityTransaction.commit();
	} catch (javax.persistence.OptimisticLockException e) {
	    success = false;
	} finally {
	    entityTransaction.commit();
	}

	return success;
    }

    @Override
    public void deleteItem(Long newsId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    News news = entityManager.find(News.class, newsId);
	    entityManager.remove(news);
	} finally {
	    entityTransaction.commit();
	}

    }

    @Override
    public News readItem(Long newsId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();

	News news = entityManager.find(News.class, newsId);

	return news;
    }

    @Override
    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	List<News> newsList = null;
	List<NewsManagementVO> newsManagementVOList = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();
	    newsManagementVOList = new ArrayList<NewsManagementVO>();
	    int start = (int) (long) searchCriteria.getPageNumber();
	    int end = 10;
	    Query query = criteriaBuilder(searchCriteria, entityManager, start, end);
	    newsList = query.getResultList();
	} finally {
	    entityTransaction.commit();
	}

	for (News news : newsList) {
	    NewsManagementVO newManagementVO = new NewsManagementVO();
	    newManagementVO.setNews(news);
	    newManagementVO.setAuthor(news.getAuthorsList().get(0));
	    newManagementVO.setCommentsList(news.getCommentsList());
	    newManagementVO.setTagsList(news.getTagsList());

	    newsManagementVOList.add(newManagementVO);
	}

	return newsManagementVOList;
    }

    private Query criteriaBuilder(SearchCriteria searchCriteria, EntityManager entityManager, int start, int end) {
	Query query = null;
	Long authorId = null;
	List<Long> tagsIdList = null;

	if (searchCriteria != null) {
	    authorId = searchCriteria.getAuthor().getAuthorId();
	    tagsIdList = searchCriteria.getTagsIdList();
	}
	int maxResults = end;

	CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
	Predicate predicate = criteriaBuilder.conjunction();
	CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
	Root<News> root = criteriaQuery.from(News.class);
	criteriaQuery.distinct(true);

	if (authorId != null && authorId != 0L) {
	    Join join = root.join("authorsList", JoinType.LEFT);
	    predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(join.get("authorId"), authorId));
	}

	if (tagsIdList != null && tagsIdList.size() != 0) {
	    Join join = root.join("tagsList", JoinType.LEFT);
	    predicate = criteriaBuilder.and(predicate, join.get("tagId").in(tagsIdList));

	}
	criteriaQuery.where(predicate);
	criteriaQuery.orderBy(criteriaBuilder.desc(root.get("newsModificationDate")));

	query = entityManager.createQuery(criteriaQuery);
	query.setFirstResult(start);
	query.setMaxResults(maxResults);
	return query;
    }

    @Override
    public NewsManagementVO showSinglNews(Long newsId) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	News news = entityManager.find(News.class, newsId);
	NewsManagementVO newsManagementVO = new NewsManagementVO();
	Author author = news.getAuthorsList().get(0);

	newsManagementVO.setAuthor(author);
	newsManagementVO.setNews(news);

	return newsManagementVO;
    }

    @Override
    public Long countAllNews() throws DAOExeption {
	Long countNews = null;
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();

	    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
	    CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
	    Root root = criteriaQuery.from(News.class);
	    criteriaQuery.distinct(true);
	    criteriaQuery.select(criteriaBuilder.countDistinct(root.get("newsId")));
	    Query query = entityManager.createQuery(criteriaQuery);
	    countNews = (Long) query.getSingleResult();
	} finally {
	    entityTransaction.commit();
	}

	return countNews;
    }

    @Override
    public void deleteaQuantityNews(List<Long> newsIdList) throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();

	    for (Long newsId : newsIdList) {
		News news = entityManager.find(News.class, newsId);
		entityManager.remove(news);
	    }
	} finally {
	    entityTransaction.commit();
	}

    }

    @Override
    public List<Long> takeNewsIdList() throws DAOExeption {
	EntityManager entityManager = emf.createEntityManager();
	EntityTransaction entityTransaction = null;
	List<News> newsList = null;
	List<Long> newsIdList = null;
	try {
	    entityTransaction = entityManager.getTransaction();
	    entityTransaction.begin();

	    newsList = (List<News>) entityManager.createQuery("from News n", News.class).getResultList();
	    newsIdList = new ArrayList<Long>();

	    for (News indexNews : newsList) {
		newsIdList.add(indexNews.getNewsId());
	    }

	} finally {
	    entityTransaction.commit();
	}

	return newsIdList;
    }

    @Override
    public Long countSearchNews(SearchCriteria searchCriteria) throws DAOExeption {
	Long countSearchNews = null;
	EntityTransaction entityTransaction = null;
	EntityManager entityManager = emf.createEntityManager();
	entityTransaction = entityManager.getTransaction();
	entityTransaction.begin();

	Query query = createSearchCriteria(searchCriteria, entityManager);
	countSearchNews = (Long) query.getSingleResult();
	try {

	} finally {
	    entityTransaction.commit();
	}

	return countSearchNews;

    }

    private Query createSearchCriteria(SearchCriteria searchCriteria, EntityManager entityManager) {
	Query query = null;
	Long authorId = null;
	List<Long> tagsIdList = null;
	if (searchCriteria != null) {
	    authorId = searchCriteria.getAuthor().getAuthorId();
	    tagsIdList = searchCriteria.getTagsIdList();
	}
	CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
	Predicate predicate = criteriaBuilder.conjunction();
	CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
	criteriaQuery.distinct(true);
	Root root = criteriaQuery.from(News.class);
	criteriaQuery.distinct(true);

	if (authorId != null) {
	    Join join = root.join("authorsList", JoinType.LEFT);
	    predicate = criteriaBuilder.and(predicate,
		    criteriaBuilder.equal(join.get("authorId"), searchCriteria.getAuthor().getAuthorId()));
	}

	if (tagsIdList != null && tagsIdList.size() != 0) {
	    Join join = root.join("tagsList", JoinType.LEFT);
	    predicate = criteriaBuilder.and(predicate, join.get("tagId").in(tagsIdList));
	    criteriaQuery.where(predicate);
	}

	criteriaQuery.select(criteriaBuilder.countDistinct(root.get("newsId")));
	query = entityManager.createQuery(criteriaQuery);

	return query;

    }

}
