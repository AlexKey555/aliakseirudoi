package com.epam.rudoi.newsportal.service;

import java.util.List;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface ICommentService.
 */
public interface ICommentService {
	
	/**
	 * Adds the comment.
	 * This method add comment
	 * @param commentItem the comment item
	 * @return the long
	 * @throws ServiceExeption the service exception
	 */
	Long addComment(Comment commentItem) throws ServiceExeption;
	
	/**
	 * Edits the comment.
	 * This method edit comment
	 * @param commentItem the comment item
	 * @throws ServiceExeption the service exception
	 */
	void editComment(Comment commentItem) throws ServiceExeption;
	
	/**
	 * Delete comment.
	 * This method delete comment
	 * @param commentId the comment id
	 * @throws ServiceExeption the service exception
	 */
	void deleteComment(Long commentId) throws ServiceExeption;
	
	/**
	 * Unlink.
	 * This method unlink comment with news
	 * @param newsId the news id
	 * @throws ServiceExeption the service exception
	 */
	void unlink(Long newsId) throws ServiceExeption;
	
	/**
	 * Unlink.
	 * This method unlink comments with qentity news
	 * @param newsIdList the news id list
	 * @throws ServiceExeption the service exception
	 */
	void unlink(List<Long> newsIdList) throws ServiceExeption;
	
	/**
	 * Gets the comment list.
	 * This method get comment list
	 * @param newsId the news id
	 * @return the comment list
	 * @throws ServiceExeption the service exception
	 */
	List<Comment> getCommentList(Long newsId) throws ServiceExeption;
	
	/**
	 * Delete news comments.
	 * This method delete comments from news
	 * @param newsId the news id
	 * @throws ServiceExeption the service exception
	 */
	void deleteNewsComments(Long newsId) throws ServiceExeption;

}
