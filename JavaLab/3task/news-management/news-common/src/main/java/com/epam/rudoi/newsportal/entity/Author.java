package com.epam.rudoi.newsportal.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

@Entity(name="AUTHORS")
@Table(name="AUTHORS")
@Indexed
@SequenceGenerator(name="SQ_AUTHOR_ID_SEQ", sequenceName="SQ_AUTHOR_ID_SEQ",allocationSize=1)
public class Author {
    	
    	@Id
    	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SQ_AUTHOR_ID_SEQ")
    	@Column(name="AUTHOR_ID",nullable=false,precision=20)
    	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private Long authorId;
    	
    	@Column(name="AUTHOR_NAME",nullable=false,precision=30)
	private String authorName;

    	@Column(name="EXPIRED",nullable=true)
    	@Temporal(TemporalType.TIMESTAMP)
    	private Date expired;
    	
    	/*@ManyToMany()
        @JoinTable(name = "NEWS_AUTHORS", joinColumns = { @JoinColumn(name = "AUTHOR_ID",
        referencedColumnName = "AUTHOR_ID") }, inverseJoinColumns = { @JoinColumn(name = "NEWS_ID",
        referencedColumnName = "NEWS_ID") })
        private List<News> newsList;*/
	
	public Author() {
	    super();
	}
	
	public Author(Long authorId, String authorName,Date expider) {
	    super();
	    this.authorId = authorId;
	    this.authorName = authorName;
	    this.expired = expider;
	}
	
	public Author(Long authorId, String authorName) {
	    super();
	    this.authorId = authorId;
	    this.authorName = authorName;
	}
	
	public Author(Long authorId) {
	    super();
	    this.authorId = authorId;
	}
	public Author(String authorName) {
	    super();
	    this.authorName = authorName;
	}

	public Long getAuthorId() {
	    return authorId;
	}

	public void setAuthorId(Long authorId) {
	    this.authorId = authorId;
	}

	public String getAuthorName() {
	    return authorName;
	}

	public void setAuthorName(String authorName) {
	    this.authorName = authorName;
	}

	public Date getExpider() {
	    return expired;
	}

	public void setExpider(Date expider) {
	    this.expired = expider;
	}

	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
	    result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
	    result = prime * result + ((expired == null) ? 0 : expired.hashCode());
	    return result;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    Author other = (Author) obj;
	    if (authorId == null) {
		if (other.authorId != null)
		    return false;
	    } else if (!authorId.equals(other.authorId))
		return false;
	    if (authorName == null) {
		if (other.authorName != null)
		    return false;
	    } else if (!authorName.equals(other.authorName))
		return false;
	    if (expired == null) {
		if (other.expired != null)
		    return false;
	    } else if (!expired.equals(other.expired))
		return false;
	    return true;
	}

	@Override
	public String toString() {
	    return "Author [authorId=" + authorId + ", authorName=" + authorName + ", expider=" + expired + "]";
	}
	
}
