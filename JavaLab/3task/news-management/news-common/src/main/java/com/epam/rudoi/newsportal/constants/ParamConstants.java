package com.epam.rudoi.newsportal.constants;

public class ParamConstants {

    public static final int PARAM_SIZE_FOR_ITER_2PARAM = 2;
    public static final int PARAM_SIZE_FOR_ITER_1PARAM = 1;
    public static final int PARAM_SIZE_FOR_ITER_0PARAM = 0;
    
}
