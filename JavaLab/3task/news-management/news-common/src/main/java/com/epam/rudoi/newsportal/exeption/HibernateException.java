package com.epam.rudoi.newsportal.exeption;

public class HibernateException extends Exception{

    private static final long serialVersionUID = 5161313562361738395L;

	public HibernateException() {
		super();
		
	}

	public HibernateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public HibernateException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public HibernateException(String message) {
		super(message);
		
	}

	public HibernateException(Throwable cause) {
		super(cause);
		
	}

    
}
