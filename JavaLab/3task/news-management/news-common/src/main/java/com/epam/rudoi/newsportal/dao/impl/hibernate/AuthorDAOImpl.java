package com.epam.rudoi.newsportal.dao.impl.hibernate;

import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@Profile("hibernate")
public class AuthorDAOImpl implements IAuthorDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Long addItem(Author author) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	try {
	    tx = session.beginTransaction();
	    session.save(author);
	} finally {
	    tx.commit();
	    session.close();
	}

	return author.getAuthorId();
    }

    @Override
    public void expiredAuthor(Long authorId) throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;

	try {
	    tx = session.beginTransaction();
	    Author author = (Author) session.createCriteria(Author.class).add(Restrictions.eq("authorId", authorId))
		    .uniqueResult();
	    author.setExpider(new Date());
	} finally {
	    tx.commit();
	    session.close();
	}

    }

    @Override
    public Author readItem(Long authorId) throws DAOExeption {

	Session session = sessionFactory.openSession();
	Author author = null;
	try {
	    author = (Author) session.createCriteria(Author.class).add(Restrictions.eq("authorId", authorId))
		    .uniqueResult();
	}

	finally {
	    session.close();
	}
	return author;
    }

    @Override
    public boolean editItem(Author author) throws DAOExeption {
	boolean success = true;
	Session session = sessionFactory.openSession();
	Transaction tx = null;

	try {
	    tx = session.beginTransaction();
	    session.update(author);
	} finally {
	    tx.commit();
	    session.close();
	}

	return success;
    }

    @Override
    public List<Author> getAuthorsList() throws DAOExeption {
	Session session = sessionFactory.openSession();
	Transaction tx = null;
	List<Author> authorsList = null;
	try {
	    tx = session.beginTransaction();
	    authorsList = session.createCriteria(Author.class).list();
	} finally {
	    tx.commit();
	    session.close();
	}
	
	return authorsList;
    }

    @Override
    public void link(Long authorId, Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void unlink(Long newsId) throws DAOExeption {
	throw new UnsupportedOperationException();

    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

    @Override
    public void deleteItem(Long authorId) throws DAOExeption {
	throw new UnsupportedOperationException();
    }

}
