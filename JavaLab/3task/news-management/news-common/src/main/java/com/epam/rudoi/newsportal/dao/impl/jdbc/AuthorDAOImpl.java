package com.epam.rudoi.newsportal.dao.impl.jdbc;

import static com.epam.rudoi.newsportal.constants.ParamConstants.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;
import com.epam.rudoi.newsportal.util.QueryBuilder;

@Profile("jdbc")
public class AuthorDAOImpl implements IAuthorDAO {

    private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO authors (author_id, author_name) VALUES (sq_author_id_seq.nextval,?)";
    private static final String SQL_EDIT_NEWS_AUTHOR = "UPDATE authors SET author_name=? WHERE author_id=?";
    private static final String SQL_LINK_NEWS_WITH_AUTHOR = "INSERT INTO news_authors (news_id, author_id) VALUES (?,?)";
    private static final String SQL_UNLINK_NEWS_WITH_AUTHOR = "DELETE FROM news_authors WHERE news_id=?";
    private static final String SQL_UNLINK_NEWS_WITH_AUTHORS_QUENTITY = "DELETE FROM news_authors WHERE news_id IN(";
    private static final String SQL_GET_AUTHOR = "SELECT a.author_id, a.author_name, a.expired FROM authors a WHERE a.author_id = ?";
    private static final String SQL_EXPIRED_AUTHOR = "UPDATE authors SET expired = current_timestamp WHERE author_id = ?";
    private static final String SQL_GET_AUTHORS_LIST = "SELECT a.author_id, a.author_name FROM authors a";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    public Long addItem(Author author) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	Long authorId = null;
	String[] column = { "AUTHOR_ID" };

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_AUTHOR, column);
	    preparedStatement.setString(1, author.getAuthorName());
	    preparedStatement.executeUpdate();
	    resultSet = preparedStatement.getGeneratedKeys();
	    if ((resultSet != null) && (resultSet.next())) {
		authorId = resultSet.getLong(1);
	    }

	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	   DBCloser.dataBaseCloser(dataSource, connection,
	   preparedStatement, resultSet);
	}
	return authorId;
    }

    public boolean editItem(Author author) throws DAOExeption {
	boolean success = true;
	PreparedStatement preparedStatement = null;
	Connection connection = null;

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_EDIT_NEWS_AUTHOR);
	    preparedStatement.setString(1, author.getAuthorName());
	    preparedStatement.setLong(2, author.getAuthorId());
	    preparedStatement.executeUpdate();

	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
	return success;

    }

    public Author readItem(Long authorId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	Author author = null;

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_GET_AUTHOR);
	    preparedStatement.setLong(1, authorId);
	    resultSet = preparedStatement.executeQuery();
	    if ((resultSet != null) && (resultSet.next())) {
		author = new Author(resultSet.getLong(1), resultSet.getString(2), resultSet.getTimestamp(3));
	    }
	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}

	return author;
    }

    public void deleteItem(Long authorId) throws DAOExeption {
	
    }

    public void link(Long authorId, Long newsId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_LINK_NEWS_WITH_AUTHOR);
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.setLong(2, authorId);
	    preparedStatement.executeUpdate();

	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}

    }

    public void unlink(Long newsId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_UNLINK_NEWS_WITH_AUTHOR);
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.executeUpdate();

	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}

    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {

	PreparedStatement preparedStatement = null;
	Connection connection = null;

	try {
	    QueryBuilder qb = new QueryBuilder();
	    String queryForUnlinkAuthors = qb.buildQueryForStatment(SQL_UNLINK_NEWS_WITH_AUTHORS_QUENTITY, newsIdList);

	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(queryForUnlinkAuthors);
	    int i = PARAM_SIZE_FOR_ITER_1PARAM;
	    for (Long newsId : newsIdList) {
		preparedStatement.setLong(i, newsId);
		i++;
	    }
	    preparedStatement.executeUpdate();

	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}

    }

    public void expiredAuthor(Long authorId) throws DAOExeption {

	PreparedStatement preparedStatement = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_EXPIRED_AUTHOR);
	    preparedStatement.setLong(1, authorId);
	    preparedStatement.executeUpdate();

	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    public List<Author> getAuthorsList() throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;

	List<Author> authorsList = new ArrayList<Author>();
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_GET_AUTHORS_LIST);
	    resultSet = preparedStatement.executeQuery();

	    while (resultSet.next()) {
		Author author = new Author(resultSet.getLong(1), resultSet.getString(2));
		authorsList.add(author);
	    }
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}

	return authorsList;
    }

}
