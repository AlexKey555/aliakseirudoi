package com.epam.rudoi.newsportal.entity;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

public class SearchCriteria {

    private Author author;
    private List<Long> tagsIdList;
    private Long pageNumber;
    
    public Long getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Long pageNumber) {
        this.pageNumber = pageNumber;
    }

    public SearchCriteria() {
	super();
    }

    public SearchCriteria(Author author, List<Long> tagsIdList) {
	super();
	this.author = author;
	this.tagsIdList = tagsIdList;
    }

    public SearchCriteria(List<Long> tagsIdList) {
	super();
	this.tagsIdList = tagsIdList;
    }

    public Author getAuthor() {
	return author;
    }

    public void setAuthor(Author author) {
	this.author = author;
    }

    public List<Long> getTagsIdList() {

	if (tagsIdList == null) {
	    return tagsIdList;
	} else {
	    return new ArrayList<Long>(tagsIdList);
	}

    }

    public void setTagsIdList(List<Long> tagsIdList) {
	this.tagsIdList = tagsIdList;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((author == null) ? 0 : author.hashCode());
	result = prime * result + ((pageNumber == null) ? 0 : pageNumber.hashCode());
	result = prime * result + ((tagsIdList == null) ? 0 : tagsIdList.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	SearchCriteria other = (SearchCriteria) obj;
	if (author == null) {
	    if (other.author != null)
		return false;
	} else if (!author.equals(other.author))
	    return false;
	if (pageNumber == null) {
	    if (other.pageNumber != null)
		return false;
	} else if (!pageNumber.equals(other.pageNumber))
	    return false;
	if (tagsIdList == null) {
	    if (other.tagsIdList != null)
		return false;
	} else if (!tagsIdList.equals(other.tagsIdList))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "SearchCriteria [author=" + author + ", tagsIdList=" + tagsIdList + ", pageNumber=" + pageNumber + "]";
    }

  

}
