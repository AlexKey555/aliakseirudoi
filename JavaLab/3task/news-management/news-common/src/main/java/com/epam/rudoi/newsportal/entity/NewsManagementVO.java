package com.epam.rudoi.newsportal.entity;

import java.util.ArrayList;
import java.util.List;

public class NewsManagementVO implements Cloneable{
    	
	private Author author;
	private Comment comment;
	private News news;
	private Tag tag;
	private SearchCriteria searchCriteria;
	
	private List<Long> newsIdList;
	private List<Comment> commentsList;
	private List<Tag> tagsList;
	private List<Long> tagsIdList;
	
	
	private List<News> newsList;
	
	public NewsManagementVO() {
	    super();
	}

	public NewsManagementVO(News news, Author author) {
	    super();
	    this.news = news;
	    this.author = author;
	}
	
	public NewsManagementVO(Author author) {
	    super();
	    this.author = author;
	}
	
	public NewsManagementVO(Comment comment) {
	    super();
	    this.comment = comment;
	}

	public NewsManagementVO(News news) {
	    super();
	    this.news = news;
	}
	
	public NewsManagementVO(Tag tag) {
	    super();
	    this.tag = tag;
	}

	public NewsManagementVO(SearchCriteria searchCriteria) {
	    super();
	    this.searchCriteria = searchCriteria;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public List<News> getNewsList() {
	    if(newsList == null) {
		return newsList;
	} else {
		return new ArrayList<News>(newsList);
	}
	}

	public void setNewsList(List<News> newsList) {
	    this.newsList = newsList;
	}

	public List<Long> getNewsIdList() {
		if(newsIdList == null) {
			return newsIdList;
		} else {
			return new ArrayList<Long>(newsIdList);
		}
		
	}

	public void setNewsIdList(List<Long> newsIdList) {
		this.newsIdList = newsIdList;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public SearchCriteria getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(SearchCriteria searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public List<Comment> getCommentsList() {

		if(commentsList == null) {
			return commentsList;
		} else {
			return new ArrayList<Comment>(commentsList);
		}
	}

	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

	public List<Tag> getTagsList() {
		
		if(tagsList == null) {
			return tagsList;
		} else {
			return new ArrayList<Tag>(tagsList);
		}
	}

	public void setTagsList(List<Tag> tagsList) {
		this.tagsList = tagsList;
	}

	public List<Long> getTagsIdList() {
	
		if(tagsIdList == null) {
			return tagsIdList;
		} else {
			return new ArrayList<Long>(tagsIdList);
		}
		
	}

	public void setTagsIdList(List<Long> tagsIdList) {
		this.tagsIdList = tagsIdList;
	}

	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((author == null) ? 0 : author.hashCode());
	    result = prime * result + ((comment == null) ? 0 : comment.hashCode());
	    result = prime * result + ((commentsList == null) ? 0 : commentsList.hashCode());
	    result = prime * result + ((news == null) ? 0 : news.hashCode());
	    result = prime * result + ((newsIdList == null) ? 0 : newsIdList.hashCode());
	    result = prime * result + ((newsList == null) ? 0 : newsList.hashCode());
	    result = prime * result + ((searchCriteria == null) ? 0 : searchCriteria.hashCode());
	    result = prime * result + ((tag == null) ? 0 : tag.hashCode());
	    result = prime * result + ((tagsIdList == null) ? 0 : tagsIdList.hashCode());
	    result = prime * result + ((tagsList == null) ? 0 : tagsList.hashCode());
	    return result;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    NewsManagementVO other = (NewsManagementVO) obj;
	    if (author == null) {
		if (other.author != null)
		    return false;
	    } else if (!author.equals(other.author))
		return false;
	    if (comment == null) {
		if (other.comment != null)
		    return false;
	    } else if (!comment.equals(other.comment))
		return false;
	    if (commentsList == null) {
		if (other.commentsList != null)
		    return false;
	    } else if (!commentsList.equals(other.commentsList))
		return false;
	    if (news == null) {
		if (other.news != null)
		    return false;
	    } else if (!news.equals(other.news))
		return false;
	    if (newsIdList == null) {
		if (other.newsIdList != null)
		    return false;
	    } else if (!newsIdList.equals(other.newsIdList))
		return false;
	    if (newsList == null) {
		if (other.newsList != null)
		    return false;
	    } else if (!newsList.equals(other.newsList))
		return false;
	    if (searchCriteria == null) {
		if (other.searchCriteria != null)
		    return false;
	    } else if (!searchCriteria.equals(other.searchCriteria))
		return false;
	    if (tag == null) {
		if (other.tag != null)
		    return false;
	    } else if (!tag.equals(other.tag))
		return false;
	    if (tagsIdList == null) {
		if (other.tagsIdList != null)
		    return false;
	    } else if (!tagsIdList.equals(other.tagsIdList))
		return false;
	    if (tagsList == null) {
		if (other.tagsList != null)
		    return false;
	    } else if (!tagsList.equals(other.tagsList))
		return false;
	    return true;
	}

	@Override
	public String toString() {
	    return "NewsManagementVO [author=" + author + ", comment=" + comment + ", news=" + news + ", tag=" + tag
		    + ", searchCriteria=" + searchCriteria + ", newsIdList=" + newsIdList + ", commentsList="
		    + commentsList + ", tagsList=" + tagsList + ", tagsIdList=" + tagsIdList + ", newsList=" + newsList
		    + "]";
	}

	


}
