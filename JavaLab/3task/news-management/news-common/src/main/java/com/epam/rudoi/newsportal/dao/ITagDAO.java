package com.epam.rudoi.newsportal.dao;

import java.util.List;

import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface ITagDAO.
 */
public interface ITagDAO extends ICrudDAO<Tag> {
	
	/**
	 * Link.
	 * This method link tag with news
	 * @param tagIdList the tag id list
	 * @param newsId the news id
	 * @throws DAOExeption the DAO exception
	 */
	void link (List<Long> tagIdList, Long newsId) throws DAOExeption;
	
	/**
	 * Unlink.
	 * This method unlink tag with news
	 * @param newsId the news id
	 * @throws DAOExeption the DAO exception
	 */
	void unlink (Long newsId) throws DAOExeption;
	
	/**
	 * Unlink by tag id.
	 * This method unlink tag with news by tagId
	 * @param tagId the tag id
	 * @throws DAOExeption the DAO exception
	 */
	void unlinkByTagId (Long tagId) throws DAOExeption;
	
	/**
	 * Gets the tags list.
	 * This method get tags list
	 * @return the tags list
	 * @throws DAOExeption the DAO exception
	 */
	List<Tag> getTagsList () throws DAOExeption;
	
	/**
	 * Gets the tags id list.
	 * This method get tags id list
	 * @return the tags id list
	 * @throws DAOExeption the DAO exception
	 */
	List<Long> getTagsIdList () throws DAOExeption;
	
	 /**
 	 * Unlink.
 	 * This method unlink tags with news by qentity news
 	 * @param newsIdList the news id list
 	 * @throws DAOExeption the DAO exception
 	 */
 	void unlink(List<Long> newsIdList) throws DAOExeption; 
	
}
