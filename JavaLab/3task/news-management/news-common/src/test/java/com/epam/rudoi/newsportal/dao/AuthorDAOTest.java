package com.epam.rudoi.newsportal.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import oracle.security.o5logon.a;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@ActiveProfiles("eclipselink")
@SpringApplicationContext({ "classpath:context_spring_test.xml" })
@DataSet(value = "dataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, })
@ContextConfiguration(locations = { "classpath:context_spring_test.xml" })
public class AuthorDAOTest extends UnitilsJUnit4 {

    @Autowired
    private IAuthorDAO authorDAO;

    @Test
    public void addAuthorTest() throws DAOExeption {
	Author expectedAuthor = new Author();
	Long returnId = null;
	expectedAuthor.setAuthorName("Freeman");
	returnId = authorDAO.addItem(expectedAuthor);
	assertNotNull(returnId);
    }
    
    @Test
    public void expiredAuthorTest() throws DAOExeption {
	Author resultAuthor = null;
	Date resultAuthorExpired = null;
	Long authorId = 3L;
	authorDAO.expiredAuthor(authorId);
	resultAuthor = authorDAO.readItem(authorId);
	resultAuthorExpired = resultAuthor.getExpider();
	assertNotNull(resultAuthorExpired);
	
    }
    
    @Test
    public void readAuthorTest() throws DAOExeption {
	Author expectedAuthor = new Author();
	Author resultAuthor = null;
	Long returnId = null;
	expectedAuthor.setAuthorName("Freeman");
	returnId = authorDAO.addItem(expectedAuthor);
	resultAuthor = authorDAO.readItem(returnId);
	assertEquals(expectedAuthor.getAuthorId(), resultAuthor.getAuthorId());
    }

    @Test
    public void editAuthorTest() throws DAOExeption {
	Author expectedAuthor = new Author();
	Author resultAuthor = null;
	expectedAuthor.setAuthorId(1L);
	expectedAuthor.setAuthorName("Bill");
	authorDAO.editItem(expectedAuthor);
	resultAuthor = authorDAO.readItem(1L);
	assertEquals(expectedAuthor, resultAuthor);
    }
    
    @Test
    public void getAuthorsListTest() throws DAOExeption {
	List<Author> authorsList = authorDAO.getAuthorsList();
	assertNotNull(authorsList);
    }
    
   /* @Test
    public void linkAuthorTest() throws DAOExeption {
	authorDAO.link(1L, 1L);
    }

    @Test
    public void unlinkAuthor1ParamTest() throws DAOExeption {
	authorDAO.unlink(1L);
    }
    
    @Test
    public void unlinkAuthor2ParamTest() throws DAOExeption {
	authorDAO.unlink(1L);
    }*/

}
