package com.epam.rudoi.newsportal.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;

import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@ActiveProfiles("eclipselink")
@SpringApplicationContext({"classpath:context_spring_test.xml"})
@DataSet(value = "DataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, })
@ContextConfiguration(locations = { "classpath:context_spring_test.xml" })
public class TagDAOTest extends UnitilsJUnit4{

	@Autowired
	private ITagDAO tagDAO;
	
	@Test
	public void addTagTest() throws DAOExeption {
		Tag expectedTag = new Tag();
		Tag resultTag = null;
		Long returnId = null;
		expectedTag.setTagName("new tag");
		returnId = tagDAO.addItem(expectedTag);
		resultTag = tagDAO.readItem(returnId);
		assertEquals(expectedTag.getTagName(), resultTag.getTagName());
	}
	
	@Test
	public void getTagsListTest() throws DAOExeption {
	    List<Tag> tagsList = tagDAO.getTagsList();
	    assertNotNull(tagsList);
	}
	
	@Test
	public void getTagsIdListTest() throws DAOExeption {
	    List<Long> tagsIdList = tagDAO.getTagsIdList();
	    assertNotNull(tagsIdList);
	}
	
	@Test
	public void readTagTest() throws DAOExeption {
	    Tag resultTag = tagDAO.readItem(3L);
	    assertNotNull(resultTag);
	}
	
	@Test
	public void editTagTest() throws DAOExeption {
	    	Tag expectedTag = new Tag();
		Tag resultTag = null;
		expectedTag.setTagId(1L);
		expectedTag.setTagName("war!!!war");
		tagDAO.editItem(expectedTag);
		resultTag = tagDAO.readItem(1L);
		assertEquals(expectedTag.getTagName(), resultTag.getTagName());
	}
	
	@Test
	public void deleteTagTest() throws DAOExeption {
	    	Tag expectedTag = new Tag();
		Tag resultTag = new Tag();
		Long resultId = null;
		Long deleteId = null;
		expectedTag.setTagName("bla bla");
		resultId = tagDAO.addItem(expectedTag);
		resultTag = tagDAO.readItem(resultId);
		deleteId = resultTag.getTagId();
		
		tagDAO.deleteItem(deleteId);
		resultTag = tagDAO.readItem(deleteId);
		assertNull(resultTag);
	}
	/*
	@Test
	public void linkTagTest() throws DAOExeption {
		List<Long> tagIdList = new ArrayList<Long>(); 
		tagDAO.link(tagIdList, 1L);
	}
	

	@Test
	public void unlinkTagTest() throws DAOExeption {
		tagDAO.unlink(1L);
	}
	

	@Test
	public void unlinkByTagIdTest() throws DAOExeption {
		tagDAO.unlinkByTagId(1L);
	}*/
}
