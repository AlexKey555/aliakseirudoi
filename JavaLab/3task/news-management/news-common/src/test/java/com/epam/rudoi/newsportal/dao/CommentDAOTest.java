package com.epam.rudoi.newsportal.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@ActiveProfiles("eclipselink")
@SpringApplicationContext({"classpath:context_spring_test.xml"})
@DataSet(value = "DataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, })
@ContextConfiguration(locations = { "classpath:context_spring_test.xml" })
public class CommentDAOTest extends UnitilsJUnit4{
	
	@Autowired
	private ICommentDAO commentDAO;

	@Test
	public void getCommentListTest() throws DAOExeption {
	    Long newsId = 1L;
	    List<Comment> resultCommentsList = null;
	    
	    resultCommentsList = commentDAO.getCommentList(newsId);
	    assertNotNull(resultCommentsList);
	}
	
	@Test
	public void deleteCommentTest() throws DAOExeption {
	    	Comment expectedComment = new Comment();	
	    	expectedComment.setCommentText("bla bla");
		expectedComment.setCreationDate(new Date());
		expectedComment.setNewsId(1L);
	    	Long resultId = commentDAO.addItem(expectedComment);
		commentDAO.deleteItem(resultId);
		Comment resultComment = commentDAO.readItem(resultId);
		assertNull(resultComment);
	}
	
	@Test
	public void deleteNewsCommentTest() throws DAOExeption {
	    	Comment expectedComment = new Comment();	
	    	expectedComment.setCommentText("bla bla");
		expectedComment.setCreationDate(new Date());
		expectedComment.setNewsId(3L);
	    	Long resultId = commentDAO.addItem(expectedComment);
		commentDAO.deleteItem(resultId);
		Comment resultComment = commentDAO.readItem(resultId);
		assertNull(resultComment);
	}
	
	@Test
	public void addCommentTest() throws DAOExeption {
	    	Comment expectedComment = new Comment();
	    	Long returnId = null;
		Comment resultComment = null;
		expectedComment.setCommentText("bla bla");
		expectedComment.setCreationDate(new Date());
		expectedComment.setNewsId(1L);
		
		returnId = commentDAO.addItem(expectedComment);
		resultComment = commentDAO.readItem(returnId);
		assertEquals(expectedComment, resultComment);
	}
	
	@Test
	public void editCommentTest() throws DAOExeption {
	    	Comment expectedComment = new Comment();
		Comment resultComment = null;
		Long returnId = 2L;
		expectedComment = commentDAO.readItem(returnId);
		expectedComment.setCommentText("bla bla bla");
		commentDAO.editItem(expectedComment);
		resultComment = commentDAO.readItem(returnId);
		
		assertEquals(expectedComment.getCommentText(), resultComment.getCommentText());
	}

	@Test
	public void readCommentTest() throws DAOExeption {
	    Comment resultComment = null;	
	    resultComment = commentDAO.readItem(3L);
	    assertNotNull(resultComment);
	}
}
