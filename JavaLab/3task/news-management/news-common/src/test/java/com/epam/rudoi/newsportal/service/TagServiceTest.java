package com.epam.rudoi.newsportal.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyLong;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.impl.jdbc.TagServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context_spring.xml")
public class TagServiceTest {

	@Mock
	private ITagDAO tagDAOMock;

	@InjectMocks
	@Autowired
	private TagServiceImpl tagService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(tagService);
		assertNotNull(tagDAOMock);
	}

	@Test
	public void addTagTest() throws DAOExeption, ServiceExeption {
		Long expectedTagId = 1L;
		Long expectedNewsId = 1L;
		Long resultId = null;
		
		NewsManagementVO expectedNewsManagementVO = new NewsManagementVO();
		Tag tagItem = new Tag();
		News newsItem = new News();
		newsItem.setNewsId(expectedNewsId);
		tagItem.setTagId(expectedTagId);
		expectedNewsManagementVO.setTag(tagItem);
		expectedNewsManagementVO.setNews(newsItem);
		
		tagItem.setTagId(expectedTagId);
		when(tagDAOMock.addItem(tagItem)).thenReturn(1L);
		resultId = tagService.addTag(expectedNewsManagementVO);
		verify(tagDAOMock, times(1)).addItem(expectedNewsManagementVO.getTag());
		assertEquals(expectedTagId, resultId);
	}

	@Test
	public void editTagTest() throws DAOExeption, ServiceExeption {
		Tag tagItem = new Tag();
		tagService.editTag(tagItem);
		verify(tagDAOMock, times(1)).editItem(tagItem);
	}

	@Test
	public void deleteTagTest() throws DAOExeption, ServiceExeption {
		tagService.deleteTag(anyLong());		
		verify(tagDAOMock, times(1)).deleteItem(anyLong());
	}


	@Test
	public void linkTagTest() throws DAOExeption, ServiceExeption {
		List<Long> tagsIdList = new ArrayList<Long>();
	    tagService.link(tagsIdList, 1L);
		verify(tagDAOMock, times(1)).link(tagsIdList, 1L);
	}
	
	@Test
	public void unlinkTagTest() throws DAOExeption, ServiceExeption {
	tagService.unlink(anyLong());
	verify(tagDAOMock, times(1)).unlink(anyLong());
	}
	
	@Test
	public void unlinkTagByTagId() throws DAOExeption, ServiceExeption {
	tagService.unlinkByTagId(anyLong());
	verify(tagDAOMock, times(1)).unlinkByTagId(anyLong());
	}

}
