package com.epam.rudoi.newsportal.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.w3c.dom.ls.LSInput;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@ActiveProfiles("eclipselink")
@SpringApplicationContext({ "classpath:context_spring_test.xml" })
@DataSet(value = "DataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, })
@ContextConfiguration(locations = { "classpath:context_spring_test.xml" })
public class NewsDAOTest extends UnitilsJUnit4 {

    @Autowired
    private INewsDAO newsDAO;
    @Autowired
    private ITagDAO tagDAO;
    @Autowired
    private IAuthorDAO authorDAO;
    @Autowired
    private ICommentDAO commentDAO;

    @Test
    public void countSearchNewsTest() throws DAOExeption {
	Long resultCount = null;
	SearchCriteria searchCriteria = new SearchCriteria();
	Author author = new Author();
	searchCriteria.setAuthor(author);
	resultCount = newsDAO.countSearchNews(searchCriteria);
	System.out.println(resultCount);
	assertNotNull(resultCount);	
	
    }
    
    /*
    @Test
    public void seachNewsTest() throws DAOExeption {
	List<NewsManagementVO> newsManagementVOList = null;
	List<Long> tagsIdList = new ArrayList<Long>();
	List<News> newsList = null;
	
	SearchCriteria searchCriteria = new SearchCriteria();
	Author author = new Author();
	//author.setAuthorId(3L);
	//tagsIdList.add(1L);
	searchCriteria.setTagsIdList(tagsIdList);
	searchCriteria.setAuthor(author);
	//searchCriteria.setPageNumber(1L);
		
	newsManagementVOList = newsDAO.searchNews(searchCriteria);
	//System.out.println(searchCriteria);
	//System.out.println(newsManagementVOList);
    }
    
     @Test
    public void addNewsTest() throws DAOExeption {
	News expectedNews = new News();
	News resultNews = null;
	Long returnId = null;
	expectedNews.setNewsTitle("test titlle");
	expectedNews.setNewsShortText("short test text");
	expectedNews.setNewsFullText("full test text");
	expectedNews.setNewsCreationDate(new Date());
	expectedNews.setNewsModificationDate(new Date());
	
	returnId = newsDAO.addItem(expectedNews);
	resultNews = newsDAO.readItem(returnId);
	assertEquals(expectedNews.getNewsFullText(), resultNews.getNewsFullText());
    }
    
    @Test
    public void editNewsTest() throws DAOExeption {
	News expectedNews = new News();
	News resultNews = null;
	expectedNews.setNewsId(2L);
	expectedNews.setNewsTitle("test titlle");
	expectedNews.setNewsShortText("short test text");
	expectedNews.setNewsFullText("!!!!!!!!");
	expectedNews.setNewsCreationDate(new Date());
	expectedNews.setNewsModificationDate(new Date());
	newsDAO.editItem(expectedNews);
	resultNews = newsDAO.readItem(2L);
	assertEquals(expectedNews.getNewsFullText(), resultNews.getNewsFullText());
    }
   
    @Test
    public void deleteNewsTest() throws DAOExeption {
	News expectedNews = new News();
	News resultNews = null;
	Long returnId = null;
	expectedNews.setNewsTitle("test titlle");
	expectedNews.setNewsShortText("short test text");
	expectedNews.setNewsFullText("full test text");
	expectedNews.setNewsCreationDate(new Date());
	expectedNews.setNewsModificationDate(new Date());
	
	returnId = newsDAO.addItem(expectedNews);
	newsDAO.deleteItem(returnId);
	resultNews = newsDAO.readItem(returnId);
	
	assertNull(resultNews);
    }

    @Test
    public void readNewsTest() throws DAOExeption {
	News expectedNews = new News();
	News resultNews = null;
	expectedNews.setNewsTitle("test titlle");
	expectedNews.setNewsShortText("test short text");
	expectedNews.setNewsFullText("!!!");
	expectedNews.setNewsCreationDate(new Date());
	expectedNews.setNewsModificationDate(new Date());
	Long returnId = newsDAO.addItem(expectedNews);
	resultNews = newsDAO.readItem(returnId);
	assertEquals(expectedNews.getNewsFullText(), resultNews.getNewsFullText());
    }

    @Test
    public void countAllNewsTest() throws DAOExeption {
	Long returnCount = newsDAO.countAllNews();
	assertNotNull(returnCount);
    }

    @Test
    public void searchNewsTest() throws DAOExeption {

	List<NewsManagementVO> resultNewsManagementVOList = new ArrayList<NewsManagementVO>();

	SearchCriteria searchCriteria = new SearchCriteria();
	Author authorItem = new Author();
	authorItem.setAuthorId(1L);

	List<Long> tagsIdList = new ArrayList<Long>();

	searchCriteria.setAuthor(authorItem);
	// searchCriteria.setTagsIdList(tagsIdList);

	newsDAO.searchNews(searchCriteria);
	resultNewsManagementVOList = newsDAO.searchNews(searchCriteria);
	assertNotNull(resultNewsManagementVOList);

    }

    @Test
    public void showSinglNewsTest() throws DAOExeption {
	NewsManagementVO resultNewsVO = newsDAO.showSinglNews(1L);
	assertNotNull(resultNewsVO);
    }*/
}
