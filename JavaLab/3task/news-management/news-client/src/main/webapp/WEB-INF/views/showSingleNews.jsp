<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<div id="buttomDivSShN">
		<a href=""><spring:message code="local.button.back" /></a>
	</div>
	<div id="#showSingleNewsDivSShN">
		<div id="newsTitleDivSShN"><c:out value="${newsVO.news.newsTitle}"></c:out></div>
		<div id="newsAuthorDivSShN">(by <c:out value="${newsVO.author.authorName}"></c:out>)</div>
		<div id="newsCreationDateDivSShN"><c:out value="${newsVO.news.newsModificationDate}"></c:out></div>
		<br> <br>
		<div id="newsFullTextDivSShN"><c:out value="${newsVO.news.newsFullText}"></c:out></div>
		<br> <br>
		<div id="newsCommentsDivSShN">
			<c:forEach var="commentL" items="${commentsList}">
				<div id="newsCommentCreationDateDivSShN"><c:out value="${commentL.creationDate}"></c:out>
				</div>
				<div id="newsCommentsTextDivSShN"><%-- ${commentL.commentText} --%>
				<c:out value="${commentL.commentText}"></c:out>
				</div>
			</c:forEach>
		</div>
		<div id="commentFormSShN">
			<form:form method="post" modelAttribute="comment" action="addComment">
				<textarea name="commentText" cols="54" rows="4"></textarea>
				<form:hidden path="newsId" value="${newsVO.news.newsId}" />
				<br>
				<form:button path="" id="bottomPostComment"> <spring:message code="local.button.postComments" /> </form:button>
			</form:form>
		</div>
	</div>
<div id="previousNextButtonsSShN">
		<div id="buttomPrevDivSShN">
		<c:if test="${newsVO.news.newsId != resultPreviousNewsId}">
			<a href="previousNews?newsId=${newsVO.news.newsId}"><spring:message code="local.button.previous" /></a>
			</c:if>
		</div>
		<div id="buttomNextDivSShN">
		<c:if test="${newsVO.news.newsId != resultNextNewsId}">
			<a href="nextNews?newsId=${newsVO.news.newsId}"><spring:message code="local.button.next" /></a>
			</c:if>
		</div>
	</div>
