<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/mainTemplate.css" />">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/searchForm.css" />">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/showSingleNews.css" />">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/clientHome.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/checkboxes.js" />" > </script>

</head>
<body>
	<div id="mainDiv">

		<div class="headerAndFooterDiv">
			<tiles:insertAttribute name="header" />
		</div>
		<div id="bodyDiv">
			<tiles:insertAttribute name="body" />
		</div>
		<div class="headerAndFooterDiv">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>