<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<div id="paginationDiv">
			<a class="paginationLinkFL" href="pagination?indexPage=${firtPage}"><<</a>
		<c:if test="${pageId-3>0}">
		<a class="paginationLink" href="pagination?indexPage=${pageId-3}">${pageId-3}</a>
		</c:if>
		<c:if test="${pageId-2>0}">
		<a class="paginationLink" href="pagination?indexPage=${pageId-2}">${pageId-2}</a>
		</c:if>
		<c:if test="${pageId-1>0}">
		<a class="paginationLink" href="pagination?indexPage=${pageId-1}">${pageId-1}</a>
		</c:if>
		<a id="currentPaginationLink" href="pagination?indexPage=${pageId}">${pageId}</a>
		<c:if test="${paginNum >= pageId+1}">
		<a class="paginationLink" href="pagination?indexPage=${pageId+1}">${pageId+1}</a>
		</c:if>
		<c:if test="${paginNum >= pageId+2}">
		<a class="paginationLink" href="pagination?indexPage=${pageId+2}">${pageId+2}</a>
		</c:if>
		<c:if test="${paginNum >= pageId+3}">
		<a class="paginationLink" href="pagination?indexPage=${pageId+3}">${pageId+3}</a>
		</c:if>
		<c:if test="${paginNum-1 >= pageId+3}">
		.....
		<a class="paginationLink" href="pagination?indexPage=${paginNum}"> ${paginNum}</a>
		</c:if>
		<a class="paginationLinkFL" href="pagination?indexPage=${lastPage}">>></a>
		</div>