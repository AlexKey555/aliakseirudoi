<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div id="mainHeaderDiv">
		<div id="topHeaderDiv">
			<spring:message code="local.message.headerMessageClient" />
		</div>
		<br> <br>
		<div id="localeDiv">
			<a href="?language=en <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message
					code="local.button.en" /></a>
					 | 
			<a	href="?language=ru <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message
					code="local.button.ru" /></a>
		</div>
	</div>
</body>
</html>