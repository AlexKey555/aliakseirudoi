package com.epam.rudoi.newsManagement.controller;


import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.INewsManagementService;
import com.epam.rudoi.newsportal.service.impl.jdbc.ServiceManagerImpl;

@Controller
public class ClientHomeController {
	
	@Autowired
	private INewsManagementService serviceManager;
	
	private static final int INDEX_PAGINATION = 1;
	private static final int NEWS_ON_PAGE = 3;
	    

	@RequestMapping({"/" })
	public ModelAndView showClientHomePage(HttpSession session) throws ServiceExeption {
	
		ModelAndView mv = new ModelAndView("clientHome", "searchCriteria", new SearchCriteria());
		
		SearchCriteria searchCriteria = fillSearchCriteria();
		session.setAttribute("searchCriteria", searchCriteria);
		List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);
		
		mv.addObject("paginNum", getPuginNumber(newsVOList));
		mv.addObject("firtPage", 1);
		mv.addObject("lastPage", getPuginNumber(newsVOList));
		
		newsVOList = newsVOList.subList(0, 3);
		mv.addObject("newsVO", newsVOList);
		mvForSearch(mv);
		
		return mv;
	}
		
	 @RequestMapping({ "/pagination" })
	    public ModelAndView paginationPage(HttpSession session, @RequestParam("indexPage") Long indexPage) throws ServiceExeption {

		ModelAndView mv = new ModelAndView("clientHome", "searchCriteria", new SearchCriteria());
		mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());
		
		SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
		if(searchCriteria.getAuthor() == null) {
		      searchCriteria = fillSearchCriteria();
		}
			
		List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);

		Integer minIndex = (int) (indexPage * 3 - 3);
		Integer maxIndex = countMaxIndex(indexPage, newsVOList.size());

		mv.addObject("pageId", indexPage);
		mv.addObject("paginNum", getPuginNumber(newsVOList));
		mv.addObject("firtPage", 1);
		mv.addObject("lastPage", getPuginNumber(newsVOList));
		mv.addObject("queryString", "&indexPage=" + indexPage);
		newsVOList = newsVOList.subList(minIndex, maxIndex);
		mv.addObject("newsVO", newsVOList);
		
		mvForSearch(mv);

		return mv;
	    }
	 
	@RequestMapping({ "/reset" })
	    public ModelAndView resetSearch() throws ServiceExeption {

		ModelAndView mv = new ModelAndView("redirect:/clientHome");
		
		return mv;
	    }
	
	@RequestMapping({"/view" })
	public ModelAndView showSingleNewsPage(@RequestParam("newsId") Long newsId)
			throws ServiceExeption {
	
		ModelAndView mv = new ModelAndView("showSingleNews", "comment", new Comment());
		
		Long newsCount = serviceManager.countAllNews();
		
		mv.addObject("newsVO", serviceManager.showSinglNews(newsId));
		mv.addObject("commentsList", serviceManager.getCommentsList(newsId));
		mv.addObject("paginNum", newsCount);			
		mv.addObject("queryString", "&newsId="+newsId);		
		mvForSearch(mv);
				
		return mv;
	}
	
	 @RequestMapping({ "/searchForm" })
	    public ModelAndView searchCriteriaCheaker(HttpSession session, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria)
		    throws ServiceExeption {
		if(searchCriteria.getAuthor() != null ) {
		    searchCriteria.setPageNumber(1L);
		    session.setAttribute("searchCriteria", searchCriteria);
		}
		
		return searchNews(session);
	    }
	    

	    public ModelAndView searchNews(HttpSession session)
		    throws ServiceExeption {

		SearchCriteria searchCriteria = (SearchCriteria)session.getAttribute("searchCriteria");

		List<NewsManagementVO> newsVOList = new ArrayList<NewsManagementVO>();
		newsVOList = serviceManager.searchNews(searchCriteria);
		
		ModelAndView mv = new ModelAndView("searchPage", "searchCriteria", new SearchCriteria() );
			
		mv.addObject("paginNum", getPuginNumber(newsVOList));
		mv.addObject("searchCriteriaResult", searchCriteria);
		mv.addObject("firtPage", 1);
		mv.addObject("lastPage", getPuginNumber(newsVOList));
		
		if(newsVOList.size() < 3) {
		    newsVOList = newsVOList.subList(0, newsVOList.size());
		} else {
		    newsVOList = newsVOList.subList(0, 3);
		}
		
		mv.addObject("newsVO", newsVOList);
		mvForSearch(mv);

		return mv;
	    }

	private void mvForSearch(ModelAndView mv) throws ServiceExeption {
		mv.addObject("authorsList", serviceManager.getAuthorsList());
		mv.addObject("tagsList", serviceManager.getTagsList());
		mv.addObject("tagsIdList", serviceManager.getTagIdList());
	}
	
	private SearchCriteria fillSearchCriteria() {
		Author author = new Author();	
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthor(author);
		searchCriteria.setPageNumber(1L);
		return searchCriteria;
	}
	
	private Integer countMaxIndex (Long indexPage, Integer listSize) {
		Integer maxIndex = (int) (indexPage * NEWS_ON_PAGE);
		if(listSize < maxIndex) {
			maxIndex = listSize;
		}
		return maxIndex;
	} 
	
	 private Long getPuginNumber(List<NewsManagementVO> newsVO) throws ServiceExeption {
		Long newsCount = (long)newsVO.size() - INDEX_PAGINATION;
		Long pnum = (newsCount - INDEX_PAGINATION) / NEWS_ON_PAGE + INDEX_PAGINATION;
		return pnum;
	    }
	
}
