package com.epam.rudoi.newsportal.dao;

import static org.junit.Assert.*;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@SpringApplicationContext({"context_spring_test.xml"})
@DataSet(value = "dataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class AuthorDAOTest extends UnitilsJUnit4{

	@SpringBean("authorDaoImpl")
	private IAuthorDAO authorDAO;

	@Test
	public void addAuthorTest() throws DAOExeption {
		Author expectedAuthor = new Author();
		Author resultAuthor = null;
		Long returnId = null;
		expectedAuthor.setAuthorId(4L);
		expectedAuthor.setAuthorName("Freeman");
		returnId = authorDAO.addItem(expectedAuthor);
		resultAuthor = authorDAO.readItem(returnId);		
		assertEquals(expectedAuthor.getAuthorName(), resultAuthor.getAuthorName());
	}

	@Test
	public void editAuthorTest() throws DAOExeption {
		Author expectedAuthor = new Author();
		Author resultAuthor = null;
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("Bill");
		authorDAO.editItem(expectedAuthor);
		resultAuthor = authorDAO.readItem(1L);
		assertEquals(expectedAuthor.getAuthorName(), resultAuthor.getAuthorName());
	}
	
	@Test
	public void readAuthorTest() throws DAOExeption {
		Author expectedAuthor = new Author();
		Author resultAuthor = null;
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("John");
		authorDAO.readItem(1L);
		resultAuthor = authorDAO.readItem(1L);
		assertEquals(expectedAuthor.getAuthorName(), resultAuthor.getAuthorName());
	}

	@Test
	
	public void linkAuthorTest() throws DAOExeption {
		authorDAO.link(1L, 1L);
	}

	@Test
	
	public void unlinkAuthorTest() throws DAOExeption {
		authorDAO.unlink(1L);
	}

}
