package com.epam.rudoi.newsportal.dao;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

import static org.junit.Assert.*;

@SpringApplicationContext({"context_spring_test.xml"})
@DataSet(value = "DataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class NewsDAOTest extends UnitilsJUnit4{

	@SpringBean("newsDaoImpl")
	private INewsDAO newsDAO;
	@SpringBean("tagDaoImpl")
	private ITagDAO tagDAO;
	@SpringBean("authorDaoImpl")
	private IAuthorDAO authorDAO;
	@SpringBean("commentDaoImpl")
	private ICommentDAO commentDAO;

	

	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void addNewsTest() throws DAOExeption {
		News expectedNews = new News();
		News resultNews = null;
		Long returnId = null;
		expectedNews.setNewsTitle("test titlle");
		expectedNews.setNewsShortText("short test text");
		expectedNews.setNewsFullText("full test text");
		returnId = newsDAO.addItem(expectedNews);
		resultNews = newsDAO.readItem(returnId);
		assertEquals(expectedNews.getNewsFullText(), resultNews.getNewsFullText());
	}
	
	@Test
	public void editNewsTest() throws DAOExeption {
		News expectedNews = new News();
		News resultNews = null;
		expectedNews.setNewsId(1L);
		expectedNews.setNewsTitle("test titlle");
		expectedNews.setNewsShortText("short test text");
		expectedNews.setNewsFullText("full test text");
		newsDAO.editItem(expectedNews);
		resultNews = newsDAO.readItem(1L);
		assertEquals(expectedNews.getNewsFullText(), resultNews.getNewsFullText());
	}
	
	@Test
	public void deleteNewsTest() throws DAOExeption {
		News resultNews = null;
		List<Long> newsIdList = new ArrayList<Long>();
		newsIdList.add(1L);
		tagDAO.unlink(1L);
		authorDAO.unlink(1L);
		commentDAO.deleteNewsComments(1L);
		newsDAO.deleteaQuantityNews(newsIdList);
		resultNews = newsDAO.readItem(1L);
		assertNull(resultNews);
	}
	
	@Test
	public void readNewsTest() throws DAOExeption {
		News expectedNews = new News();
		News resultNews = null;
		expectedNews.setNewsId(1L);
		expectedNews.setNewsTitle("test titlle");
		expectedNews.setNewsShortText("test short text");
		expectedNews.setNewsFullText("test full text");
		newsDAO.readItem(1L);
		resultNews = newsDAO.readItem(1L);
		assertEquals(expectedNews.getNewsFullText(), resultNews.getNewsFullText());
	}
	
	@Test
	public void countAllNewsTest() throws DAOExeption {
		newsDAO.countAllNews();
	}
	
	@Test
	public void searchNewsTest() throws DAOExeption {
	
		List<NewsManagementVO> resultNewsManagementVOList = new ArrayList<NewsManagementVO>();
		
		SearchCriteria searchCriteria = new SearchCriteria();
		Author authorItem = new Author();
		authorItem.setAuthorId(1L);
		
		List<Long> tagsIdList = new ArrayList<Long>();
	
		searchCriteria.setAuthor(authorItem);
		//searchCriteria.setTagsIdList(tagsIdList);
		
		newsDAO.searchNews(searchCriteria);
		resultNewsManagementVOList = newsDAO.searchNews(searchCriteria);
		assertNotNull(resultNewsManagementVOList);
		
	}
	
	@Test
	public void showSinglNewsTest() throws DAOExeption {	
		newsDAO.showSinglNews(1L);
	}
}
