package com.epam.rudoi.newsportal.dao;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.After;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@SpringApplicationContext({"context_spring_test.xml"})
@DataSet(value = "DataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class CommentDAOTest extends UnitilsJUnit4{
	
	@SpringBean("commentDaoImpl")
	private ICommentDAO commentDAO;

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addCommentTest() throws DAOExeption {
		Comment expectedComment = new Comment();
		Comment resultComment = null;
		Long returnId = null;
		expectedComment.setNewsId(1L);
		expectedComment.setCommentText("test adding comment!!!!!!!!!!!!!!!");
		returnId = commentDAO.addItem(expectedComment);
		resultComment = commentDAO.readItem(returnId);
		assertEquals(expectedComment.getCommentText(), resultComment.getCommentText());
		
	}

	@Test
	public void editCommentTest() throws DAOExeption {
		Comment expectedComment = new Comment();
		Comment resaultComment = null;
		expectedComment.setCommentId(1L);
		expectedComment.setNewsId(1L);
		expectedComment.setCommentText("WOW!WOW!WOW!");
		expectedComment.setCreationDate(null);
		commentDAO.editItem(expectedComment);
		resaultComment = commentDAO.readItem(1L);
		assertEquals(expectedComment.getCommentText(), resaultComment.getCommentText());

	}

	@Test
	public void readCommentTest() throws DAOExeption {
		Comment expectedComment = new Comment();
		Comment resaultComment = null;
		expectedComment.setCommentId(1L);
		expectedComment.setNewsId(1L);
		expectedComment.setCommentText("WOW! Wonderfull!");
		commentDAO.readItem(1L);
		resaultComment = commentDAO.readItem(1L);
		assertEquals(expectedComment.getCommentText(), resaultComment.getCommentText());
	}

	@Test
	public void deleteCommentTest() throws DAOExeption {
		Comment resultComment = null;
		commentDAO.deleteItem(1L);
		resultComment = commentDAO.readItem(1L);
		assertNull(resultComment);
	}

}
