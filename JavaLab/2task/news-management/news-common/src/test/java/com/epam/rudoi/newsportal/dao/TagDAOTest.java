package com.epam.rudoi.newsportal.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

@SpringApplicationContext({"context_spring_test.xml"})
@DataSet(value = "DataSetDAO.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class TagDAOTest extends UnitilsJUnit4{

	@SpringBean("tagDaoImpl")
	private ITagDAO tagDAO;

	
	@Test
	public void addTagTest() throws DAOExeption {
		Tag expectedTag = new Tag();
		Tag resultTag = null;
		Long returnId = null;
		expectedTag.setTagId(4L);
		expectedTag.setTagName("test adding tag");
		returnId = tagDAO.addItem(expectedTag);
		resultTag = tagDAO.readItem(returnId);
		assertEquals(expectedTag.getTagName(), resultTag.getTagName());
	}

	@Test
	public void editTagTest() throws DAOExeption {
		Tag expectedTag = new Tag();
		Tag resultTag = null;
		expectedTag.setTagId(1L);
		expectedTag.setTagName("war,war,war");
		tagDAO.editItem(expectedTag);
		resultTag = tagDAO.readItem(1L);
		assertEquals(expectedTag, resultTag);
	}
	
	@Test
	public void deleteTagTest() throws DAOExeption {
		Tag resultTag = null;
		tagDAO.unlinkByTagId(1L);
		tagDAO.deleteItem(1L);
		resultTag = tagDAO.readItem(1L);
		assertNull(resultTag);
	}
	
	@Test
	public void readTagTest() throws DAOExeption {
		Tag expectedTag = new Tag();
		Tag resultTag = null;
		expectedTag.setTagId(1L);
		expectedTag.setTagName("war");
		tagDAO.readItem(expectedTag.getTagId());
		resultTag = tagDAO.readItem(1L);
		assertEquals(expectedTag, resultTag);
	}
	
	@Test
	public void linkTagTest() throws DAOExeption {
		List<Long> tagIdList = new ArrayList<Long>(); 
		tagDAO.link(tagIdList, 1L);
	}
	

	@Test
	public void unlinkTagTest() throws DAOExeption {
		tagDAO.unlink(1L);
	}
	

	@Test
	public void unlinkByTagIdTest() throws DAOExeption {
		tagDAO.unlinkByTagId(1L);
	}
}
