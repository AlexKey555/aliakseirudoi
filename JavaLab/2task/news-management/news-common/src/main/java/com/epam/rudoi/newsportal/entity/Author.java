package com.epam.rudoi.newsportal.entity;

import java.sql.Timestamp;

import javax.validation.constraints.Size;

public class Author {

	private Long authorId;
	private String authorName;
	private Timestamp expider;
	
	public Author() {
	    super();
	}
	
	public Author(Long authorId, String authorName,Timestamp expider) {
	    super();
	    this.authorId = authorId;
	    this.authorName = authorName;
	    this.expider = expider;
	}
	
	public Author(Long authorId, String authorName) {
	    super();
	    this.authorId = authorId;
	    this.authorName = authorName;
	}
	
	public Author(Long authorId) {
	    super();
	    this.authorId = authorId;
	}
	public Author(String authorName) {
	    super();
	    this.authorName = authorName;
	}
	
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Timestamp getExpider() {
		return expider;
	}
	public void setExpider(Timestamp expider) {
		this.expider = expider;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expider == null) ? 0 : expider.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expider == null) {
			if (other.expider != null)
				return false;
		} else if (!expider.equals(other.expider))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AuthorItem [authorId=" + authorId + ", authorName="
				+ authorName + ", expider=" + expider + "]";
	}
	
	

}
