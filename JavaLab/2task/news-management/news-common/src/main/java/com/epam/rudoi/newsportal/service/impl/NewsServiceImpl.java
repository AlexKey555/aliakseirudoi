package com.epam.rudoi.newsportal.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.INewsService;

public class NewsServiceImpl implements INewsService {

    private static final Logger LOGGER = Logger.getLogger(NewsServiceImpl.class);
    private INewsDAO newsDAO;

    public void setNewsDAO(INewsDAO newsDAO) {
	this.newsDAO = newsDAO;
    }

    public Long addNews(News newsItem) throws ServiceExeption {
	Long newsId = null;
	try {
	    newsId = newsDAO.addItem(newsItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Adding news exeption", e);
	    throw new ServiceExeption();
	}

	return newsId;
    }

    public void editNews(News newsItem) throws ServiceExeption {
	try {
	    newsDAO.editItem(newsItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing news exeption", e);
	    throw new ServiceExeption();
	}
    }

    public Long countAllNews() throws ServiceExeption {
	Long totalNewsCount = null;
	try {
	    totalNewsCount = newsDAO.countAllNews();
	} catch (DAOExeption e) {
	    LOGGER.error("Counting all news exeption", e);
	    throw new ServiceExeption(e);
	}
	return totalNewsCount;
    }

    public NewsManagementVO showSingleNews(Long newsId) throws ServiceExeption {
	NewsManagementVO newsManagementVO = null;
	try {
	    newsManagementVO = newsDAO.showSinglNews(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Showing news exeption", e);
	    throw new ServiceExeption(e);
	}
	return newsManagementVO;
    }

    public List<NewsManagementVO> SearchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	List<NewsManagementVO> newsVOList = null;
	try {

	    newsVOList = newsDAO.searchNews(searchCriteria);
	} catch (DAOExeption e) {
	    LOGGER.error("Search news exeption", e);
	    throw new ServiceExeption(e);
	}

	return newsVOList;
    }

    @Override
    public void deleteaQuantityNews(List<Long> newsIdList) throws ServiceExeption {

	try {
	    newsDAO.deleteaQuantityNews(newsIdList);
	} catch (DAOExeption e) {
	    LOGGER.error("deleting news exeption", e);
	    throw new ServiceExeption(e);
	}

    }

    @Override
    public List<Long> takeNewsIdList() throws ServiceExeption {
	List<Long> newsIdList;
	try {
	    newsIdList = newsDAO.takeNewsIdList();
	} catch (DAOExeption e) {
	    LOGGER.error("take news list exeption", e);
	    throw new ServiceExeption(e);
	}
	return newsIdList;
    }

}
