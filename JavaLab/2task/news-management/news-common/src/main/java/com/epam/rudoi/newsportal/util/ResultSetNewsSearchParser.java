package com.epam.rudoi.newsportal.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

public class ResultSetNewsSearchParser {

    public List<NewsManagementVO> parseResultSetNewsSearch(ResultSet resultSet) throws DAOExeption{
	

	List<NewsManagementVO> newsVOList = new ArrayList<NewsManagementVO>();
	Long row = null;
	List<Tag> tagList = new ArrayList<Tag>();
	List<Long> tagListId = new ArrayList<Long>();
	NewsManagementVO newsVO = new NewsManagementVO();

	try {

	    while (resultSet.next()) {

		News news = new News();
		Tag tag = new Tag();
		Author author = new Author();

		if (row != null) {

		    if (row == resultSet.getLong(2)) {
			tag.setTagId(resultSet.getLong(12));
			tag.setTagName(resultSet.getString(13));
			tagListId.add(resultSet.getLong(12));
			tagList.add(tag);

			continue;

		    } else {
			newsVOList.add(newsVO);

			newsVO = new NewsManagementVO();

			tagList = new ArrayList<Tag>();

			row = resultSet.getLong(2);

			news.setNewsId(resultSet.getLong(2));
			news.setNewsTitle(resultSet.getString(3));
			news.setNewsShortText(resultSet.getString(4));
			news.setNewsFullText(resultSet.getString(5));
			news.setNewsCreationDate(resultSet.getTimestamp(6));
			news.setNewsModificationDate(resultSet.getDate(7));
			news.setNewsCommentAmmount(resultSet.getLong(8));

			author.setAuthorId(resultSet.getLong(9));
			author.setAuthorName(resultSet.getString(10));

			tag.setTagId(resultSet.getLong(12));
			tag.setTagName(resultSet.getString(13));

			tagListId.add(resultSet.getLong(12));
			tagList.add(tag);

			newsVO.setAuthor(author);
			newsVO.setNews(news);
			newsVO.setTagsIdList(tagListId);
			newsVO.setTagsList(tagList);

		    }

		} else {

		    row = resultSet.getLong(2);

		    news.setNewsId(resultSet.getLong(2));
		    news.setNewsTitle(resultSet.getString(3));
		    news.setNewsShortText(resultSet.getString(4));
		    news.setNewsFullText(resultSet.getString(5));
		    news.setNewsCreationDate(resultSet.getTimestamp(6));
		    news.setNewsModificationDate(resultSet.getDate(7));
		    news.setNewsCommentAmmount(resultSet.getLong(8));

		     author.setAuthorId(resultSet.getLong(9));
		    author.setAuthorName(resultSet.getString(10));

		    tag.setTagId(resultSet.getLong(12));
		    tag.setTagName(resultSet.getString(13));

		    tagListId.add(resultSet.getLong(12));
		    tagList.add(tag);

		    newsVO.setAuthor(author);
		    newsVO.setNews(news);

		    newsVO.setTagsIdList(tagListId);
		    newsVO.setTagsList(tagList);

		}

	    }

	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    newsVOList.add(newsVO);
	}

	return newsVOList;
	
	
	
    }
    
    
}
