package com.epam.rudoi.newsportal.dao.impl;

import static com.epam.rudoi.newsportal.constants.ParamConstants.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;
import com.epam.rudoi.newsportal.util.QueryBuilder;

public class CommentDAOImpl implements ICommentDAO {

	private static final String SQL_ADD_COMMENT = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (sq_comment_id_seq.nextval,?,?,current_timestamp)";
	private static final String SQL_EDIT_COMMENT = "UPDATE comments SET comment_text=?";
	private static final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE comment_id=?";
	private static final String SQL_UNLINK_COMMENT = "DELETE FROM comments WHERE news_id=?";
	private static final String SQL_UNLINK_NEWS_WITH_COMMENTS_QUENTITY = "DELETE FROM comments WHERE news_id IN(";
	private static final String SQL_GET_COMMENT = "SELECT a.comment_id, a.news_id, a.comment_text, a.creation_date FROM comments a WHERE a.comment_id = ?";
	private static final String SQL_GET_COMMENT_LIST = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE news_id = ?";
	private static final String SQL_DELETE_ALL_NEWS_COMMENTS = "DELETE FROM comments WHERE news_id=?";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addItem(Comment comment) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Long commentId = null;
		String[] column = {"COMMENT_ID"};
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD_COMMENT, column);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if((resultSet != null)&&(resultSet.next())){
				commentId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
		DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}

		return commentId;
	}

	public void deleteItem(Long commentId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, commentId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	@Override 
	public void deleteNewsComments(Long newsId) throws DAOExeption {
	    PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_ALL_NEWS_COMMENTS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}
	}
	
	public void editItem(Comment comment) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_EDIT_COMMENT);
			preparedStatement.setString(1, comment.getCommentText());
			
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	public Comment readItem(Long commentId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Comment comment = null;
		
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_COMMENT);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null)&&(resultSet.next())){
			    comment = new Comment(resultSet.getLong(1), resultSet.getLong(2), resultSet.getString(3), resultSet.getTimestamp(4));
			}
		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}
			
		return comment;
		
	}

	public void unlink(Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_UNLINK_COMMENT);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}
	
	@Override
	public void unlink(List<Long> newsIdList) throws DAOExeption {

	    PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
		    QueryBuilder qb = new QueryBuilder();
		    String queryForUnlinkComments = qb.buildQueryForStatment(SQL_UNLINK_NEWS_WITH_COMMENTS_QUENTITY, newsIdList);
		    connection = DataSourceUtils.doGetConnection(dataSource);
		    preparedStatement = connection.prepareStatement(queryForUnlinkComments);
		    int i = PARAM_SIZE_FOR_ITER_1PARAM;
		    for (Long newsId : newsIdList) {
			preparedStatement.setLong(i, newsId);
			i++;
		    }
		    preparedStatement.executeUpdate();

		} catch (SQLException e) {
		    throw new DAOExeption(e);
		} finally {
		    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	    
	}

	public List<Comment> getCommentList(Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		List<Comment> commentList = new ArrayList<Comment>();
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_COMMENT_LIST);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			commentList = parseResultSetGetCommentList(resultSet);
		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}
		
		return commentList;
	}

	private List<Comment> parseResultSetGetCommentList(ResultSet resultSet)
			throws SQLException {
		List<Comment> commentList = new ArrayList<Comment>();
		while (resultSet.next()) {
			Comment comment = new Comment(resultSet.getLong(1), resultSet.getLong(2), resultSet.getString(3), resultSet.getTimestamp(4));
			commentList.add(comment);			
		}
		return commentList;
	}
}
