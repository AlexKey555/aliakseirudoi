package com.epam.rudoi.newsportal.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import static com.epam.rudoi.newsportal.constants.ParamConstants.*;

import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;
import com.epam.rudoi.newsportal.util.ParamForStatmentBuilder;
import com.epam.rudoi.newsportal.util.QueryBuilder;
import com.epam.rudoi.newsportal.util.ResultSetNewsSearchParser;


public class NewsDAOImpl implements INewsDAO {

    private static final String SQL_ADD_NEWS = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (sq_news_id_seq.nextval,?,?,?,current_timestamp,sysdate)";
    private static final String SQL_EDIT_NEWS = "UPDATE news SET title=?, short_text=?, full_text=?, modification_date=sysdate WHERE news_id=?";
    private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE news_id IN(";
    private static final String SQL_GET_NEWS = "SELECT a.news_id, a.title, a.short_text, a.full_text, a.creation_date, a.modification_date FROM news a WHERE a.news_id = ?";
    private static final String SQL_SHOW_SINGL_NEWS = "SELECT a.news_id, a.title, a.short_text, a.full_text, a.creation_date, a.modification_date, c.author_name FROM news a"
	    + " INNER JOIN news_authors b ON a.news_id = b.news_id"
	    + " INNER JOIN authors c ON b.author_id = c.author_id" + " WHERE a.news_id=?";
    private static final String SQL_SHOW_NEWS_COUNT = "SELECT COUNT(DISTINCT a.news_id) FROM news a";
    private static final String SQL_SORT_NEWS_BY_MOST_COMMENTS_MODIFICAT_DATE = "SELECT ROW_NUMBER,news_id,title,short_text,full_text,creation_date,modification_date,count_comments,author_id,author_name,count_news,tag_id,tag_name"
	    + "	FROM"
	    + "	(SELECT ROW_NUMBER() OVER (ORDER BY count_comments DESC,modification_date,all_news.news_id,t.tag_id)"
	    + "	AS ROW_NUMBER,all_news.news_id,title,short_text,full_text,creation_date,modification_date,count_comments,author_id,author_name,count_news,t.tag_id,nt.tag_name"
	    + "	FROM (SELECT news_id,title,short_text,full_text,creation_date,modification_date,count_comments,author_id,author_name,COUNT(*) OVER() AS count_news"
	    + "	FROM (SELECT SUM(NVL2(dn,1,0)) AS count_comments, news_id, title, short_text, full_text, creation_date, modification_date, author_id,author_name"
	    + "	FROM (SELECT a.news_id, d.NEWS_ID dn, a.title, a.short_text, a.full_text, a.creation_date, d.comment_id, a.modification_date, c.author_id, f.author_name"
	    + "	FROM news a LEFT JOIN comments d ON a.news_id = d.news_id INNER JOIN news_authors c ON a.news_id = c.news_id"
	    + "	INNER JOIN authors f ON c.author_id = f.author_id)"
	    + "	GROUP BY news_id,title,short_text,full_text,creation_date,modification_date,author_id,author_name)"
	    + "	) all_news INNER JOIN NEWS_TAGS t ON all_news.news_id=t.news_id"
	    + "	INNER JOIN TAGS nt ON t.tag_id=nt.tag_id"
	    + "	ORDER BY count_comments DESC,modification_date,all_news.news_id,t.tag_id"
	    + "	) all_row"
	    + " {0} {1}"
	    + "	ORDER BY count_comments DESC,modification_date DESC,news_id,tag_id";

    private static final String SQL_SEARCH_NEWS_AUTHOR_JOIN = " WHERE author_id IN(?) ";

    private static final String SQL_SEARCH_NEWS_TAGS_JOIN = " WHERE tag_id IN(";

    private static final String SQL_SEARCH_NEWS_AUTHORS_AND_TAGS = "WHERE author_id IN(?) AND tag_id IN(";

    private static final String SQL_SORT_NEWS_PARAM_GAG = " ";
    
    private static final String SQL_TAKE_NEWS_ID_LIST = " SELECT news_id FROM news ";
    
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    public Long addItem(News news) throws DAOExeption {

	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	Long newsId = null;
	String[] column = { "NEWS_ID" };
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_ADD_NEWS, column);
	    preparedStatement.setString(1, news.getNewsTitle());
	    preparedStatement.setString(2, news.getNewsShortText());
	    preparedStatement.setString(3, news.getNewsFullText());
	    preparedStatement.executeUpdate();
	    resultSet = preparedStatement.getGeneratedKeys();
	    if ((resultSet != null) && (resultSet.next())) {
		newsId = resultSet.getLong(1);

	    }
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return newsId;

    }

    public void editItem(News news) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_EDIT_NEWS);
	    preparedStatement.setString(1, news.getNewsTitle());
	    preparedStatement.setString(2, news.getNewsShortText());
	    preparedStatement.setString(3, news.getNewsFullText());
	    preparedStatement.setLong(4, news.getNewsId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    public void deleteItem(Long newsId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    @Override
    public void deleteaQuantityNews(List<Long> newsIdList) throws DAOExeption {

	PreparedStatement preparedStatement = null;
	Connection connection = null;

	try {
	    QueryBuilder qb = new QueryBuilder();
	    String queryForDeletion = qb.buildQueryForStatment(SQL_DELETE_NEWS, newsIdList);
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(queryForDeletion);
	    int i = PARAM_SIZE_FOR_ITER_1PARAM;
	    for (Long newsId : newsIdList) {
		preparedStatement.setLong(i, newsId);
		i++;
	    }
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    public News readItem(Long newsId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	News news = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_GET_NEWS);
	    preparedStatement.setLong(1, newsId);
	    resultSet = preparedStatement.executeQuery();
	    if ((resultSet != null) && (resultSet.next())) {
		news = new News(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getTimestamp(5), resultSet.getDate(6));
	    }
	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return news;

    }

    public Long countAllNews() throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	Long totalNewsCount = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_SHOW_NEWS_COUNT);
	    resultSet = preparedStatement.executeQuery();
	    if ((resultSet != null) && (resultSet.next())) {
		totalNewsCount = resultSet.getLong(1);
	    }

	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return totalNewsCount;
    }

    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws DAOExeption {
	ResultSetNewsSearchParser rsP = new ResultSetNewsSearchParser();
	//ParamForStatmentBuilder pb = new ParamForStatmentBuilder();
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	List<NewsManagementVO> newsVOList = new ArrayList<NewsManagementVO>();
	String searchQ = buildSearchQuery(searchCriteria);
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(searchQ);
	    ParamForStatmentBuilder.buidlSearchParam(searchCriteria, preparedStatement);
	    resultSet = preparedStatement.executeQuery();
	    newsVOList = rsP.parseResultSetNewsSearch(resultSet);
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return newsVOList;
    }

    private String buildSearchQuery(SearchCriteria searchCriteria) {
	String querySearchNews = null;
	String searchNewsTagsJoin = null;
	//QueryBuilder qb = new QueryBuilder();
	searchNewsTagsJoin = QueryBuilder.buildQueryByParam(searchCriteria, SQL_SEARCH_NEWS_AUTHORS_AND_TAGS,
		SQL_SEARCH_NEWS_TAGS_JOIN);
	querySearchNews = QueryBuilder.buildQueryForStatment(searchCriteria, SQL_SORT_NEWS_BY_MOST_COMMENTS_MODIFICAT_DATE,
		SQL_SEARCH_NEWS_AUTHOR_JOIN, SQL_SORT_NEWS_PARAM_GAG, searchNewsTagsJoin);
	return querySearchNews;
    }

    public NewsManagementVO showSinglNews(Long newsId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	News news = null;
	Author author = null;
	NewsManagementVO newsManagementVO = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_SHOW_SINGL_NEWS);
	    preparedStatement.setLong(1, newsId);
	    resultSet = preparedStatement.executeQuery();
	    if ((resultSet != null) && (resultSet.next())) {
		
		news = new News(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getTimestamp(5), resultSet.getDate(6));
		author = new Author(resultSet.getString(7));
		newsManagementVO = new NewsManagementVO(news, author);
	    }
	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return newsManagementVO;
    }

    @Override
    public List<Long> takeNewsIdList() throws DAOExeption {
	
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	List<Long> newsIdList = new ArrayList<Long>();
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_TAKE_NEWS_ID_LIST);
	    resultSet = preparedStatement.executeQuery();
	    while(resultSet.next()) {
		newsIdList.add(resultSet.getLong(1));
	    }
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return newsIdList;
    }
}
