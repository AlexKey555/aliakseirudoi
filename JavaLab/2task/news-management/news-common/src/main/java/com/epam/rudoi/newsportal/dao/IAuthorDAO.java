package com.epam.rudoi.newsportal.dao;

import java.util.List;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

/**
 * The Interface IAuthorDAO.
 */
public interface IAuthorDAO extends ICrudDAO<Author>{
	
	/**
	 * Link.
	 * This method link author with news  
	 * @param authorId the author id
	 * @param newsId the news id
	 * @throws DAOExeption the DAO exception
	 */
	void link (Long authorId, Long newsId) throws DAOExeption;
	
	/**
	 * Unlink.
	 * This method unlink author with news
	 * @param newsId the news id
	 * @throws DAOExeption the DAO exception
	 */
	void unlink (Long newsId) throws DAOExeption;
	
	/**
	 * Gets the authors list.
	 * This method get authors list
	 * @return the authors list
	 * @throws DAOExeption the DAO exception
	 */
	List<Author> getAuthorsList () throws DAOExeption;
	
	/**
	 * Unlink.
	 * This method unlink qentity news with each authors
	 * @param newsIdList the news id list
	 * @throws DAOExeption the DAO exception
	 */
	void unlink (List<Long> newsIdList) throws DAOExeption;

	/**
	 * Expired author.
	 * This method expired author
	 * @param authorId the author id
	 * @throws DAOExeption the DAO exception
	 */
	void expiredAuthor(Long authorId) throws DAOExeption;
	
}
