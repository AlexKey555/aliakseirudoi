package com.epam.rudoi.newsportal.service;

import java.util.List;

import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

// TODO: Auto-generated Javadoc
/**
 * The Interface INewsService.
 */
public interface INewsService {

	/**
	 * Adds the news.
	 * This method add news
	 * @param newsItem the news item
	 * @return the long
	 * @throws ServiceExeption the service exception
	 */
	Long addNews(News newsItem) throws ServiceExeption;

	/**
	 * Edits the news.
	 * This method edit news
	 * @param newsItem the news item
	 * @throws ServiceExeption the service exception
	 */
	void editNews(News newsItem) throws ServiceExeption;

	/**
	 * Deletea quantity news.
	 * This method delete aentity news
	 * @param newsIdList the news id list
	 * @throws ServiceExeption the service exception
	 */
	void deleteaQuantityNews(List<Long> newsIdList) throws ServiceExeption; 

	/**
	 * Show single news.
	 * This method show one news
	 * @param newsId the news id
	 * @return the news management vo
	 * @throws ServiceExeption the service exception
	 */
	NewsManagementVO showSingleNews(Long newsId) throws ServiceExeption;

	/**
	 * Count all news.
	 * This method counts all news
	 * @return the long
	 * @throws ServiceExeption the service exception
	 */
	Long countAllNews() throws ServiceExeption;

	/**
	 * Search news.
	 * This method search news by param
	 * @param searchCriteria the search criteria
	 * @return the list
	 * @throws ServiceExeption the service exception
	 */
	List<NewsManagementVO> SearchNews(SearchCriteria searchCriteria) throws ServiceExeption;
	
	/**
	 * Take news id list.
	 * This method take news id list
	 * @return the list
	 * @throws ServiceExeption the service exception
	 */
	List<Long> takeNewsIdList () throws ServiceExeption;
		

}
