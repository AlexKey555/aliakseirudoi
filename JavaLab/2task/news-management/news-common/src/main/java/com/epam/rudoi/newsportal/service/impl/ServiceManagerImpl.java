package com.epam.rudoi.newsportal.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.IAuthorService;
import com.epam.rudoi.newsportal.service.ICommentService;
import com.epam.rudoi.newsportal.service.INewsManagementService;
import com.epam.rudoi.newsportal.service.INewsService;
import com.epam.rudoi.newsportal.service.ITagService;

@Transactional(rollbackFor = Exception.class)
public class ServiceManagerImpl implements INewsManagementService {

    private IAuthorService authorService;
    private ICommentService commentService;
    private INewsService newsService;
    private ITagService tagService;

    public void setAuthorService(IAuthorService authorService) {
	this.authorService = authorService;
    }

    public void setCommentService(ICommentService commentService) {
	this.commentService = commentService;
    }

    public void setNewsService(INewsService newsService) {
	this.newsService = newsService;
    }

    public void setTagService(ITagService tagService) {
	this.tagService = tagService;
    }

    public void addNews(NewsManagementVO newsManagementVO) throws ServiceExeption {
	Long newsId = newsService.addNews(newsManagementVO.getNews());
	authorService.link(newsManagementVO.getAuthor().getAuthorId(), newsId);
	tagService.link(newsManagementVO.getTagsIdList(), newsId);

    }

    public void editNews(NewsManagementVO newsManagementVO) throws ServiceExeption {
	newsService.editNews(newsManagementVO.getNews());
	authorService.editAuthor(newsManagementVO.getAuthor());
	tagService.editTag(newsManagementVO.getTag());
    }

    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws ServiceExeption {
	List<NewsManagementVO> newsVOList = newsService.SearchNews(searchCriteria);
	return newsVOList;
    }

    public NewsManagementVO showSinglNews(Long newsId) throws ServiceExeption {
	NewsManagementVO newsManagementVO = newsService.showSingleNews(newsId);
	return newsManagementVO;
    }

    public Long countAllNews() throws ServiceExeption {
	Long totalNewsCount = newsService.countAllNews();
	return totalNewsCount;
    }

    public void deleteNews(NewsManagementVO newsManagementVO) throws ServiceExeption {
	authorService.unlink(newsManagementVO.getNewsIdList());
	tagService.unlink(newsManagementVO.getNewsIdList());
	commentService.unlink(newsManagementVO.getNewsIdList());
	newsService.deleteaQuantityNews(newsManagementVO.getNewsIdList());
    }

    @Override
    public List<Long> takeNewsIdList() throws ServiceExeption {
	List<Long> newsIdList = newsService.takeNewsIdList();
	return newsIdList;
    }

    public Long addComment(Comment commentItem) throws ServiceExeption {
	return commentService.addComment(commentItem);

    }

    public void deleteComments(Long commentId) throws ServiceExeption {
	commentService.deleteComment(commentId);

    }

    public List<Comment> getCommentsList(Long newsId) throws ServiceExeption {
	List<Comment> commentsList = commentService.getCommentList(newsId);
	return commentsList;
    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceExeption {
	commentService.deleteNewsComments(newsId);

    }

    public void addTag(NewsManagementVO newsManagementVO) throws ServiceExeption {
	tagService.addTag(newsManagementVO);

    }

    public void deleteTag(Long tagId) throws ServiceExeption {
	tagService.deleteTag(tagId);

    }

    public List<Tag> getTagsList() throws ServiceExeption {
	List<Tag> tagsList = null;
	tagsList = tagService.getTagsList();
	return tagsList;
    }

    public List<Long> getTagIdList() throws ServiceExeption {
	List<Long> tagsIdList = null;
	tagsIdList = tagService.getTagIdList();
	return tagsIdList;
    }

    public List<Author> getAuthorsList() throws ServiceExeption {
	List<Author> authorsList = null;
	authorsList = authorService.getAuthorsList();
	return authorsList;
    }

    @Override
    public void addAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	authorService.addAuthor(newsManagementVO);
    }

    @Override
    public void editAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	authorService.editAuthor(newsManagementVO.getAuthor());
    }

    @Override
    public void expiredAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption {
	authorService.expiredAuthor(newsManagementVO.getAuthor().getAuthorId());

    }

    @Override
    public void editTag(NewsManagementVO newsManagementVO) throws ServiceExeption {
	tagService.editTag(newsManagementVO.getTag());
    }

}
