package com.epam.rudoi.newsportal.dao.impl;

import static com.epam.rudoi.newsportal.constants.ParamConstants.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;
import com.epam.rudoi.newsportal.util.QueryBuilder;

public class TagDAOImpl implements ITagDAO {

    private static final String SQL_ADD_TAG = "INSERT INTO tags (tag_id, tag_name) VALUES (sq_tag_id_seq.nextval,?)";
    private static final String SQL_EDIT_TAG = "UPDATE tags SET tag_name=? WHERE tag_id=?";
    private static final String SQL_DELETE_TAG = "DELETE FROM tags WHERE tag_id=?";
    private static final String SQL_LINK_NEWS_WITH_TAG = "INSERT INTO news_tags (news_id, tag_id) VALUES (?,?)";
    private static final String SQL_UNLINK_NEWS_WITH_TAG = "DELETE FROM news_tags WHERE news_id=?";
    private static final String SQL_UNLINK_NEWS_WITH_TAG_BY_TAG_ID = "DELETE FROM news_tags WHERE tag_id=?";
    private static final String SQL_UNLINK_NEWS_WITH_TAGS_QUENTITY = "DELETE FROM news_tags WHERE news_id IN(";
    private static final String SQL_GET_TAG = "SELECT a.tag_id, a.tag_name FROM tags a WHERE a.tag_id = ?";
    private static final String SQL_GET_TAGS_LIST = "SELECT a.tag_id, a.tag_name FROM tags a";
    private static final String SQL_GET_TAGS_ID_LIST = "SELECT a.tag_id FROM tags a";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    public Long addItem(Tag teg) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	Long tagId = null;
	String[] column = { "TAG_ID" };
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_ADD_TAG, column);
	    preparedStatement.setString(1, teg.getTagName());
	    preparedStatement.executeUpdate();
	    resultSet = preparedStatement.getGeneratedKeys();
	    if ((resultSet != null) && (resultSet.next())) {
		tagId = resultSet.getLong(1);
	    }

	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}

	return tagId;
    }

    public void deleteItem(Long tagId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
	    preparedStatement.setLong(1, tagId);
	    preparedStatement.executeUpdate();

	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}

    }

    public void editItem(Tag tag) throws DAOExeption {

	PreparedStatement preparedStatement = null;
	Connection connection = null;

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_EDIT_TAG);
	    preparedStatement.setString(1, tag.getTagName());
	    preparedStatement.setLong(2, tag.getTagId());
	    preparedStatement.executeUpdate();

	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}

    }

    public Tag readItem(Long tagId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	Tag tag = null;

	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_GET_TAG);
	    preparedStatement.setLong(1, tagId);
	    resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		tag = new Tag(resultSet.getLong(1), resultSet.getString(2));
	    }
	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return tag;
    }

    public void link(List<Long> tagIdList, Long newsId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    connection.setAutoCommit(false);
	    preparedStatement = connection.prepareStatement(SQL_LINK_NEWS_WITH_TAG);
	    for (Long i : tagIdList) {
		preparedStatement.setLong(1, newsId);
		preparedStatement.setLong(2, i);
		preparedStatement.addBatch();
	    }
	    preparedStatement.executeBatch();
	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    public void unlink(Long newsId) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_UNLINK_NEWS_WITH_TAG);
	    preparedStatement.setLong(1, newsId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    public void unlinkByTagId(Long tagId) throws DAOExeption {

	PreparedStatement preparedStatement = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_UNLINK_NEWS_WITH_TAG_BY_TAG_ID);
	    preparedStatement.setLong(1, tagId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOExeption();
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    @Override
    public void unlink(List<Long> newsIdList) throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	try {
	    QueryBuilder qb = new QueryBuilder();
	    String queryForUnlinkTags = qb.buildQueryForStatment(SQL_UNLINK_NEWS_WITH_TAGS_QUENTITY, newsIdList);
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(queryForUnlinkTags);
	    int i = PARAM_SIZE_FOR_ITER_1PARAM;
	    for (Long newsId : newsIdList) {
		preparedStatement.setLong(i, newsId);
		i++;
	    }
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
	}
    }

    public List<Tag> getTagsList() throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	List<Tag> tagsList = new ArrayList<Tag>();
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_GET_TAGS_LIST);
	    resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		Tag tag = new Tag(resultSet.getLong(1), resultSet.getString(2));
		tagsList.add(tag);
	    }
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return tagsList;
    }

    public List<Long> getTagsIdList() throws DAOExeption {
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	ResultSet resultSet = null;
	List<Long> tagsIdList = new ArrayList<Long>();
	try {
	    connection = DataSourceUtils.doGetConnection(dataSource);
	    preparedStatement = connection.prepareStatement(SQL_GET_TAGS_ID_LIST);
	    resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		tagsIdList.add(resultSet.getLong(1));
	    }
	} catch (SQLException e) {
	    throw new DAOExeption(e);
	} finally {
	    DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
	}
	return tagsIdList;
    }
}
