package com.epam.rudoi.newsportal.entity;

import java.sql.Timestamp;
import java.sql.Date;

import javax.validation.constraints.Size;

public class News {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5654015138645077722L;
	
	private Long newsId;
	private String newsTitle;
	private String newsShortText;
	private String newsFullText;
	private Timestamp newsCreationDate;
	private Date newsModificationDate;
	private Long newsCount;
	private Long newsCommentAmmount;
	
	public News() {
	    super();
	    this.newsId = newsId;
	}
	
	public News(Long newsId) {
	    super();
	}
	
	public News(Long newsId, String newsTitle, String newsShortText, String newsFullText) {
	    super();
	    this.newsId = newsId;
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	}
	
	public News(Long newsId, String newsTitle, String newsShortText, String newsFullText, Timestamp newsCreationDate, Date newsModificationDate) {
	    super();
	    this.newsId = newsId;
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	    this.newsCreationDate = newsCreationDate;
	    this.newsModificationDate = newsModificationDate;
	}
	
	public News(Long newsId, String newsTitle, String newsShortText, String newsFullText, Long newsCount, Long newsCommentAmmount) {
	    super();
	    this.newsId = newsId;
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	    this.newsCount = newsCount;
	    this.newsCommentAmmount = newsCommentAmmount;
	}
	
	public News(String newsTitle, String newsShortText, String newsFullText) {
	    super();
	    this.newsTitle = newsTitle;
	    this.newsShortText = newsShortText;
	    this.newsFullText = newsFullText;
	}
	
	public Long getNewsId() {
		return newsId;
	}
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	public String getNewsTitle() {
		return newsTitle;
	}
	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	public String getNewsShortText() {
		return newsShortText;
	}
	public void setNewsShortText(String newsShortText) {
		this.newsShortText = newsShortText;
	}
	public String getNewsFullText() {
		return newsFullText;
	}
	public void setNewsFullText(String newsFullText) {
		this.newsFullText = newsFullText;
	}
	public Timestamp getNewsCreationDate() {
		return newsCreationDate;
	}
	public void setNewsCreationDate(Timestamp newsCreationDate) {
		this.newsCreationDate = newsCreationDate;
	}
	public Date getNewsModificationDate() {
		return newsModificationDate;
	}
	public void setNewsModificationDate(Date newsModificationDate) {
		this.newsModificationDate = newsModificationDate;
	}
	public Long getNewsCount() {
		return newsCount;
	}
	public void setNewsCount(Long newsCount) {
		this.newsCount = newsCount;
	}
	public Long getNewsCommentAmmount() {
		return newsCommentAmmount;
	}
	public void setNewsCommentAmmount(Long newsCommentAmmount) {
		this.newsCommentAmmount = newsCommentAmmount;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((newsCommentAmmount == null) ? 0 : newsCommentAmmount
						.hashCode());
		result = prime * result
				+ ((newsCount == null) ? 0 : newsCount.hashCode());
		result = prime
				* result
				+ ((newsCreationDate == null) ? 0 : newsCreationDate.hashCode());
		result = prime * result
				+ ((newsFullText == null) ? 0 : newsFullText.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime
				* result
				+ ((newsModificationDate == null) ? 0 : newsModificationDate
						.hashCode());
		result = prime * result
				+ ((newsShortText == null) ? 0 : newsShortText.hashCode());
		result = prime * result
				+ ((newsTitle == null) ? 0 : newsTitle.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (newsCommentAmmount == null) {
			if (other.newsCommentAmmount != null)
				return false;
		} else if (!newsCommentAmmount.equals(other.newsCommentAmmount))
			return false;
		if (newsCount == null) {
			if (other.newsCount != null)
				return false;
		} else if (!newsCount.equals(other.newsCount))
			return false;
		if (newsCreationDate == null) {
			if (other.newsCreationDate != null)
				return false;
		} else if (!newsCreationDate.equals(other.newsCreationDate))
			return false;
		if (newsFullText == null) {
			if (other.newsFullText != null)
				return false;
		} else if (!newsFullText.equals(other.newsFullText))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (newsModificationDate == null) {
			if (other.newsModificationDate != null)
				return false;
		} else if (!newsModificationDate.equals(other.newsModificationDate))
			return false;
		if (newsShortText == null) {
			if (other.newsShortText != null)
				return false;
		} else if (!newsShortText.equals(other.newsShortText))
			return false;
		if (newsTitle == null) {
			if (other.newsTitle != null)
				return false;
		} else if (!newsTitle.equals(other.newsTitle))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", newsTitle=" + newsTitle
				+ ", newsShortText=" + newsShortText + ", newsFullText="
				+ newsFullText + ", newsCreationDate=" + newsCreationDate
				+ ", newsModificationDate=" + newsModificationDate
				+ ", newsCount=" + newsCount + ", newsCommentAmmount="
				+ newsCommentAmmount + "]";
	}
	
	
	
	

}
