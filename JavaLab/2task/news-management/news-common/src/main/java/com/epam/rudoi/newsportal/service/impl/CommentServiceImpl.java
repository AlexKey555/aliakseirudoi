package com.epam.rudoi.newsportal.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.ICommentService;

public class CommentServiceImpl implements ICommentService {

    private static final Logger LOGGER = Logger.getLogger(CommentServiceImpl.class);

    private ICommentDAO commentDAO;

    public void setCommentDAO(ICommentDAO commentDAO) {
	this.commentDAO = commentDAO;
    }

    public Long addComment(Comment commentItem) throws ServiceExeption {
	Long commentId = null;
	try {
	    commentDAO.addItem(commentItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Adding comment exeption", e);
	    throw new ServiceExeption(e);

	}
	commentId = commentItem.getCommentId();
	return commentId;
    }

    public void editComment(Comment commentItem) throws ServiceExeption {
	try {
	    commentDAO.editItem(commentItem);
	} catch (DAOExeption e) {
	    LOGGER.error("Editing comment exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void deleteComment(Long commentId) throws ServiceExeption {
	try {
	    commentDAO.deleteItem(commentId);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting comment exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceExeption {
	try {
	    commentDAO.deleteNewsComments(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Deleting all news comments exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    public void unlink(Long newsId) throws ServiceExeption {
	try {
	    commentDAO.unlink(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking tag exeption", e);
	    throw new ServiceExeption(e);
	}
    }

    @Override
    public void unlink(List<Long> newsIdList) throws ServiceExeption {

	try {
	    commentDAO.unlink(newsIdList);
	} catch (DAOExeption e) {
	    LOGGER.error("Unlinking tags with news exeption", e);
	    throw new ServiceExeption(e);
	}

    }

    public List<Comment> getCommentList(Long newsId) throws ServiceExeption {
	List<Comment> commentList = null;

	try {
	    commentList = commentDAO.getCommentList(newsId);
	} catch (DAOExeption e) {
	    LOGGER.error("getting comment list exeption", e);
	    throw new ServiceExeption(e);
	}
	return commentList;
    }
}
