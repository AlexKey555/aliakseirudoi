package com.epam.rudoi.newsportal.exeption;

public class DAOExeption extends Exception {

	private static final long serialVersionUID = 5161313562361738395L;

	public DAOExeption() {
		super();
		
	}

	public DAOExeption(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public DAOExeption(String message, Throwable cause) {
		super(message, cause);
		
	}

	public DAOExeption(String message) {
		super(message);
		
	}

	public DAOExeption(Throwable cause) {
		super(cause);
		
	}

	

}
