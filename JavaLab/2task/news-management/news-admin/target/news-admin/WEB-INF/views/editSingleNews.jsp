<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


	<div id="mainBody">




<div id="showSingleNewsDiv">
		<div id="newsTitleDiv"><c:out value="${newsVO.news.newsTitle}"/></div>
		<div id="newsAuthorDiv">(by <c:out value="${newsVO.author.authorName}"/>)</div>
		<div id="newsCreationDateDiv"><c:out value="${newsVO.news.newsModificationDate}"/>
		
		</div>
		<br> <br>
		<div id="newsFullTextDiv"><%-- ${newsVO.news.newsFullText} --%>
		<c:out value="${newsVO.news.newsFullText}"></c:out>
		</div>
		<br> <br>

		<div id="newsCommentsDivEditSinglNews">
			<c:forEach var="commentL" items="${commentsList}">

				<div id="newsCommentCreationDateDiv"><c:out value="${commentL.creationDate}"/>
				</div>
				<div id="newsCommentsTextDiv">
				
				<c:out value="${commentL.commentText}"></c:out>
				
				<div style="float: right;" >
				
<form:form method="post" modelAttribute="comment" action="deleteCommentAdmin">
<form:hidden path="commentId" value="${commentL.commentId}" />
<form:hidden path="newsId" value="${newsVO.news.newsId}" />
<form:button path="" id="bottomDeleteComment"> X </form:button>
</form:form>
				</div>
				</div>
			</c:forEach>
		</div>

		<div id="commentForm">

			<form:form method="post" modelAttribute="comment" action="addCommentAdmin">

				<form:textarea  path="commentText" cols="54" rows="4" placeholder="max lengh 100 symbols" maxlength="100"></form:textarea>

				<form:hidden path="newsId" value="${newsVO.news.newsId}" />
				<br>
				<form:button path="" id="bottomPostComment"> <spring:message code="local.button.postComments" /> </form:button>

			</form:form>
		
		</div>

	</div>

<div id="notValidDivMessage">
		${messageCommentNotValid}
		</div>

	<div id="previousNextButtons">
		<div id="buttomPrevDiv">
		<c:if test="${newsVO.news.newsId != resultPreviousNewsId}">
			<a href="previousNews?newsId=${newsVO.news.newsId}"><spring:message code="local.button.previous" /></a>
			</c:if>
		</div>

		<div id="buttomNextDiv">
		<c:if test="${newsVO.news.newsId != resultNextNewsId}">
			<a href="nextNews?newsId=${newsVO.news.newsId}"><spring:message code="local.button.next" /></a>
			</c:if>
		</div>
	</div>

	</div>
