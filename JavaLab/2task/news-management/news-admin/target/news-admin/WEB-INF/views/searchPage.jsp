<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<div id="mainBody">

<jsp:include page="/WEB-INF/views/input/search.jsp" />

		<c:forEach var="parseNews" items="${newsVO}">
			<div id="newsTitleDiv"><c:out value="${parseNews.news.newsTitle}"/></div>
			<div id="newsAuthorDiv">(by <c:out value="${parseNews.author.authorName}"/> )</div>
			<div id="newsCreationDateDiv"> <c:out value="${parseNews.news.newsCreationDate}"/></div>
			<br>
			<br>
			<div id="newsShortTextDiv"><c:out value="${parseNews.news.newsShortText}"/></div>
			<br>
			<div id="linkReadMoreDiv">
				<a href="editSingleNews?&newsId=${parseNews.news.newsId}"><spring:message
					code="local.button.editAdmin" /></a>
			</div>
			<div id="newsCommentsDiv"><c:out value="${parseNews.news.newsCommentAmmount}"/></div>
			<div id="newsTagsDiv">
				<c:forEach var="parseTags" items="${parseNews.tagsList}">
			<c:out value="${parseTags.tagName}"/>
			</c:forEach>
			</div>
			<br>
			<br>
		</c:forEach>
		<jsp:include page="/WEB-INF/views/input/pagination.jsp" />
	</div>