<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
 <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css" />">

 
<script>
	var expanded = false;
	function showCheckboxes() {
		var checkboxes = document.getElementById("checkboxes");
		if (!expanded) {
			checkboxes.style.display = "block";
			expanded = true;
		} else {
			checkboxes.style.display = "none";
			expanded = false;
		}
	}
</script>
</head>
<body>
<div id="mainDiv">

	<div class="headerAndFooterDiv">
		<tiles:insertAttribute name="header" />
	</div>
	
	<div id="bodyMainDiv">
	
	<table id="mTable">
	<tr>
	<td id="td">
	<div id="menuDiv">
		<tiles:insertAttribute name="menu" />
		</div>
	</td>
	<td>
	<div id="bodyDiv">
		<tiles:insertAttribute name="body" />
		</div>
	</td>
	</tr>
	</table>
	
	</div>
	<div class="headerAndFooterDiv">
		<tiles:insertAttribute name="footer" />
	</div>


</div>
</body>
</html>