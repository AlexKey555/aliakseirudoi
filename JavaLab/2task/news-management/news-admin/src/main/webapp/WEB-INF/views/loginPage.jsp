<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



	<div id="mainLoginDiv">
		
			<form class="form-signin" method="post"
				action="j_spring_security_check">
				<h2 class="form-signin-heading">
					<spring:message code="local.message.login"
						text="default text" />
				</h2>
				<div class="loginFormDiv">
				<div class="laberFormDiv">
				 <label for="inputLogin" >
				 <spring:message code="local.message.login" text="default text" /></label>
				 </div>
				 <div class="inputFormDiv">
					<input id="inputLogin" pattern="[\w\W]{3,30}$" title="3 - 30 symbols" maxlength="30"  class="form-control" placeholder="Login"
					type="text" name='j_username' />
					</div> 
				</div>
				<div class="loginFormDiv">
				<div class="laberFormDiv">
					<label for="inputPassword" >
					<spring:message code="local.message.password" text="default text" /></label>
				</div>
				<div class="inputFormDiv">
					<input pattern="[\w\W]{6,35}$" title="6 - 35 symbols" maxlength="35" placeholder="Password" required="required" class="form-control" type="password"
					name='j_password' />
					</div>
				</div>
				 <div id="buttonLogin">
					<input  type="submit"
					value="<spring:message code="local.button.login"
						text="default text" />" />
						</div>
			</form>
			
			<div id="notValidDivMessage">
		${failMessage}
		</div>
	

	</div>
