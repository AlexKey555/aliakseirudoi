package com.epam.rudoi.news_management.controller;


import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static com.epam.rudoi.news_management.constants.Expression.*;

import com.epam.rudoi.news_management.controller.util.ConfigurationManager;
import com.epam.rudoi.news_management.controller.util.ValidityChecker;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.impl.ServiceManagerImpl;

@Controller
public class EditSingleNewsController {

    @Autowired
    private ServiceManagerImpl serviceManager;

    @RequestMapping({ "/addCommentAdmin" })
    public ModelAndView addCommentPage(@ModelAttribute("comment") Comment comment) throws ServiceExeption {

	if (ValidityChecker.checkValidity(comment.getCommentText(), EXPRESSION_FOR_SHORTTEXT_COMMENTS)) {
	    serviceManager.addComment(comment);
	} else {
	    ModelAndView mv = new ModelAndView("redirect:/editSingleNews");
	    mv.addObject("messageCommentNotValid", ConfigurationManager.getProperty("message.fail.maxLengthComment"));
	    return mv;
	}

	ModelAndView mv = new ModelAndView("redirect:/editSingleNews?newsId=" + comment.getNewsId());

	return mv;
    }

    @RequestMapping({ "/nextNews" })
    public ModelAndView showNextNews(HttpSession session, @RequestParam("newsId") Long newsId) throws ServiceExeption {

	List<NewsManagementVO> newsVOList = getNewsVOList(session);

	ModelAndView mv = new ModelAndView("editSingleNews", "comment", new Comment());

	Long nextNewsId = singleNewsNext(newsId, newsVOList);

	mv.addObject("resultNextNewsId", newsVOList.get(newsVOList.size() - 1).getNews().getNewsId());
	mv.addObject("newsVO", serviceManager.showSinglNews(nextNewsId));
	mv.addObject("commentsList", serviceManager.getCommentsList(nextNewsId));
	mv.addObject("queryString", "&newsId=" + newsId);

	return mv;

    }

    @RequestMapping({ "/previousNews" })
    public ModelAndView showPreviousNews(HttpSession session, @RequestParam("newsId") Long newsId)
	    throws ServiceExeption {

	List<NewsManagementVO> newsVOList = getNewsVOList(session);

	ModelAndView mv = new ModelAndView("editSingleNews", "comment", new Comment());

	Long previousNewsId = singleNewsPrevious(newsId, newsVOList);

	mv.addObject("resultPreviousNewsId", newsVOList.get(0).getNews().getNewsId());
	mv.addObject("newsVO", serviceManager.showSinglNews(previousNewsId));
	mv.addObject("commentsList", serviceManager.getCommentsList(previousNewsId));
	mv.addObject("queryString", "&newsId=" + newsId);

	return mv;
    }

    @RequestMapping({ "/deleteCommentAdmin" })
    public ModelAndView deleteComment(@ModelAttribute("comment") Comment comment) throws ServiceExeption {

	serviceManager.deleteComments(comment.getCommentId());

	ModelAndView mv = new ModelAndView("redirect:/editSingleNews?newsId=" + comment.getNewsId());

	return mv;
    }

    
    private List<NewsManagementVO> getNewsVOList(HttpSession session) throws ServiceExeption {
   	SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
   	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);
   	return newsVOList;
       }
    
    private Long singleNewsNext(Long newsId, List<NewsManagementVO> newsVOList) {
	ListIterator<NewsManagementVO> itr = newsVOList.listIterator();
	while (itr.hasNext()) {
	    if (itr.next().getNews().getNewsId().equals(newsId) & itr.hasNext()) {
		Long newsIdNext = itr.next().getNews().getNewsId();
		return newsIdNext;
	    }
	}
	return newsId;
    }

    private Long singleNewsPrevious(Long newsId, List<NewsManagementVO> newsVOList) {
	int i = 0;
	Long newsIdPrevious = null;
	n: for (NewsManagementVO newsManagementVO : newsVOList) {
	    if (newsManagementVO.getNews().getNewsId().equals(newsId)) {
		i = newsVOList.indexOf(newsManagementVO);
		break n;
	    }
	}
	if (i != 0) {
	    newsIdPrevious = newsVOList.get(i - 1).getNews().getNewsId();
	    return newsIdPrevious;
	}
	return newsId;
    }
}
