package com.epam.rudoi.news_management.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.rudoi.news_management.controller.util.ConfigurationManager;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.impl.ServiceManagerImpl;

@Controller
public class AdminHomeController {

    @Autowired
    private ServiceManagerImpl serviceManager;

    @RequestMapping({ "/" })
    public ModelAndView loginPage() throws ServiceExeption {
	return new ModelAndView("loginPage");
    }

    @RequestMapping({ "/logout" })
    public ModelAndView logoutPage() throws ServiceExeption {
	return new ModelAndView("loginPage");
    }

    @RequestMapping({ "/fail2login" })
    public ModelAndView failLoginPage() throws ServiceExeption {

	ModelAndView mv = new ModelAndView("loginPage");

	mv.addObject("failMessage", ConfigurationManager.getProperty("message.fail.loginOrPassword"));

	return mv;
    }
    
    @RequestMapping({ "/reset" })
    public ModelAndView resetSearch() throws ServiceExeption {

	ModelAndView mv = new ModelAndView("redirect:/adminHome");
	
	return mv;
    }
    
    @RequestMapping({ "/adminHome" })
    public ModelAndView showAdminHomePage(HttpSession session) throws ServiceExeption {

	ModelAndView mv = new ModelAndView("adminHome", "searchCriteria", new SearchCriteria());
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());

	SearchCriteria searchCriteria = fillSearchCriteria();
	session.setAttribute("searchCriteria", searchCriteria);
	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);
	
	mv.addObject("paginNum", getPuginNumber(newsVOList));
	mv.addObject("firtPage", 1);
	mv.addObject("lastPage", getPuginNumber(newsVOList));
	
	newsVOList = newsVOList.subList(0, 10);
	mv.addObject("newsVO", newsVOList);
	
	mvForSearch(mv);

	return mv;
    }

    @RequestMapping({ "/pagination" })
    public ModelAndView paginationPage(HttpSession session, @RequestParam("indexPage") Long indexPage) throws ServiceExeption {

	ModelAndView mv = new ModelAndView("adminHome", "searchCriteria", new SearchCriteria());
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());
	
	SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
	if(searchCriteria.getAuthor() == null) {
	      searchCriteria = fillSearchCriteria();
	}
		
	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);

	Integer minIndex = (int) (indexPage * 10 - 10);
	Integer maxIndex = countMaxIndex(indexPage, newsVOList.size());

	mv.addObject("pageId", indexPage);
	mv.addObject("paginNum", getPuginNumber(newsVOList));
	mv.addObject("firtPage", 1);
	mv.addObject("lastPage", getPuginNumber(newsVOList));
	mv.addObject("queryString", "&indexPage=" + indexPage);
	newsVOList = newsVOList.subList(minIndex, maxIndex);
	mv.addObject("newsVO", newsVOList);
	
	mvForSearch(mv);

	return mv;
    }

    
    @RequestMapping({ "/searchForm" })
    public ModelAndView searchCriteriaCheaker(HttpSession session, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria)
	    throws ServiceExeption {
		
	if(searchCriteria.getAuthor() != null) {
	    session.setAttribute("searchCriteria", searchCriteria);
	}
	
	return searchNews(session);
    }
    
    public ModelAndView searchNews(HttpSession session)
	    throws ServiceExeption {

	SearchCriteria searchCriteria = (SearchCriteria)session.getAttribute("searchCriteria");
	
	List<NewsManagementVO> newsVOList = new ArrayList<NewsManagementVO>();
	newsVOList = serviceManager.searchNews(searchCriteria);
	
	ModelAndView mv = new ModelAndView("searchPage", "searchCriteria", new SearchCriteria() );
		
	mv.addObject("paginNum", getPuginNumber(newsVOList));
	mv.addObject("searchCriteriaResult", searchCriteria);
	mv.addObject("firtPage", 1);
	mv.addObject("lastPage", getPuginNumber(newsVOList));
	
	if(newsVOList.size() < 10) {
	    newsVOList = newsVOList.subList(0, newsVOList.size());
	} else {
	    newsVOList = newsVOList.subList(0, 10);
	}
	
	mv.addObject("newsVO", newsVOList);
	mvForSearch(mv);

	return mv;
    }
    
    private void mvForSearch(ModelAndView mv) throws ServiceExeption {
	mv.addObject("authorsList", serviceManager.getAuthorsList());
	mv.addObject("tagsList", serviceManager.getTagsList());
	mv.addObject("tagsIdList", serviceManager.getTagIdList());
    }

    private Long getPuginNumber(List<NewsManagementVO> newsVO) throws ServiceExeption {
	Long newsCount = (long)newsVO.size() - 1;
	Long pnum = (newsCount - 1) / 10 + 1;
	return pnum;
    }

    private SearchCriteria fillSearchCriteria() {
	Author author = new Author();
	SearchCriteria searchCriteria = new SearchCriteria();
	searchCriteria.setAuthor(author);
	return searchCriteria;
    }

    private Integer countMaxIndex(Long indexPage, Integer listSize) {
	Integer maxIndex = (int) (indexPage * 10);
	if (listSize < maxIndex) {
	    maxIndex = listSize;
	}
	return maxIndex;
    }
}
