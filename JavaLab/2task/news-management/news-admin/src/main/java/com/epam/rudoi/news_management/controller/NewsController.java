package com.epam.rudoi.news_management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.rudoi.news_management.controller.util.ConfigurationManager;
import com.epam.rudoi.news_management.controller.util.ValidityChecker;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.impl.ServiceManagerImpl;
import static com.epam.rudoi.news_management.constants.Expression.*;

@Controller
public class NewsController {

    @Autowired
    private ServiceManagerImpl serviceManager;

    @RequestMapping({ "addNewsAction" })
    public ModelAndView addNewsAction(@ModelAttribute("newsVO") NewsManagementVO newsVO) throws ServiceExeption {

	if (newsVO.getAuthor() == null | newsVO.getTagsIdList() == null) {
	    ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
	    mv.addObject("messageMissAuthorOrTags", ConfigurationManager.getProperty("message.fail.newsAdd"));
	    mvForSearch(mv);
	    return mv;
	} else {

	   
	      if (ValidityChecker.checkValidity(newsVO.getNews().getNewsTitle(), EXPRESSION_FOR_TAGS_AUTHORS_TITLE)) {

		if (ValidityChecker.checkValidity(newsVO.getNews().getNewsShortText(),
			EXPRESSION_FOR_SHORTTEXT_COMMENTS)) {

		    if (ValidityChecker.checkValidity(newsVO.getNews().getNewsFullText(), EXPRESSION_FOR_FULLTEXT)) {

		    } else {
			ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
			mv.addObject("messageFullTextNotValid", "length size must be 3 - 2000 symbols");
			mvForSearch(mv);
			return mv;
		    }

		} else {
		    ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
		    mv.addObject("messageShortTextNotValid", "length size must be 3 - 100 symbols");
		    mvForSearch(mv);
		    return mv;
		}

	    } else {
		ModelAndView mv = new ModelAndView("addNewsPage", "newsVO", new NewsManagementVO());
		mv.addObject("messageTitleNotValid", "length size must be 3 - 30 symbols");
		mvForSearch(mv);
		return mv;
	    }

	    serviceManager.addNews(newsVO);
	}

	SearchCriteria searchCriteria = fillSearchCriteria();
	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);

	ModelAndView mv = new ModelAndView("redirect:/adminHome", "searchCriteria", new SearchCriteria());
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());

	mv.addObject("paginNum", getPuginNumber(newsVOList));
	mv.addObject("firtPage", 1);
	mv.addObject("lastPage", getPuginNumber(newsVOList));
	
	newsVOList = newsVOList.subList(0, 3);
	mv.addObject("newsVO", newsVOList);
	mvForSearch(mv);

	return mv;
    }
    
    @RequestMapping({ "/editSingleNews" })
    public ModelAndView showSingleNewsPage(@RequestParam("newsId") Long newsId) throws ServiceExeption {

	ModelAndView mv = new ModelAndView("editSingleNews", "comment", new Comment());

	mv.addObject("newsVO", serviceManager.showSinglNews(newsId));
	mv.addObject("commentsList", serviceManager.getCommentsList(newsId));
	mv.addObject("paginNum", serviceManager.countAllNews());
	mv.addObject("queryString", "&newsId=" + newsId);
	mvForSearch(mv);

	return mv;
    }
    
    @RequestMapping({ "/deleteNews" })
    public ModelAndView deleteNews(@ModelAttribute("newsVO1") NewsManagementVO newsVO) throws ServiceExeption {

	ModelAndView mv = new ModelAndView("adminHome", "searchCriteria", new SearchCriteria());
	mv.getModelMap().addAttribute("newsVO1", new NewsManagementVO());
	
	serviceManager.deleteNews(newsVO);

	SearchCriteria searchCriteria = fillSearchCriteria();
	
	List<NewsManagementVO> newsVOList = serviceManager.searchNews(searchCriteria);
	newsVOList = newsVOList.subList(0, 3);

	mv.addObject("paginNum", getPuginNumber(newsVOList));
	mv.addObject("firtPage", 1);
	mv.addObject("lastPage", getPuginNumber(newsVOList));
	
	newsVOList = newsVOList.subList(0, 3);
	mv.addObject("newsVO", newsVOList);
	mvForSearch(mv);

	return mv;
    }

    private SearchCriteria fillSearchCriteria() {
	Author author = new Author();
	SearchCriteria searchCriteria = new SearchCriteria();
	searchCriteria.setAuthor(author);
	return searchCriteria;
    }

    private void mvForSearch(ModelAndView mv) throws ServiceExeption {
	mv.addObject("authorsList", serviceManager.getAuthorsList());
	mv.addObject("tagsList", serviceManager.getTagsList());
	mv.addObject("tagsIdList", serviceManager.getTagIdList());
    }

    private Long getPuginNumber(List<NewsManagementVO> newsVO) throws ServiceExeption {
   	Long newsCount = (long)newsVO.size() - 1;
   	Long pnum = (newsCount - 1) / 3 + 1;
   	return pnum;
       }

}
