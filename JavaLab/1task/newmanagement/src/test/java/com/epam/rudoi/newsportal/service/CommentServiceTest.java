package com.epam.rudoi.newsportal.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyLong;
import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.impl.CommentServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config_spring.xml")
public class CommentServiceTest {

	@Mock
	private ICommentDAO commentDAOMock;

	@InjectMocks
	@Autowired
	private CommentServiceImpl commentService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(commentService);
		assertNotNull(commentDAOMock);
	}

	@Test
	public void addCommentTest() throws DAOExeption, ServiceExeption {
		Long expectedId = 1L;
		Long resultId = null;
		Comment commentItem = new Comment();
		commentItem.setCommentId(expectedId);
		when(commentDAOMock.addItem(commentItem)).thenReturn(resultId);
		resultId = commentService.addComment(commentItem);
		verify(commentDAOMock, times(1)).addItem(commentItem);
		assertEquals(expectedId, resultId);
	}

	@Test
	public void editCommentTest() throws DAOExeption, ServiceExeption {
		Long expectedId = 1L;
		Comment commentItem = new Comment();
		commentItem.setCommentId(expectedId);
		commentService.editComment(commentItem);
		verify(commentDAOMock, times(1)).editItem(commentItem);
	}

	@Test
	public void deleteCommentTest() throws DAOExeption, ServiceExeption {
		commentService.deleteComment(anyLong());
		verify(commentDAOMock, times(1)).deleteItem(anyLong());
	}

}
