package com.epam.rudoi.newsportal.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.service.impl.AuthorServiceImpl;
import com.epam.rudoi.newsportal.service.impl.CommentServiceImpl;
import com.epam.rudoi.newsportal.service.impl.NewsServiceImpl;
import com.epam.rudoi.newsportal.service.impl.TagServiceImpl;

import static org.mockito.Matchers.anyLong;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config_spring.xml")

public class ServiceManagerTest {

	@Mock
	private IAuthorDAO authorDaoMock;
	@Mock
	private ITagDAO tagDaoMock;
	@Mock
	private ICommentDAO commentDaoMock;
	@Mock
	private INewsDAO newsDaoMock;

	@InjectMocks
	@Autowired
	private AuthorServiceImpl authorService;
	@InjectMocks
	@Autowired
	private NewsServiceImpl newsService;
	@InjectMocks
	@Autowired
	private TagServiceImpl tagService;;
	@InjectMocks
	@Autowired
	private CommentServiceImpl commentService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(authorService);
		assertNotNull(authorDaoMock);
		assertNotNull(newsService);
		assertNotNull(newsDaoMock);
		assertNotNull(tagService);
		assertNotNull(tagDaoMock);
		assertNotNull(commentService);
		assertNotNull(commentDaoMock);
		
	}
	
	
	@Test
	public void addNewsTest() throws DAOExeption, ServiceExeption {
		Long expectedNewsId = 1L;
		Long expectedAuthorId = 1L;
		Long expectedTagId = 1L;
		Long resultId = null;		
			
		Author author = new Author();
		Tag tag = new Tag();
		News news = new News();
		
		author.setAuthorId(expectedAuthorId);
		tag.setTagId(expectedTagId);
		news.setNewsId(expectedNewsId);
				
		when(newsDaoMock.addItem(news)).thenReturn(anyLong());
		resultId = newsService.addNews(news);
		
		verify(newsDaoMock, times(1)).addItem(news);
		
		assertEquals(expectedNewsId, resultId);
		
	}

	@Test
	public void editNewsTest() throws DAOExeption, ServiceExeption {
		Long expectedId = 1L;
		News news = new News();
		news.setNewsId(expectedId);
		newsService.editNews(news);
		verify(newsDaoMock, times(1)).editItem(news);
	
	}

	@Test
	public void deleteNewsTest() throws DAOExeption, ServiceExeption {
		newsService.deleteNews(anyLong());
		verify(newsDaoMock, times(1)).deleteItem(anyLong());

	}

	@Test
	public void searchNewsTest() 	throws DAOExeption, ServiceExeption {
		List<News> expectedNewsList = new ArrayList<News>();
		List<News> resultNewsList = null;
		
		SearchCriteria searchCriteria = new SearchCriteria();
				
		when(newsDaoMock.searchNews(searchCriteria)).thenReturn(expectedNewsList);
		resultNewsList = newsService.SearchNews(searchCriteria);
		verify(newsDaoMock, times(1)).searchNews(searchCriteria);		
		assertNotNull(resultNewsList);
		
	}

	@Test
	public void showSinglNewsTest() throws DAOExeption, ServiceExeption {
		newsService.showSingleNews(anyLong());
		when(newsDaoMock.showSinglNews(anyLong())).thenReturn(new News());
		verify(newsDaoMock, times(1)).showSinglNews(anyLong());
		assertNotNull(newsService.showSingleNews(anyLong()));
	}

	@Test
	public void countAllNewsTest() throws DAOExeption, ServiceExeption {
		
	}
		
	
	@Test
	public void addCommentTest() throws DAOExeption, ServiceExeption {
		Long expectedId = 1L;
		Long resultId = null;
		Comment commentItem = new Comment();
		commentItem.setCommentId(expectedId);
		when(commentDaoMock.addItem(commentItem)).thenReturn(resultId);
		resultId = commentService.addComment(commentItem);
		verify(commentDaoMock, times(1)).addItem(commentItem);
		assertEquals(expectedId, resultId);
	}

	@Test
	public void deleteCommentsTest() throws DAOExeption, ServiceExeption {
		commentService.deleteComment(anyLong());
		verify(commentDaoMock, times(1)).deleteItem(anyLong());
	}
	
	
	@Test
	public void addTagTest() throws DAOExeption, ServiceExeption {
		Long expectedTagId = 1L;
		Long expectedNewsId = 1L;
		Long resultId = null;
		
		NewsManagementVO expectedNewsManagementVO = new NewsManagementVO();
		Tag tagItem = new Tag();
		News newsItem = new News();
		newsItem.setNewsId(expectedNewsId);
		tagItem.setTagId(expectedTagId);
		expectedNewsManagementVO.setTagItem(tagItem);
		expectedNewsManagementVO.setNewsItem(newsItem);
		
		tagItem.setTagId(expectedTagId);
		when(tagDaoMock.addItem(tagItem)).thenReturn(1L);
		resultId = tagService.addTag(expectedNewsManagementVO);
		verify(tagDaoMock, times(1)).addItem(expectedNewsManagementVO.getTagItem());
		verify(tagDaoMock, times(1)).link(anyLong(), anyLong());
		assertEquals(expectedTagId, resultId);
	}

	@Test
	public void deleteTagTest() throws DAOExeption, ServiceExeption {
		tagService.deleteTag(anyLong());		
		verify(tagDaoMock, times(1)).deleteItem(anyLong());
	}
	
	
}
