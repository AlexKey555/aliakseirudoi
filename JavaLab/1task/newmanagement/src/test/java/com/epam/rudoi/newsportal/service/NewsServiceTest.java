package com.epam.rudoi.newsportal.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyLong;
import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.impl.NewsServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config_spring.xml")
public class NewsServiceTest {

	@Mock
	private INewsDAO newsDAOMock;

	@InjectMocks
	@Autowired
	private NewsServiceImpl newsService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(newsService);
		assertNotNull(newsDAOMock);
	}

	@Test
	public void addNewsTest() throws DAOExeption, ServiceExeption {
		Long expectedId = 1L;
		Long resultId = null;
		News newsItem = new News();
		newsItem.setNewsId(expectedId);
		when(newsDAOMock.addItem(newsItem)).thenReturn(anyLong());
		resultId = newsService.addNews(newsItem);
		verify(newsDAOMock, times(1)).addItem(newsItem);
		assertEquals(expectedId, resultId);
	}
	
	@Test
	public void editNewsTest() throws DAOExeption, ServiceExeption {
	Long expectedId = 1L;
	News newsItem = new News();
	newsItem.setNewsId(expectedId);
	newsService.editNews(newsItem);
	verify(newsDAOMock, times(1)).editItem(newsItem);
	}
	
	@Test
	public void deleteNewsTest() throws DAOExeption, ServiceExeption {
	newsService.deleteNews(anyLong());
	verify(newsDAOMock, times(1)).deleteItem(anyLong());
	}
	
	@Test
	public void showSingNewsTest() throws DAOExeption, ServiceExeption { 
		newsService.showSingleNews(anyLong());
		when(newsDAOMock.showSinglNews(anyLong())).thenReturn(new News());
		verify(newsDAOMock, times(1)).showSinglNews(anyLong());
		assertNotNull(newsService.showSingleNews(anyLong()));
	
	}
	
}
