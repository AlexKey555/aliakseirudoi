package com.epam.rudoi.newsportal.service;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.impl.AuthorServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config_spring.xml")
public class AuthorServiceTest {

	@Mock
	private IAuthorDAO authorDaoMock;

	@InjectMocks
	@Autowired
	private AuthorServiceImpl authorService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(authorService);
		assertNotNull(authorDaoMock);
	}

	@Test
	public void addAuthorTest() throws DAOExeption, ServiceExeption {
		Long expectedAuthorId = 1L;
		Long expectedNewsId = 1L;
		Long resultId = null;
				
		NewsManagementVO expectedNewsManagementVO = new NewsManagementVO();
		Author authorItem = new Author();
		News newsItem = new News();
		newsItem.setNewsId(expectedNewsId);
		authorItem.setAuthorId(expectedAuthorId);
		expectedNewsManagementVO.setAuthorItem(authorItem);
		expectedNewsManagementVO.setNewsItem(newsItem);
				
		when(authorDaoMock.addItem(authorItem)).thenReturn(Long.valueOf(1L));
		resultId = authorService.addAuthor(expectedNewsManagementVO);
		verify(authorDaoMock, times(1)).addItem(authorItem);
		verify(authorDaoMock, times(1)).link(anyLong(), anyLong());
		
		assertEquals(expectedAuthorId, resultId);
		
	
		
	}

	@Test
	public void editAuthorTest() throws DAOExeption, ServiceExeption {
		Long expectedId = 1L;
		Author authorItem = new Author();
		authorItem.setAuthorId(expectedId);
		authorService.editAuthor(authorItem);
		verify(authorDaoMock, times(1)).editItem(authorItem);
	}

	@Test
	public void linkAuthorTest() throws DAOExeption, ServiceExeption {
		authorService.link(anyLong(), anyLong());
		verify(authorDaoMock, times(1)).link(anyLong(), anyLong());
	}

	@Test
	public void unlinkAuthorTest() throws DAOExeption, ServiceExeption {
		authorService.unlink(anyLong());
		verify(authorDaoMock, times(1)).unlink(anyLong());
	}

}
