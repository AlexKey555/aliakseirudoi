package com.epam.rudoi.newsportal.service;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

public interface IAuthorService {

	Long addAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption;
	
	void editAuthor(Author authorItem) throws ServiceExeption;
	
	void link (Long authorId, Long newsId) throws ServiceExeption;
	
	void unlink (Long newsId) throws ServiceExeption;
	
	void expiredAuthor (Long authorId) throws ServiceExeption;
}
