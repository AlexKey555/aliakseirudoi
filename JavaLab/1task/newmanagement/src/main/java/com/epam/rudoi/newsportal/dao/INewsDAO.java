package com.epam.rudoi.newsportal.dao;

import java.util.List;

import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

public interface INewsDAO extends ICrudDAO<News> {

	List<News> searchNews (SearchCriteria searchCriteria) throws DAOExeption;
	
	News showSinglNews (Long newsId) throws DAOExeption;
	
	Long countAllNews() throws DAOExeption;
	
}
