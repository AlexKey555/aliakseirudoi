package com.epam.rudoi.newsportal.service;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

public interface ICommentService {
	
	Long addComment(Comment commentItem) throws ServiceExeption;
	
	void editComment(Comment commentItem) throws ServiceExeption;
	
	void deleteComment(Long commentId) throws ServiceExeption;
	
	void unlink(Long newsId) throws ServiceExeption;

}
