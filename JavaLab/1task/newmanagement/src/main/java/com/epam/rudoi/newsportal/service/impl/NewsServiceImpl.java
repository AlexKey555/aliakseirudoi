package com.epam.rudoi.newsportal.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.INewsService;

public class NewsServiceImpl implements INewsService {

	private static final Logger LOGGER = Logger
			.getLogger(NewsServiceImpl.class);
	private INewsDAO newsDAO;

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	/** 
	 * Adding news and link author with news
	 * @throws ServiceExeption if can't add news 
	 */
	public Long addNews(News newsItem) throws ServiceExeption {
		Long newsId = null;
		try {
			newsDAO.addItem(newsItem);
		} catch (DAOExeption e) {
			LOGGER.error("Adding news exeption", e);
			throw new ServiceExeption();
		}
		newsId = newsItem.getNewsId();
		return newsId;
	}

	/** 
	 * Editing news 
	 * @throws ServiceExeption if can't edit news 
	 */
	public void editNews(News newsItem) throws ServiceExeption {
		try {
			newsDAO.editItem(newsItem);
		} catch (DAOExeption e) {
			LOGGER.error("Editing news exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Deleting news 
	 * @throws ServiceExeption if can't delete news 
	 */
	public void deleteNews(Long newsId) throws ServiceExeption {
		try {
			newsDAO.deleteItem(newsId);
		} catch (DAOExeption e) {
			LOGGER.error("Deleting news exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Count all news 
	 * @throws ServiceExeption if can't count news 
	 */
	public Long countAllNews() throws ServiceExeption {
		Long totalNewsCount = null;
		try {
			totalNewsCount = newsDAO.countAllNews();
		} catch (DAOExeption e) {
			LOGGER.error("Counting all news exeption", e);
			throw new ServiceExeption();
		}
		return totalNewsCount;
	}

	/** 
	 * Showing single news 
	 * @throws ServiceExeption if can't show single news 
	 */
	public News showSingleNews(Long newsId) throws ServiceExeption {
		News newsItem = null;
		try {
			newsItem = newsDAO.showSinglNews(newsId);
		} catch (DAOExeption e) {
			LOGGER.error("Showing news exeption", e);
			throw new ServiceExeption();
		}
		return newsItem;
	}


	/** 
	 * Searching news by author and tag
	 * @throws ServiceExeption if can't search news 
	 */
	public List<News> SearchNews(SearchCriteria searchCriteria)
			throws ServiceExeption {
		List<News> newsList = null;
		
		try {
			
			newsList = newsDAO.searchNews(searchCriteria);
		} catch (DAOExeption e) {
			LOGGER.error("Search news exeption", e);
			throw new ServiceExeption();
		}
		
		return newsList;
	}

}
