package com.epam.rudoi.newsportal.entity;

import java.util.List;

public class SearchCriteria {

	private Author authorItem;
	private List<Tag> tagItems;
	private Long minIndexSearch;
	private Long maxIndexSearch;
	public Author getAuthorItem() {
		return authorItem;
	}
	public void setAuthorItem(Author authorItem) {
		this.authorItem = authorItem;
	}
	public List<Tag> getTagItems() {
		return tagItems;
	}
	public void setTagItems(List<Tag> tagItems) {
		this.tagItems = tagItems;
	}
	public Long getMinSearchValue() {
		return minIndexSearch;
	}
	public void setMinSearchValue(Long minSearchValue) {
		this.minIndexSearch = minSearchValue;
	}
	public Long getMaxSearchValue() {
		return maxIndexSearch;
	}
	public void setMaxSearchValue(Long maxSearchValue) {
		this.maxIndexSearch = maxSearchValue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorItem == null) ? 0 : authorItem.hashCode());
		result = prime * result
				+ ((maxIndexSearch == null) ? 0 : maxIndexSearch.hashCode());
		result = prime * result
				+ ((minIndexSearch == null) ? 0 : minIndexSearch.hashCode());
		result = prime * result
				+ ((tagItems == null) ? 0 : tagItems.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (authorItem == null) {
			if (other.authorItem != null)
				return false;
		} else if (!authorItem.equals(other.authorItem))
			return false;
		if (maxIndexSearch == null) {
			if (other.maxIndexSearch != null)
				return false;
		} else if (!maxIndexSearch.equals(other.maxIndexSearch))
			return false;
		if (minIndexSearch == null) {
			if (other.minIndexSearch != null)
				return false;
		} else if (!minIndexSearch.equals(other.minIndexSearch))
			return false;
		if (tagItems == null) {
			if (other.tagItems != null)
				return false;
		} else if (!tagItems.equals(other.tagItems))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SearchCriteria [authorItem=" + authorItem + ", tagItems="
				+ tagItems + ", minSearchValue=" + minIndexSearch
				+ ", maxSearchValue=" + maxIndexSearch + "]";
	}
	
	
}
