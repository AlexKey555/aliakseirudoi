package com.epam.rudoi.newsportal.service;

import java.util.List;

import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

public interface INewsService {

	Long addNews(News newsItem) throws ServiceExeption;

	void editNews(News newsItem) throws ServiceExeption;

	void deleteNews(Long newsId) throws ServiceExeption;

	News showSingleNews(Long newsId) throws ServiceExeption;

	Long countAllNews() throws ServiceExeption;

	List<News> SearchNews(SearchCriteria searchCriteria) throws ServiceExeption;

}
