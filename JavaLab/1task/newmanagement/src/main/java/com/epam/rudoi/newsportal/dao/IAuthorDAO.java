package com.epam.rudoi.newsportal.dao;

import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

public interface IAuthorDAO extends ICrudDAO<Author>{
	
	void link (Long authorId, Long newsId) throws DAOExeption;
	
	void unlink (Long newsId) throws DAOExeption;
	
	

	
}
