package com.epam.rudoi.newsportal.dao;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

public interface ICommentDAO extends ICrudDAO<Comment>{
	
	void unlink(Long newsId) throws DAOExeption;

}
