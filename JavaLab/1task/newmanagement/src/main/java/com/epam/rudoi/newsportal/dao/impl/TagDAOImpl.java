package com.epam.rudoi.newsportal.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;

public class TagDAOImpl implements ITagDAO {

	private static final String ADD_TAG = "INSERT INTO tags (tag_id, tag_name) VALUES (sq_tag_id_seq.nextval,?)";
	private static final String EDIT_TAG = "UPDATE tags SET tag_name=? WHERE tag_id=?";
	private static final String DELETE_TAG = "DELETE FROM tags WHERE tag_id=?";
	private static final String LINK_NEWS_WITH_TAG = "INSERT INTO news_tags (news_id, tag_id) VALUES (?,?)";
	private static final String UNLINK_NEWS_WITH_TAG = "DELETE FROM news_tags WHERE news_id=?";
	private static final String UNLINK_NEWS_WITH_TAG_BY_TAG_ID = "DELETE FROM news_tags WHERE tag_id=?";
	private static final String GET_TAG = "SELECT a.tag_id, a.tag_name FROM tags a WHERE a.tag_id = ?";
	
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/** 
	 * Fill in DB table tags creating tag's line  
	 * @throws DAOExeption if can't create the line 
	 */
	public Long addItem(Tag teg) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Long tagId = null;
		String[] column = {"TAG_ID"};
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(ADD_TAG, column);
			preparedStatement.setString(1, teg.getTagName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if((resultSet != null)&&(resultSet.next())){
				tagId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}

		return tagId;
	}

	/** 
	 * Delete line in DB table tags by tagId if need delete tag severally from news   
	 * @throws DAOExeption if can't delete line 
	 */
	public void deleteItem(Long tagId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_TAG);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}
	

	/** 
	 * Edit DB table tags by lines  
	 * @throws DAOExeption if can't edit tag 
	 */
	public void editItem(Tag tag) throws DAOExeption{
	
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(EDIT_TAG);
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getTagId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	public Tag readItem(Long tagId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Tag tag = new Tag();
		
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(GET_TAG);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag.setTagId(resultSet.getLong(1));
				tag.setTagName(resultSet.getString(2));
			}
		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}
			
		return tag;
	
	}

	/** 
	 * Link DB table tags and DB table news in news table news_tags  
	 * @throws DAOExeption if can't link tags and news or can't create news table news_tags
	 */
	public void link(Long tagId, Long newsId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(LINK_NEWS_WITH_TAG);
			preparedStatement.setLong(1, tagId);
			preparedStatement.setLong(2, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	/** 
	 * Unlink DB table tags with DB table news in table news_tags by newsId when deleting news
	 * @throws DAOExeption if can't unlink tags and news or delete line in news_tags 
	 */
	public void unlink(Long newsId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(UNLINK_NEWS_WITH_TAG);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	/** 
	 * Unlink DB table tags with DB table news in table news_tags by tagId when deleting tag
	 * @throws DAOExeption if can't unlink tags and news or delete line in news_tags 
	 */
	public void unlinkByTagId(Long tagId) throws DAOExeption {

		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(UNLINK_NEWS_WITH_TAG_BY_TAG_ID);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}
		
	}



}
