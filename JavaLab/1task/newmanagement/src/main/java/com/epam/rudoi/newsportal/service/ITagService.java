package com.epam.rudoi.newsportal.service;

import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

public interface ITagService {

	Long addTag(NewsManagementVO managementVO) throws ServiceExeption;

	void editTag(Tag tagItem) throws ServiceExeption;

	void deleteTag(Long tagId) throws ServiceExeption;
	
	void link(Long tagId, Long newsId) throws ServiceExeption;

	void unlink(Long newsId) throws ServiceExeption;
	
	void unlinkByTagId(Long tagId) throws ServiceExeption;

}
