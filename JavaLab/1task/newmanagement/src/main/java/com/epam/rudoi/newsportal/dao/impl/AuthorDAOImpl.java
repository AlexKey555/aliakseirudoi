package com.epam.rudoi.newsportal.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;

public class AuthorDAOImpl implements IAuthorDAO {

	private static final String ADD_NEWS_AUTHOR = "INSERT INTO authors (author_id, author_name) VALUES (sq_author_id_seq.nextval,?)";
	private static final String EDIT_NEWS_AUTHOR = "UPDATE authors SET author_name=?";
	private static final String LINK_NEWS_WITH_AUTHOR = "INSERT INTO news_authors (news_id, author_id) VALUES (?,?)";
	private static final String UNLINK_NEWS_WITH_AUTHOR = "DELETE FROM news_authors WHERE news_id=?";
	private static final String GET_AUTHOR = "SELECT a.author_id, a.author_name, a.expired FROM authors a WHERE a.author_id = ?";
	private static final String EXPIRED_AUTHOR = "UPDATE authors SET expired = current_timestamp WHERE author_id = ?";
	
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Fill in DB table authors creating author's line
	 * 
	 * @throws DAOExeption
	 *             if can't create the line
	 */
	public Long addItem(Author author) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Long authorId = null;
		String[] column = {"AUTHOR_ID"};
		
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(ADD_NEWS_AUTHOR, column);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if((resultSet != null)&&(resultSet.next())){
				authorId = resultSet.getLong(1);
			}
			
		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}

		return authorId;
	}

	/**
	 * Edit DB table authors by lines
	 * 
	 * @throws DAOExeption
	 *             if can't edit author
	 */
	public void editItem(Author author) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(EDIT_NEWS_AUTHOR);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	public Author readItem(Long authorId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Author author = new Author();

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(GET_AUTHOR);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null)&&(resultSet.next())) {
				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
				author.setExpider(resultSet.getTimestamp(3));
			}
		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}

		return author;
	}

	public void deleteItem(Long authorId) throws DAOExeption {
	expiredAuthor(authorId);
	}

	/**
	 * Link DB table authors and DB table news in news table news_authors
	 * 
	 * @throws DAOExeption
	 *             if can't link authors and news or can't create news table
	 *             news_authors
	 */
	public void link(Long authorId, Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(LINK_NEWS_WITH_AUTHOR);
			preparedStatement.setLong(1, authorId);
			preparedStatement.setLong(2, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	/**
	 * Unlink DB table authors with DB table news in table news_authors
	 * 
	 * @throws DAOExeption
	 *             if can't unlink authors and news or delete line in
	 *             news_authors
	 */
	public void unlink(Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(UNLINK_NEWS_WITH_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	private void expiredAuthor(Long authorId) throws DAOExeption {
	
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(EXPIRED_AUTHOR);
			preparedStatement.setLong(1, authorId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}
	}

}
