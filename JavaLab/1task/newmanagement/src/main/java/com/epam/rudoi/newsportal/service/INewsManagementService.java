package com.epam.rudoi.newsportal.service;

import java.util.List;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;

public interface INewsManagementService {
	
	

	void addNews(NewsManagementVO newsManagementVO) throws ServiceExeption;

	void editNews(NewsManagementVO newsManagementVO) throws ServiceExeption;

	void deleteNews(NewsManagementVO newsManagementVO) throws ServiceExeption;

	List<News> searchNews(SearchCriteria searchCriteria)
			throws ServiceExeption;

	News showSinglNews(Long newsId) throws ServiceExeption;

	Long countAllNews() throws ServiceExeption;
	

	
	void addComment(Comment commentItem) throws ServiceExeption;

	void deleteComments(Long commentId) throws ServiceExeption;
	
	

	void addTag(NewsManagementVO newsManagementVO) throws ServiceExeption;

	void deleteTag(Long tagId) throws ServiceExeption;
	

}
