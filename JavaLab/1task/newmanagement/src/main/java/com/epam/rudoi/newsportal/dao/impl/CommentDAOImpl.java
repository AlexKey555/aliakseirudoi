package com.epam.rudoi.newsportal.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;

public class CommentDAOImpl implements ICommentDAO {

	private static final String ADD_COMMENT = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (sq_comment_id_seq.nextval,?,?,current_timestamp)";
	private static final String EDIT_COMMENT = "UPDATE comments SET comment_text=?";
	private static final String DELETE_COMMENT = "DELETE FROM comments WHERE comment_id=?";
	private static final String UNLINK_COMMENT = "DELETE FROM comments WHERE news_id=?";
	private static final String GET_COMMENT = "SELECT a.comment_id, a.news_id, a.comment_text, a.creation_date FROM comments a WHERE a.comment_id = ?";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/** 
	 * Fill in DB table comments creating comment's line  
	 * @throws DAOExeption if can't create the line 
	 */
	public Long addItem(Comment comment) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Long commentId = null;
		String[] column = {"COMMENT_ID"};
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(ADD_COMMENT, column);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if((resultSet != null)&&(resultSet.next())){
				commentId = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}

		return commentId;
	}

	/** 
	 * Delete line in DB table comments   
	 * @throws DAOExeption if can't delete line 
	 */
	public void deleteItem(Long commentId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_COMMENT);
			preparedStatement.setLong(1, commentId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	/** 
	 * Edit DB table comments by lines  
	 * @throws DAOExeption if can't edit comment 
	 */
	public void editItem(Comment comment) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(EDIT_COMMENT);
			preparedStatement.setString(1, comment.getCommentText());
			
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	public Comment readItem(Long commentId) throws DAOExeption{
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Comment comment = new Comment();
		
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(GET_COMMENT);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null)&&(resultSet.next())){
				comment.setCommentId(resultSet.getLong(1));
				comment.setNewsId(resultSet.getLong(2));
				comment.setCommentText(resultSet.getString(3));
				comment.setCreationDate(resultSet.getTimestamp(4));
			}
		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}
			
		return comment;
		
	}

	public void unlink(Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(UNLINK_COMMENT);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

}
