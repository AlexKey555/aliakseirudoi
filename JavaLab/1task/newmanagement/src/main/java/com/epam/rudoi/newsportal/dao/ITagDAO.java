package com.epam.rudoi.newsportal.dao;

import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;

public interface ITagDAO extends ICrudDAO<Tag> {
	
	void link (Long tagId,Long newsId) throws DAOExeption;
	
	void unlink (Long newsId) throws DAOExeption;
	
	void unlinkByTagId (Long tagId) throws DAOExeption;
	
}
