package com.epam.rudoi.newsportal.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.IAuthorService;
import com.epam.rudoi.newsportal.service.ICommentService;
import com.epam.rudoi.newsportal.service.INewsManagementService;
import com.epam.rudoi.newsportal.service.INewsService;
import com.epam.rudoi.newsportal.service.ITagService;

@Transactional(rollbackFor=Exception.class)
public class ServiceManagerImpl implements INewsManagementService {

	private IAuthorService authorService;
	private ICommentService commentService;
	private INewsService newsService;
	private ITagService tagService;

	
	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	
	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	/** 
	 * Adding news complex method comprising: adding news, adding author, adding tag
	 * @throws ServiceExeption if can't perform adding news complex method 
	 */
	public void addNews(NewsManagementVO newsManagementVO)
			throws ServiceExeption {
		newsService.addNews(newsManagementVO.getNewsItem());
		authorService.addAuthor(newsManagementVO);
		tagService.addTag(newsManagementVO);
	}

	/** 
	 * Editing news complex method comprising: editing news, editing author, editing tag
	 * @throws ServiceExeption if can't perform editing news complex method 
	 */
	public void editNews(NewsManagementVO newsManagementVO)
			throws ServiceExeption {
		newsService.editNews(newsManagementVO.getNewsItem());
		authorService.editAuthor(newsManagementVO.getAuthorItem());
		tagService.editTag(newsManagementVO.getTagItem());
	}

	public List<News> searchNews(SearchCriteria searchCriteria)
			throws ServiceExeption {
		List<News> newsList = newsService.SearchNews(searchCriteria);
		return newsList;
	}


	/** 
	 * Showing single news method 
	 * @throws ServiceExeption if can't show single news
	 */
	public News showSinglNews(Long newsId) throws ServiceExeption {
		News newsItem = newsService.showSingleNews(newsId);
		return newsItem;
	}

	/** 
	 * Counting news method 
	 * @throws ServiceExeption if can't count news 
	 */
	public Long countAllNews() throws ServiceExeption {
		Long totalNewsCount = newsService.countAllNews();
		return totalNewsCount;
	}

	/** 
	 * Deleting news complex method comprising: deleting news, deleting tag, unlinking author and news, unlinking tags and news
	 * @throws ServiceExeption if can't perform deleting news complex method 
	 */
	public void deleteNews(NewsManagementVO newsManagementVO)
			throws ServiceExeption {
		authorService.unlink(newsManagementVO.getNewsItem().getNewsId());
		authorService.expiredAuthor(newsManagementVO.getAuthorItem().getAuthorId());
		tagService.unlink(newsManagementVO.getNewsItem().getNewsId());
		commentService.unlink(newsManagementVO.getNewsItem().getNewsId());
		newsService.deleteNews(newsManagementVO.getNewsItem().getNewsId());

	}

	/** 
	 * Adding comment method 
	 * @throws ServiceExeption if can't add comment 
	 */
	public void addComment(Comment commentItem) throws ServiceExeption {
		commentService.addComment(commentItem);

	}

	/** 
	 * Deleting comment method 
	 * @throws ServiceExeption if can't delete comment 
	 */
	public void deleteComments(Long commentId) throws ServiceExeption {
		commentService.deleteComment(commentId);

	}

	/** 
	 * Adding tag method 
	 * @throws ServiceExeption if can't add tag 
	 */
	public void addTag(NewsManagementVO newsManagementVO) throws ServiceExeption {
		tagService.addTag(newsManagementVO);

	}

	/** 
	 * Deleting tag method 
	 * @throws ServiceExeption if can't delete tag 
	 */
	public void deleteTag(Long tagId) throws ServiceExeption {
		tagService.deleteTag(tagId);

	}

}
