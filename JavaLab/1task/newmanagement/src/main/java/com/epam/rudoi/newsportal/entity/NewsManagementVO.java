package com.epam.rudoi.newsportal.entity;

public class NewsManagementVO {

	private Author author;
	private Comment comment;
	private News news;
	private Tag tag;
	private SearchCriteria searchCriteria;
	private User user;
	
	public Author getAuthorItem() {
		return author;
	}
	public void setAuthorItem(Author authorItem) {
		this.author = authorItem;
	}
	public Comment getCommentItem() {
		return comment;
	}
	public void setCommentItem(Comment commentItem) {
		this.comment = commentItem;
	}
	public News getNewsItem() {
		return news;
	}
	public void setNewsItem(News newsItem) {
		this.news = newsItem;
	}
	public Tag getTagItem() {
		return tag;
	}
	public void setTagItem(Tag tagItem) {
		this.tag = tagItem;
	}
	public SearchCriteria getSearchCriteria() {
		return searchCriteria;
	}
	public void setSearchCriteria(SearchCriteria searchCriteria) {
		this.searchCriteria = searchCriteria;
	}
	public User getUserItem() {
		return user;
	}
	public void setUserItem(User userItem) {
		this.user = userItem;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comment == null) ? 0 : comment.hashCode());
		result = prime * result
				+ ((news == null) ? 0 : news.hashCode());
		result = prime * result
				+ ((searchCriteria == null) ? 0 : searchCriteria.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result
				+ ((user == null) ? 0 : user.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsManagementVO other = (NewsManagementVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (searchCriteria == null) {
			if (other.searchCriteria != null)
				return false;
		} else if (!searchCriteria.equals(other.searchCriteria))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EntityVO [authorItem=" + author + ", commentItem="
				+ comment + ", newsItem=" + news + ", tagItem="
				+ tag + ", searchCriteria=" + searchCriteria
				+ ", userItem=" + user + "]";
	}
	
	
	
}
