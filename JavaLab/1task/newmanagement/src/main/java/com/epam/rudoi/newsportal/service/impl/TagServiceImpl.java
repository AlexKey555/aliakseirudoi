package com.epam.rudoi.newsportal.service.impl;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.rudoi.newsportal.dao.ITagDAO;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.ITagService;

@Transactional(rollbackFor=Exception.class)
public class TagServiceImpl implements ITagService {

	private static final Logger LOGGER = Logger.getLogger(TagServiceImpl.class);
	private ITagDAO tagDAO;

	public void setTagDAO(ITagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	/** 
	 * Adding tag and link tag with news
	 * @throws ServiceExeption if can't add tag 
	 */
	public Long addTag(NewsManagementVO newsManagementVO) throws ServiceExeption {
		Long tagId;
		try {
			tagId =	tagDAO.addItem(newsManagementVO.getTagItem());
			tagDAO.link(newsManagementVO.getTagItem().getTagId(), newsManagementVO.getNewsItem().getNewsId());
		} catch (DAOExeption e) {
			LOGGER.error("Linking tag with news exeption", e);
			throw new ServiceExeption();
		}

		return tagId;
	}

	/** 
	 * Editing tag
	 * @throws ServiceExeption if can't edit tag 
	 */
	public void editTag(Tag tagItem) throws ServiceExeption {
		try {
			tagDAO.editItem(tagItem);
		} catch (DAOExeption e) {
			LOGGER.error("Editing tag exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Deleting tag apart from the news
	 * @throws ServiceExeption if can't delete tag apart from the news 
	 */
	public void deleteTag(Long tagId) throws ServiceExeption {
		try {
			tagDAO.deleteItem(tagId);
			tagDAO.unlinkByTagId(tagId);
		} catch (DAOExeption e) {
			LOGGER.error("Deleting tag by tagId exeption", e);
			throw new ServiceExeption();
		}
	}


	/** 
	 * Linking tag with news
	 * @throws ServiceExeption if can't link tag with news 
	 */
	public void link(Long tagId, Long newsId) throws ServiceExeption {
		try {
			tagDAO.link(tagId, newsId);
		} catch (DAOExeption e) {
			LOGGER.error("Linking tag exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Unlinking tag with news in deleting news process
	 * @throws ServiceExeption if can't unlink tag with news in deleting news process
	 */
	public void unlink(Long newsId) throws ServiceExeption {
		try {
			tagDAO.unlink(newsId);
		} catch (DAOExeption e) {
			LOGGER.error("Unlinking tag exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Unlinking tag with news in deleting tag process
	 * @throws ServiceExeption if can't unlink tag with news in deleting tag process
	 */
	public void unlinkByTagId(Long tagId) throws ServiceExeption {
		try {
			tagDAO.unlinkByTagId(tagId);
		} catch (DAOExeption e) {
			LOGGER.error("Unlinking tag with news by tagId exeption", e);
			throw new ServiceExeption();
		}

	}

}
