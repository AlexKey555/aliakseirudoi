package com.epam.rudoi.newsportal.service.impl;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.rudoi.newsportal.dao.IAuthorDAO;
import com.epam.rudoi.newsportal.entity.Author;
import com.epam.rudoi.newsportal.entity.NewsManagementVO;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.IAuthorService;

@Transactional(rollbackFor=Exception.class)
public class AuthorServiceImpl implements IAuthorService {
	
	private static final Logger LOGGER = Logger.getLogger(AuthorServiceImpl.class);
	private IAuthorDAO authorDAO;


	public void setAuthorDAO(IAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}


	/** 
	 * Adding author and link author with news
	 * @throws ServiceExeption if can't add new author or link author with news 
	 */
	public Long addAuthor(NewsManagementVO newsManagementVO) throws ServiceExeption{ // use transaction
		Long authorId = null;
		try {
			authorId = authorDAO.addItem(newsManagementVO.getAuthorItem());
			authorDAO.link(newsManagementVO.getAuthorItem().getAuthorId(), newsManagementVO.getNewsItem().getNewsId());
		} catch (DAOExeption e) {
			LOGGER.error("Linking author with news exeption", e);
			throw new ServiceExeption(); 
		}
	
		return authorId;
	}

	
	/** 
	 * Editing author's information 
	 * @throws ServiceExeption if can't edit author's information 
	 */
	public void editAuthor(Author authorItem) throws ServiceExeption{
		try {
			authorDAO.editItem(authorItem);
		} catch (DAOExeption e) {
			LOGGER.error("Editing author exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Linking author with news 
	 * @throws ServiceExeption if can't link author with news 
	 */
	public void link(Long authorId, Long newsId) throws ServiceExeption{
		try {
			authorDAO.link(authorId, newsId);
		} catch (DAOExeption e) {
			LOGGER.error("Linking author with news exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Unlinking author with news 
	 * @throws ServiceExeption if can't unlink author with news 
	 */
	public void unlink(Long newsId) throws ServiceExeption{
		try {
			authorDAO.unlink(newsId);
		} catch (DAOExeption e) {
			LOGGER.error("Unlinking author with news exeption", e);
			throw new ServiceExeption();
		}
	}


	public void expiredAuthor(Long authorId) throws ServiceExeption {
		try {
			authorDAO.deleteItem(authorId);
		} catch (DAOExeption e) {
			LOGGER.error("Unlinking author with news exeption", e);
			throw new ServiceExeption();
		}
		
	}

}
