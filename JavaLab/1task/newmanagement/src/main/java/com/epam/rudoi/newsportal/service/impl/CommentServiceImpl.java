package com.epam.rudoi.newsportal.service.impl;

import org.apache.log4j.Logger;
import com.epam.rudoi.newsportal.dao.ICommentDAO;
import com.epam.rudoi.newsportal.entity.Comment;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.exeption.ServiceExeption;
import com.epam.rudoi.newsportal.service.ICommentService;

public class CommentServiceImpl implements ICommentService{
	
	private static final Logger LOGGER = Logger.getLogger(CommentServiceImpl.class);
	private ICommentDAO commentDAO;
	

	public void setCommentDAO(ICommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/** 
	 * Adding comment to news
	 * @throws ServiceExeption if can't add comment to news 
	 */
	public Long addComment(Comment commentItem) throws ServiceExeption{
		Long commentId = null;
		try {
			commentDAO.addItem(commentItem);
		} catch (DAOExeption e) {
			LOGGER.error("Adding comment exeption", e);
			throw new ServiceExeption();
			
		}
		commentId = commentItem.getCommentId();
		return commentId;
	}

	/** 
	 * Editing news comment
	 * @throws ServiceExeption if can't edit news comment 
	 */
	public void editComment(Comment commentItem) throws ServiceExeption{
		try {
			commentDAO.editItem(commentItem);
		} catch (DAOExeption e) {
			LOGGER.error("Editing comment exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Deleting news comment
	 * @throws ServiceExeption if can't delete news comment 
	 */
	public void deleteComment(Long commentId) throws ServiceExeption{
		try {
			commentDAO.deleteItem(commentId);
		} catch (DAOExeption e) {
			LOGGER.error("Deleting comment exeption", e);
			throw new ServiceExeption();
		}
	}

	/** 
	 * Unlinking tag with news in deleting news process
	 * @throws ServiceExeption if can't unlink tag with news in deleting news process
	 */
	public void unlink(Long newsId) throws ServiceExeption {
		try {
			commentDAO.unlink(newsId);
		} catch (DAOExeption e) {
			LOGGER.error("Unlinking tag exeption", e);
			throw new ServiceExeption();
		}
	}
	

}
