package com.epam.rudoi.newsportal.dao;

import com.epam.rudoi.newsportal.exeption.DAOExeption;

public interface ICrudDAO <E>{

	Long addItem (E entity) throws DAOExeption;
	
	void editItem (E entity) throws DAOExeption;
	
	void deleteItem (Long itemId) throws DAOExeption;
	
	E readItem(Long itemId) throws DAOExeption;
	
}
