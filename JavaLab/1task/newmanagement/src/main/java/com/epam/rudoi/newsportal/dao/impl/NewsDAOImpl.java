package com.epam.rudoi.newsportal.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.rudoi.newsportal.dao.INewsDAO;
import com.epam.rudoi.newsportal.entity.News;
import com.epam.rudoi.newsportal.entity.SearchCriteria;
import com.epam.rudoi.newsportal.entity.Tag;
import com.epam.rudoi.newsportal.exeption.DAOExeption;
import com.epam.rudoi.newsportal.util.DBCloser;

public class NewsDAOImpl implements INewsDAO {

	private static final String ADD_NEWS = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (sq_news_id_seq.nextval,?,?,?,current_timestamp,sysdate)";
	private static final String EDIT_NEWS = "UPDATE news SET title=?, short_text=?, full_text=?, modification_date=sysdate WHERE news_id=?";
	private static final String DELETE_NEWS = "DELETE FROM news WHERE news_id=?";
	private static final String GET_NEWS = "SELECT a.news_id, a.title, a.short_text, a.full_text, a.creation_date, a.modification_date FROM news a WHERE a.news_id = ?";
	private static final String SHOW_SINGL_NEWS = "SELECT a.news_id, a.title, a.short_text, a.full_text, a.creation_date, a.modification_date FROM news a WHERE a.news_id=?";
	private static final String SHOW_NEWS_COUNT = "SELECT COUNT(DISTINCT a.news_id) FROM news a";
	private static final String SORT_NEWS_BY_MOST_COMMENTS_MODIFICAT_DATE = "SELECT ROW_NUMBER() OVER (ORDER BY count_comments DESC,modification_date)"
			+ " AS ROW_NUMBER,news_id,title,short_text,full_text,creation_date,modification_date,count_comments,author_id,tag_id"
		+ " FROM (SELECT COUNT(*) AS count_comments, news_id, title, short_text, full_text, creation_date, modification_date, author_id, tag_id"
		+ " FROM (SELECT alias_news.news_id, alias_news.title, alias_news.short_text, alias_news.full_text, alias_news.creation_date, alias_comments.comment_id, alias_news.modification_date, alias_news_authors.author_id, alias_news_tags.tag_id"
		+ " FROM news alias_news INNER JOIN comments alias_comments ON alias_news.news_id = alias_comments.news_id INNER JOIN news_authors alias_news_authors ON alias_news.news_id = alias_news_authors.news_id"
		+ " LEFT JOIN news_tags alias_news_tags ON alias_news.news_id = alias_news_tags.news_id"
		+ " {1} {0})"
		+ " GROUP BY news_id,title,short_text,full_text,creation_date,modification_date,author_id,tag_id)";

	private static final String SEARCH_NEWS_AUTHOR_JOIN = " AND alias_news_authors.author_id = ? ";

	private static final String SEARCH_NEWS_TAGS_JOIN = " AND alias_news_tags.tag_id IN(?) ";

	private static final String SEARCH_NEWS_TAGS_AND_AUTHOR_JOIN = " AND alias_news_authors.author_id = ? AND alias_news_tags.tag_id IN(?) ";

	private static final String SORT_NEWS = " WHERE alias_news.news_id BETWEEN ? AND ? ";

	private static final String SORT_NEWS_PARAM_GAG = "";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Fill in DB table news creating news line
	 * 
	 * @throws DAOExeption
	 *             if can't create the line
	 */
	public Long addItem(News news) throws DAOExeption {

		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Long authorId = null;
		String[] column = { "NEWS_ID" };
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(ADD_NEWS, column);
			preparedStatement.setString(1, news.getNewsTitle());
			preparedStatement.setString(2, news.getNewsShortText());
			preparedStatement.setString(3, news.getNewsFullText());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if ((resultSet != null) && (resultSet.next())) {
				authorId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}
		return authorId;

	}

	/**
	 * Edit DB table news by lines
	 * 
	 * @throws DAOExeption
	 *             if can't edit news
	 */
	public void editItem(News news) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(EDIT_NEWS);
			preparedStatement.setString(1, news.getNewsTitle());
			preparedStatement.setString(2, news.getNewsShortText());
			preparedStatement.setString(3, news.getNewsFullText());
			preparedStatement.setLong(4, news.getNewsId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}
	}

	/**
	 * Delete line in DB table news
	 * 
	 * @throws DAOExeption
	 *             if can't delete line
	 */
	public void deleteItem(Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}

	}

	public News readItem(Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		News news = new News();

		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(GET_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null) && (resultSet.next())) {
				news.setNewsId(resultSet.getLong(1));
				news.setNewsTitle(resultSet.getString(2));
				news.setNewsShortText(resultSet.getString(3));
				news.setNewsFullText(resultSet.getString(4));
				news.setNewsCreationDate(resultSet.getTimestamp(5));
				news.setNewsModificationDate(resultSet.getDate(6));
			}
		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}

		return news;

	}

	/**
	 * Count all unique news in DB table news
	 * 
	 * @throws DAOExeption
	 *             if can't count unique news
	 */
	public Long countAllNews() throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Long totalNewsCount = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SHOW_NEWS_COUNT);
			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null) && (resultSet.next())) {
				totalNewsCount = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}
		return totalNewsCount;
	}

	/**
	 * Search news by SearchCriteria SearchCriteria entity include authorItem
	 * and tagsItem Searching by one or two parameters (author or tags)
	 * 
	 * @throws DAOExeption
	 *             if can't search news
	 */
	public List<News> searchNews(SearchCriteria searchCriteria)
			throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		List<News> resultNewsList = new ArrayList<News>();
		String searchQ = buildSearchQuery(searchCriteria);
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(searchQ);
			checkSearchParameters(searchCriteria, preparedStatement);
			resultSet = preparedStatement.executeQuery();
			resultNewsList = parseResultSetNewsSearch(resultSet);
		} catch (SQLException e) {
			throw new DAOExeption(e);
		} finally {
//		DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}

		return resultNewsList;
	}

	/**
	 * Check search parameters;
	 * 
	 * @throws DAOExeption
	 *             if can't do it
	 */
	private void checkSearchParameters(SearchCriteria searchCriteria,
			PreparedStatement preparedStatement) throws SQLException {
		
		Long haveAuthorId = searchCriteria.getAuthorItem().getAuthorId();
		List<Tag> haveTagsList = searchCriteria.getTagItems();
		
		preparedStatement.setLong(1, searchCriteria.getMinSearchValue());
		preparedStatement.setLong(2, searchCriteria.getMaxSearchValue());
		
		if ((haveAuthorId != null) && (haveTagsList != null)) {
			
			preparedStatement.setLong(3, searchCriteria.getAuthorItem()
					.getAuthorId());
			List<Tag> tagsList = searchCriteria.getTagItems();
			preparedStatement.setString(4, buildQueryTagsParam(tagsList));
			
		}

		if (haveAuthorId != null) {
			preparedStatement.setLong(3, searchCriteria.getAuthorItem()
					.getAuthorId());
		}

		if (haveTagsList != null) {
			List<Tag> tagsList = searchCriteria.getTagItems();
			preparedStatement.setString(4, buildQueryTagsParam(tagsList));
		}
	}

	/**
	 * build search query
	 * 
	 * @throws DAOExeption
	 *             if can't do it
	 */
	private String buildSearchQuery(SearchCriteria searchCriteria) {

		String querySearchNews = null;
		Long haveAuthorId = searchCriteria.getAuthorItem().getAuthorId();
		List<Tag> haveTagsList = searchCriteria.getTagItems();

		if (haveAuthorId == null && haveTagsList.size() == 0) {
			querySearchNews = MessageFormat.format(
					SORT_NEWS_BY_MOST_COMMENTS_MODIFICAT_DATE, SORT_NEWS_PARAM_GAG, SORT_NEWS);
		} else {
			if ((haveAuthorId != null) && (haveTagsList != null)) {
				querySearchNews = MessageFormat.format(
						SORT_NEWS_BY_MOST_COMMENTS_MODIFICAT_DATE,
						SEARCH_NEWS_TAGS_AND_AUTHOR_JOIN, SORT_NEWS);

			} else {
				if (haveAuthorId != null) {
					querySearchNews = MessageFormat.format(
							SORT_NEWS_BY_MOST_COMMENTS_MODIFICAT_DATE,
							SEARCH_NEWS_AUTHOR_JOIN, SORT_NEWS);

				} else {
					if (haveTagsList != null) {
						querySearchNews = MessageFormat.format(
								SORT_NEWS_BY_MOST_COMMENTS_MODIFICAT_DATE,
								SEARCH_NEWS_TAGS_JOIN, SORT_NEWS);
					}

				}

			}

		}

		return querySearchNews;
	}

	/**
	 * build query for prepared Tags parameters
	 * 
	 * @throws DAOExeption
	 *             if can't do it
	 */
	private String buildQueryTagsParam(List<Tag> tagsId) {
		StringBuilder resultQueryTags = new StringBuilder();
		for (Tag tagId : tagsId) {
			resultQueryTags.append(tagId.toString());
			resultQueryTags.append(",");
		}
		
		return resultQueryTags.toString();
	}

	/**
	 * Fill in resultNewsList
	 * 
	 * @throws DAOExeption
	 *             if can't fill in
	 */
	private List<News> parseResultSetNewsSearch(ResultSet resultSet)
			throws DAOExeption {
		List<News> resultNewsList = new ArrayList<News>();
		try {
			while (resultSet.next()) {
				News newsItem = new News();
				newsItem.setNewsId(resultSet.getLong(2));
				newsItem.setNewsTitle(resultSet.getString(3));
				newsItem.setNewsShortText(resultSet.getString(4));
				newsItem.setNewsFullText(resultSet.getString(5));
				newsItem.setNewsCreationDate(resultSet.getTimestamp(6));
				newsItem.setNewsModificationDate(resultSet.getDate(7));
				resultNewsList.add(newsItem);
			}
		} catch (SQLException e) {
			throw new DAOExeption();
		}
		return resultNewsList;
	}

	
	/**
	 * Show single news by newsId from DB table news
	 * 
	 * @throws DAOExeption
	 *             if can't show single news
	 */
	public News showSinglNews(Long newsId) throws DAOExeption {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		News newsItem = new News();
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			preparedStatement = connection.prepareStatement(SHOW_SINGL_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null) && (resultSet.next())) {
				newsItem.setNewsId(resultSet.getLong(1));
				newsItem.setNewsTitle(resultSet.getString(2));
				newsItem.setNewsShortText(resultSet.getString(3));
				newsItem.setNewsFullText(resultSet.getString(4));
				newsItem.setNewsCreationDate(resultSet.getTimestamp(5));
				newsItem.setNewsModificationDate(resultSet.getDate(6));
			}
		} catch (SQLException e) {
			throw new DAOExeption();
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement,
					resultSet);
		}
		return newsItem;

	}

}
